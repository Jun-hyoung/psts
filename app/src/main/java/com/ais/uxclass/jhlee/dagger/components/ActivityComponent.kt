package com.ais.uxclass.jhlee.dagger.components

import android.app.Activity
import dagger.Component
import com.ais.uxclass.jhlee.base.BaseActivity
import com.ais.uxclass.jhlee.base.BaseFragment
import com.ais.uxclass.jhlee.dagger.modules.ActivityModule
import com.ais.uxclass.jhlee.dagger.scopes.PerActivity

@PerActivity
@Component(dependencies = [(AppComponent::class)], modules = arrayOf(ActivityModule::class))
interface ActivityComponent {

    fun inject(activity: BaseActivity)

    fun inject(fragment: BaseFragment)

    fun activity(): Activity
}
