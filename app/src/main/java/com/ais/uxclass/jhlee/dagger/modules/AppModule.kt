package com.ais.uxclass.jhlee.dagger.modules

import android.app.Application
import com.ais.uxclass.jhlee.base.Navigator
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule(private val application: Application) {

    @Provides
    @Singleton
    internal fun provideApplication(): Application {
        return this.application
    }

    @Provides
    @Singleton
    internal fun provideNavigator(): Navigator {
        return Navigator()
    }
}
