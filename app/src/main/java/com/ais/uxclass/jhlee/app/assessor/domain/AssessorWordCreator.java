package com.ais.uxclass.jhlee.app.assessor.domain;

import com.ais.uxclass.jhlee.app.assessor.domain.model.MarksAssessment;
import com.ais.uxclass.jhlee.base.AndroidContext;

import org.docx4j.wml.Body;
import org.docx4j.wml.BooleanDefaultTrue;
import org.docx4j.wml.CTBookmark;
import org.docx4j.wml.CTBorder;
import org.docx4j.wml.CTColumns;
import org.docx4j.wml.CTHeight;
import org.docx4j.wml.CTMarkupRange;
import org.docx4j.wml.CTPageNumber;
import org.docx4j.wml.CTShd;
import org.docx4j.wml.CTTblLayoutType;
import org.docx4j.wml.CTTblLook;
import org.docx4j.wml.CTTblPrBase;
import org.docx4j.wml.CTVerticalAlignRun;
import org.docx4j.wml.CTVerticalJc;
import org.docx4j.wml.Color;
import org.docx4j.wml.Document;
import org.docx4j.wml.HpsMeasure;
import org.docx4j.wml.Jc;
import org.docx4j.wml.P;
import org.docx4j.wml.PPr;
import org.docx4j.wml.PPrBase;
import org.docx4j.wml.ParaRPr;
import org.docx4j.wml.R;
import org.docx4j.wml.RFonts;
import org.docx4j.wml.RPr;
import org.docx4j.wml.SectPr;
import org.docx4j.wml.Tbl;
import org.docx4j.wml.TblBorders;
import org.docx4j.wml.TblGrid;
import org.docx4j.wml.TblGridCol;
import org.docx4j.wml.TblPr;
import org.docx4j.wml.TblWidth;
import org.docx4j.wml.Tc;
import org.docx4j.wml.TcPr;
import org.docx4j.wml.TcPrInner;
import org.docx4j.wml.Text;
import org.docx4j.wml.Tr;
import org.docx4j.wml.TrPr;

import java.math.BigInteger;
import java.util.LinkedHashMap;

import javax.xml.bind.JAXBElement;


public class AssessorWordCreator {

    private static final String TABLE1_NAME = AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_category_content_title);
    private static final String TABLE2_NAME = AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_category_language_title);
    private static final String TABLE3_NAME = AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_category_technical_title);

    public Document create(MarksAssessment assessment) {
        org.docx4j.wml.ObjectFactory wmlObjectFactory = new org.docx4j.wml.ObjectFactory();

        Document document = wmlObjectFactory.createDocument();
        // Create object for body
        Body body = wmlObjectFactory.createBody();
        document.setBody(body);
        // Create object for p
        P p = wmlObjectFactory.createP();
        body.getContent().add( p);
        // Create object for pPr
        PPr ppr = wmlObjectFactory.createPPr();
        p.setPPr(ppr);
        // Create object for rPr
        ParaRPr pararpr = wmlObjectFactory.createParaRPr();
        ppr.setRPr(pararpr);
        // Create object for rFonts
        RFonts rfonts = wmlObjectFactory.createRFonts();
        pararpr.setRFonts(rfonts);
        rfonts.setAscii( "Times New Roman");
        rfonts.setCs( "Times New Roman");
        rfonts.setEastAsia( "Times New Roman");
        rfonts.setHAnsi( "Times New Roman");
        // Create object for pStyle
        PPrBase.PStyle pprbasepstyle = wmlObjectFactory.createPPrBasePStyle();
        ppr.setPStyle(pprbasepstyle);
        pprbasepstyle.setVal( "Title");
        // Create object for contextualSpacing
        BooleanDefaultTrue booleandefaulttrue = wmlObjectFactory.createBooleanDefaultTrue();
        ppr.setContextualSpacing(booleandefaulttrue);
        // Create object for bookmarkStart (wrapped in JAXBElement)
        CTBookmark bookmark = wmlObjectFactory.createCTBookmark();
        JAXBElement<org.docx4j.wml.CTBookmark> bookmarkWrapped = wmlObjectFactory.createPBookmarkStart(bookmark);
        p.getContent().add( bookmarkWrapped);
        bookmark.setName( "_gjdgxs");
        bookmark.setColFirst( BigInteger.valueOf( 0) );
        bookmark.setColLast( BigInteger.valueOf( 0) );
        bookmark.setId( BigInteger.valueOf( 0) );
        // Create object for bookmarkEnd (wrapped in JAXBElement)
        CTMarkupRange markuprange = wmlObjectFactory.createCTMarkupRange();
        JAXBElement<org.docx4j.wml.CTMarkupRange> markuprangeWrapped = wmlObjectFactory.createPBookmarkEnd(markuprange);
        p.getContent().add( markuprangeWrapped);
        markuprange.setId( BigInteger.valueOf( 0) );


        // Create object for r
        R r = wmlObjectFactory.createR();
        p.getContent().add( r);
        // Create object for rPr
        RPr rpr = wmlObjectFactory.createRPr();
        r.setRPr(rpr);
        // Create object for rFonts
        RFonts rfonts2 = wmlObjectFactory.createRFonts();
        rpr.setRFonts(rfonts2);
        rfonts2.setAscii( "Times New Roman");
        rfonts2.setCs( "Times New Roman");
        rfonts2.setEastAsia( "Times New Roman");
        rfonts2.setHAnsi( "Times New Roman");
        // Create object for t (wrapped in JAXBElement)
        Text text = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped = wmlObjectFactory.createRT(text);
        r.getContent().add( textWrapped);
        text.setValue( "Presentation Assessment ");
        text.setSpace( "preserve");
        // Create object for p
        P p2 = wmlObjectFactory.createP();
        body.getContent().add( p2);
        // Create object for pPr
        PPr ppr2 = wmlObjectFactory.createPPr();
        p2.setPPr(ppr2);
        // Create object for rPr
        ParaRPr pararpr2 = wmlObjectFactory.createParaRPr();
        ppr2.setRPr(pararpr2);
        // Create object for rFonts
        RFonts rfonts3 = wmlObjectFactory.createRFonts();
        pararpr2.setRFonts(rfonts3);
        rfonts3.setAscii( "Times New Roman");
        rfonts3.setCs( "Times New Roman");
        rfonts3.setEastAsia( "Times New Roman");
        rfonts3.setHAnsi( "Times New Roman");
        // Create object for pStyle
        PPrBase.PStyle pprbasepstyle2 = wmlObjectFactory.createPPrBasePStyle();
        ppr2.setPStyle(pprbasepstyle2);
        pprbasepstyle2.setVal( "Title");
        // Create object for contextualSpacing
        BooleanDefaultTrue booleandefaulttrue2 = wmlObjectFactory.createBooleanDefaultTrue();
        ppr2.setContextualSpacing(booleandefaulttrue2);
        // Create object for r
        R r2 = wmlObjectFactory.createR();
        p2.getContent().add( r2);
        // Create object for rPr
        RPr rpr2 = wmlObjectFactory.createRPr();
        r2.setRPr(rpr2);
        // Create object for rFonts
        RFonts rfonts4 = wmlObjectFactory.createRFonts();
        rpr2.setRFonts(rfonts4);
        rfonts4.setAscii( "Times New Roman");
        rfonts4.setCs( "Times New Roman");
        rfonts4.setEastAsia( "Times New Roman");
        rfonts4.setHAnsi( "Times New Roman");
        // Create object for t (wrapped in JAXBElement)
        Text text2 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped2 = wmlObjectFactory.createRT(text2);
        r2.getContent().add( textWrapped2);
        text2.setValue( "Report");
        // Create object for p
        P p3 = wmlObjectFactory.createP();
        body.getContent().add( p3);
        // Create object for tbl (wrapped in JAXBElement)
        Tbl tbl = wmlObjectFactory.createTbl();
        JAXBElement<org.docx4j.wml.Tbl> tblWrapped = wmlObjectFactory.createBodyTbl(tbl);
        body.getContent().add( tblWrapped);
        // Create object for tr
        Tr tr = wmlObjectFactory.createTr();
        tbl.getContent().add( tr);
        // Create object for tc (wrapped in JAXBElement)
        Tc tc = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped = wmlObjectFactory.createTrTc(tc);
        tr.getContent().add( tcWrapped);
        // Create object for p
        P p4 = wmlObjectFactory.createP();
        tc.getContent().add( p4);
        // Create object for pPr
        PPr ppr3 = wmlObjectFactory.createPPr();
        p4.setPPr(ppr3);
        // Create object for rPr
        ParaRPr pararpr3 = wmlObjectFactory.createParaRPr();
        ppr3.setRPr(pararpr3);
        // Create object for sz
        HpsMeasure hpsmeasure = wmlObjectFactory.createHpsMeasure();
        pararpr3.setSz(hpsmeasure);
        hpsmeasure.setVal( BigInteger.valueOf( 24) );
        // Create object for rFonts
        RFonts rfonts5 = wmlObjectFactory.createRFonts();
        pararpr3.setRFonts(rfonts5);
        rfonts5.setAscii( "Times New Roman");
        rfonts5.setCs( "Times New Roman");
        rfonts5.setEastAsia( "Times New Roman");
        rfonts5.setHAnsi( "Times New Roman");
        // Create object for szCs
        HpsMeasure hpsmeasure2 = wmlObjectFactory.createHpsMeasure();
        pararpr3.setSzCs(hpsmeasure2);
        hpsmeasure2.setVal( BigInteger.valueOf( 24) );
        // Create object for jc
        Jc jc = wmlObjectFactory.createJc();
        ppr3.setJc(jc);
        jc.setVal(org.docx4j.wml.JcEnumeration.RIGHT);
        // Create object for r
        R r3 = wmlObjectFactory.createR();
        p4.getContent().add( r3);
        // Create object for rPr
        RPr rpr3 = wmlObjectFactory.createRPr();
        r3.setRPr(rpr3);
        // Create object for sz
        HpsMeasure hpsmeasure3 = wmlObjectFactory.createHpsMeasure();
        rpr3.setSz(hpsmeasure3);
        hpsmeasure3.setVal( BigInteger.valueOf( 24) );
        // Create object for rFonts
        RFonts rfonts6 = wmlObjectFactory.createRFonts();
        rpr3.setRFonts(rfonts6);
        rfonts6.setAscii( "Times New Roman");
        rfonts6.setCs( "Times New Roman");
        rfonts6.setEastAsia( "Times New Roman");
        rfonts6.setHAnsi( "Times New Roman");
        // Create object for szCs
        HpsMeasure hpsmeasure4 = wmlObjectFactory.createHpsMeasure();
        rpr3.setSzCs(hpsmeasure4);
        hpsmeasure4.setVal( BigInteger.valueOf( 24) );
        // Create object for t (wrapped in JAXBElement)
        Text text3 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped3 = wmlObjectFactory.createRT(text3);
        r3.getContent().add( textWrapped3);
        text3.setValue( "Course Code:");
        // Create object for tcPr
        TcPr tcpr = wmlObjectFactory.createTcPr();
        tc.setTcPr(tcpr);
        // Create object for vAlign
        CTVerticalJc verticaljc = wmlObjectFactory.createCTVerticalJc();
        tcpr.setVAlign(verticaljc);
        verticaljc.setVal(org.docx4j.wml.STVerticalJc.BOTTOM);
        // Create object for tcW
        TblWidth tblwidth = wmlObjectFactory.createTblWidth();
        tcpr.setTcW(tblwidth);
        tblwidth.setW( BigInteger.valueOf( 1560) );
        tblwidth.setType( "dxa");
        // Create object for tcBorders
        TcPrInner.TcBorders tcprinnertcborders = wmlObjectFactory.createTcPrInnerTcBorders();
        tcpr.setTcBorders(tcprinnertcborders);
        // Create object for top
        CTBorder border = wmlObjectFactory.createCTBorder();
        tcprinnertcborders.setTop(border);
        border.setVal(org.docx4j.wml.STBorder.NIL);
        // Create object for bottom
        CTBorder border2 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders.setBottom(border2);
        border2.setVal(org.docx4j.wml.STBorder.NIL);
        // Create object for left
        CTBorder border3 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders.setLeft(border3);
        border3.setVal(org.docx4j.wml.STBorder.NIL);
        // Create object for right
        CTBorder border4 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders.setRight(border4);
        border4.setVal(org.docx4j.wml.STBorder.NIL);
        // Create object for tc (wrapped in JAXBElement)
        Tc tc2 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped2 = wmlObjectFactory.createTrTc(tc2);
        tr.getContent().add( tcWrapped2);
        // Create object for p
        P p5 = wmlObjectFactory.createP();
        tc2.getContent().add( p5);
        // Create object for pPr
        PPr ppr4 = wmlObjectFactory.createPPr();
        p5.setPPr(ppr4);
        // Create object for rPr
        ParaRPr pararpr4 = wmlObjectFactory.createParaRPr();
        ppr4.setRPr(pararpr4);
        // Create object for sz
        HpsMeasure hpsmeasure5 = wmlObjectFactory.createHpsMeasure();
        pararpr4.setSz(hpsmeasure5);
        hpsmeasure5.setVal( BigInteger.valueOf( 24) );
        // Create object for rFonts
        RFonts rfonts7 = wmlObjectFactory.createRFonts();
        pararpr4.setRFonts(rfonts7);
        rfonts7.setAscii( "Times New Roman");
        rfonts7.setCs( "Times New Roman");
        rfonts7.setEastAsia( "Times New Roman");
        rfonts7.setHAnsi( "Times New Roman");
        // Create object for szCs
        HpsMeasure hpsmeasure6 = wmlObjectFactory.createHpsMeasure();
        pararpr4.setSzCs(hpsmeasure6);
        hpsmeasure6.setVal( BigInteger.valueOf( 24) );
        // Create object for r
        R r4 = wmlObjectFactory.createR();
        p5.getContent().add( r4);
        // Create object for rPr
        RPr rpr4 = wmlObjectFactory.createRPr();
        r4.setRPr(rpr4);
        // Create object for sz
        HpsMeasure hpsmeasure7 = wmlObjectFactory.createHpsMeasure();
        rpr4.setSz(hpsmeasure7);
        hpsmeasure7.setVal( BigInteger.valueOf( 24) );
        // Create object for rFonts
        RFonts rfonts8 = wmlObjectFactory.createRFonts();
        rpr4.setRFonts(rfonts8);
        rfonts8.setAscii( "Times New Roman");
        rfonts8.setCs( "Times New Roman");
        rfonts8.setEastAsia( "Times New Roman");
        rfonts8.setHAnsi( "Times New Roman");
        // Create object for szCs
        HpsMeasure hpsmeasure8 = wmlObjectFactory.createHpsMeasure();
        rpr4.setSzCs(hpsmeasure8);
        hpsmeasure8.setVal( BigInteger.valueOf( 24) );
        // Create object for t (wrapped in JAXBElement)
        Text text4 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped4 = wmlObjectFactory.createRT(text4);
        r4.getContent().add( textWrapped4);
        text4.setValue( assessment.getProfile().getName());
        // Create object for tcPr
        TcPr tcpr2 = wmlObjectFactory.createTcPr();
        tc2.setTcPr(tcpr2);
        // Create object for vAlign
        CTVerticalJc verticaljc2 = wmlObjectFactory.createCTVerticalJc();
        tcpr2.setVAlign(verticaljc2);
        verticaljc2.setVal(org.docx4j.wml.STVerticalJc.BOTTOM);
        // Create object for tcW
        TblWidth tblwidth2 = wmlObjectFactory.createTblWidth();
        tcpr2.setTcW(tblwidth2);
        tblwidth2.setW( BigInteger.valueOf( 2158) );
        tblwidth2.setType( "dxa");
        // Create object for tcBorders
        TcPrInner.TcBorders tcprinnertcborders2 = wmlObjectFactory.createTcPrInnerTcBorders();
        tcpr2.setTcBorders(tcprinnertcborders2);
        // Create object for top
        CTBorder border5 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders2.setTop(border5);
        border5.setVal(org.docx4j.wml.STBorder.NIL);
        // Create object for bottom
        CTBorder border6 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders2.setBottom(border6);
        border6.setVal(org.docx4j.wml.STBorder.SINGLE);
        border6.setSz( BigInteger.valueOf( 4) );
        border6.setColor( "000000");
        border6.setSpace( BigInteger.valueOf( 0) );
        // Create object for left
        CTBorder border7 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders2.setLeft(border7);
        border7.setVal(org.docx4j.wml.STBorder.NIL);
        // Create object for right
        CTBorder border8 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders2.setRight(border8);
        border8.setVal(org.docx4j.wml.STBorder.NIL);
        // Create object for tc (wrapped in JAXBElement)
        Tc tc3 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped3 = wmlObjectFactory.createTrTc(tc3);
        tr.getContent().add( tcWrapped3);
        // Create object for p
        P p6 = wmlObjectFactory.createP();
        tc3.getContent().add( p6);
        // Create object for pPr
        PPr ppr5 = wmlObjectFactory.createPPr();
        p6.setPPr(ppr5);
        // Create object for rPr
        ParaRPr pararpr5 = wmlObjectFactory.createParaRPr();
        ppr5.setRPr(pararpr5);
        // Create object for sz
        HpsMeasure hpsmeasure9 = wmlObjectFactory.createHpsMeasure();
        pararpr5.setSz(hpsmeasure9);
        hpsmeasure9.setVal( BigInteger.valueOf( 24) );
        // Create object for rFonts
        RFonts rfonts9 = wmlObjectFactory.createRFonts();
        pararpr5.setRFonts(rfonts9);
        rfonts9.setAscii( "Times New Roman");
        rfonts9.setCs( "Times New Roman");
        rfonts9.setEastAsia( "Times New Roman");
        rfonts9.setHAnsi( "Times New Roman");
        // Create object for szCs
        HpsMeasure hpsmeasure10 = wmlObjectFactory.createHpsMeasure();
        pararpr5.setSzCs(hpsmeasure10);
        hpsmeasure10.setVal( BigInteger.valueOf( 24) );
        // Create object for jc
        Jc jc2 = wmlObjectFactory.createJc();
        ppr5.setJc(jc2);
        jc2.setVal(org.docx4j.wml.JcEnumeration.RIGHT);
        // Create object for r
        R r5 = wmlObjectFactory.createR();
        p6.getContent().add( r5);
        // Create object for rPr
        RPr rpr5 = wmlObjectFactory.createRPr();
        r5.setRPr(rpr5);
        // Create object for sz
        HpsMeasure hpsmeasure11 = wmlObjectFactory.createHpsMeasure();
        rpr5.setSz(hpsmeasure11);
        hpsmeasure11.setVal( BigInteger.valueOf( 24) );
        // Create object for rFonts
        RFonts rfonts10 = wmlObjectFactory.createRFonts();
        rpr5.setRFonts(rfonts10);
        rfonts10.setAscii( "Times New Roman");
        rfonts10.setCs( "Times New Roman");
        rfonts10.setEastAsia( "Times New Roman");
        rfonts10.setHAnsi( "Times New Roman");
        // Create object for szCs
        HpsMeasure hpsmeasure12 = wmlObjectFactory.createHpsMeasure();
        rpr5.setSzCs(hpsmeasure12);
        hpsmeasure12.setVal( BigInteger.valueOf( 24) );
        // Create object for t (wrapped in JAXBElement)
        Text text5 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped5 = wmlObjectFactory.createRT(text5);
        r5.getContent().add( textWrapped5);
        text5.setValue( "Semester:");
        // Create object for tcPr
        TcPr tcpr3 = wmlObjectFactory.createTcPr();
        tc3.setTcPr(tcpr3);
        // Create object for vAlign
        CTVerticalJc verticaljc3 = wmlObjectFactory.createCTVerticalJc();
        tcpr3.setVAlign(verticaljc3);
        verticaljc3.setVal(org.docx4j.wml.STVerticalJc.BOTTOM);
        // Create object for tcW
        TblWidth tblwidth3 = wmlObjectFactory.createTblWidth();
        tcpr3.setTcW(tblwidth3);
        tblwidth3.setW( BigInteger.valueOf( 1503) );
        tblwidth3.setType( "dxa");
        // Create object for tcBorders
        TcPrInner.TcBorders tcprinnertcborders3 = wmlObjectFactory.createTcPrInnerTcBorders();
        tcpr3.setTcBorders(tcprinnertcborders3);
        // Create object for top
        CTBorder border9 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders3.setTop(border9);
        border9.setVal(org.docx4j.wml.STBorder.NIL);
        // Create object for bottom
        CTBorder border10 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders3.setBottom(border10);
        border10.setVal(org.docx4j.wml.STBorder.NIL);
        // Create object for left
        CTBorder border11 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders3.setLeft(border11);
        border11.setVal(org.docx4j.wml.STBorder.NIL);
        // Create object for right
        CTBorder border12 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders3.setRight(border12);
        border12.setVal(org.docx4j.wml.STBorder.NIL);
        // Create object for tc (wrapped in JAXBElement)
        Tc tc4 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped4 = wmlObjectFactory.createTrTc(tc4);
        tr.getContent().add( tcWrapped4);
        // Create object for p
        P p7 = wmlObjectFactory.createP();
        tc4.getContent().add( p7);
        // Create object for pPr
        PPr ppr6 = wmlObjectFactory.createPPr();
        p7.setPPr(ppr6);
        // Create object for rPr
        ParaRPr pararpr6 = wmlObjectFactory.createParaRPr();
        ppr6.setRPr(pararpr6);
        // Create object for sz
        HpsMeasure hpsmeasure13 = wmlObjectFactory.createHpsMeasure();
        pararpr6.setSz(hpsmeasure13);
        hpsmeasure13.setVal( BigInteger.valueOf( 24) );
        // Create object for rFonts
        RFonts rfonts11 = wmlObjectFactory.createRFonts();
        pararpr6.setRFonts(rfonts11);
        rfonts11.setAscii( "Times New Roman");
        rfonts11.setCs( "Times New Roman");
        rfonts11.setEastAsia( "Times New Roman");
        rfonts11.setHAnsi( "Times New Roman");
        // Create object for szCs
        HpsMeasure hpsmeasure14 = wmlObjectFactory.createHpsMeasure();
        pararpr6.setSzCs(hpsmeasure14);
        hpsmeasure14.setVal( BigInteger.valueOf( 24) );
        // Create object for r
        R r6 = wmlObjectFactory.createR();
        p7.getContent().add( r6);
        // Create object for rPr
        RPr rpr6 = wmlObjectFactory.createRPr();
        r6.setRPr(rpr6);
        // Create object for sz
        HpsMeasure hpsmeasure15 = wmlObjectFactory.createHpsMeasure();
        rpr6.setSz(hpsmeasure15);
        hpsmeasure15.setVal( BigInteger.valueOf( 24) );
        // Create object for rFonts
        RFonts rfonts12 = wmlObjectFactory.createRFonts();
        rpr6.setRFonts(rfonts12);
        rfonts12.setAscii( "Times New Roman");
        rfonts12.setCs( "Times New Roman");
        rfonts12.setEastAsia( "Times New Roman");
        rfonts12.setHAnsi( "Times New Roman");
        // Create object for szCs
        HpsMeasure hpsmeasure16 = wmlObjectFactory.createHpsMeasure();
        rpr6.setSzCs(hpsmeasure16);
        hpsmeasure16.setVal( BigInteger.valueOf( 24) );


//        // Create object for t (wrapped in JAXBElement)
//        Text text6 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped6 = wmlObjectFactory.createRT(text6);
//        r6.getContent().add( textWrapped6);
//        text6.setValue( "3");
//        // Create object for r
//        R r7 = wmlObjectFactory.createR();
//        p7.getContent().add( r7);
//        // Create object for rPr
//        RPr rpr7 = wmlObjectFactory.createRPr();
//        r7.setRPr(rpr7);
//        // Create object for sz
//        HpsMeasure hpsmeasure17 = wmlObjectFactory.createHpsMeasure();
//        rpr7.setSz(hpsmeasure17);
//        hpsmeasure17.setVal( BigInteger.valueOf( 24) );
//        // Create object for rFonts
//        RFonts rfonts13 = wmlObjectFactory.createRFonts();
//        rpr7.setRFonts(rfonts13);
//        rfonts13.setAscii( "Times New Roman");
//        rfonts13.setCs( "Times New Roman");
//        rfonts13.setEastAsia( "Times New Roman");
//        rfonts13.setHAnsi( "Times New Roman");
//        // Create object for szCs
//        HpsMeasure hpsmeasure18 = wmlObjectFactory.createHpsMeasure();
//        rpr7.setSzCs(hpsmeasure18);
//        hpsmeasure18.setVal( BigInteger.valueOf( 24) );
//        // Create object for vertAlign
//        CTVerticalAlignRun verticalalignrun = wmlObjectFactory.createCTVerticalAlignRun();
//        rpr7.setVertAlign(verticalalignrun);
//        verticalalignrun.setVal(org.docx4j.wml.STVerticalAlignRun.SUPERSCRIPT);
//
//
//        // Create object for t (wrapped in JAXBElement)
//        Text text7 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped7 = wmlObjectFactory.createRT(text7);
//        r7.getContent().add( textWrapped7);
//        text7.setValue( "rd");

        // Create object for r
        R r8 = wmlObjectFactory.createR();
        p7.getContent().add( r8);
        // Create object for rPr
        RPr rpr8 = wmlObjectFactory.createRPr();
        r8.setRPr(rpr8);
        // Create object for sz
        HpsMeasure hpsmeasure19 = wmlObjectFactory.createHpsMeasure();
        rpr8.setSz(hpsmeasure19);
        hpsmeasure19.setVal( BigInteger.valueOf( 24) );
        // Create object for rFonts
        RFonts rfonts14 = wmlObjectFactory.createRFonts();
        rpr8.setRFonts(rfonts14);
        rfonts14.setAscii( "Times New Roman");
        rfonts14.setCs( "Times New Roman");
        rfonts14.setEastAsia( "Times New Roman");
        rfonts14.setHAnsi( "Times New Roman");
        // Create object for szCs
        HpsMeasure hpsmeasure20 = wmlObjectFactory.createHpsMeasure();
        rpr8.setSzCs(hpsmeasure20);
        hpsmeasure20.setVal( BigInteger.valueOf( 24) );

        // Create object for t (wrapped in JAXBElement)
        Text text8 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped8 = wmlObjectFactory.createRT(text8);
        r8.getContent().add( textWrapped8);
        text8.setValue( assessment.getProfile().getSemsester());
        text8.setSpace( "preserve");
        // Create object for tcPr
        TcPr tcpr4 = wmlObjectFactory.createTcPr();
        tc4.setTcPr(tcpr4);
        // Create object for vAlign
        CTVerticalJc verticaljc4 = wmlObjectFactory.createCTVerticalJc();
        tcpr4.setVAlign(verticaljc4);
        verticaljc4.setVal(org.docx4j.wml.STVerticalJc.BOTTOM);
        // Create object for tcW
        TblWidth tblwidth4 = wmlObjectFactory.createTblWidth();
        tcpr4.setTcW(tblwidth4);
        tblwidth4.setW( BigInteger.valueOf( 1503) );
        tblwidth4.setType( "dxa");
        // Create object for tcBorders
        TcPrInner.TcBorders tcprinnertcborders4 = wmlObjectFactory.createTcPrInnerTcBorders();
        tcpr4.setTcBorders(tcprinnertcborders4);
        // Create object for top
        CTBorder border13 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders4.setTop(border13);
        border13.setVal(org.docx4j.wml.STBorder.NIL);
        // Create object for bottom
        CTBorder border14 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders4.setBottom(border14);
        border14.setVal(org.docx4j.wml.STBorder.SINGLE);
        border14.setSz( BigInteger.valueOf( 4) );
        border14.setColor( "000000");
        border14.setSpace( BigInteger.valueOf( 0) );
        // Create object for left
        CTBorder border15 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders4.setLeft(border15);
        border15.setVal(org.docx4j.wml.STBorder.NIL);
        // Create object for right
        CTBorder border16 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders4.setRight(border16);
        border16.setVal(org.docx4j.wml.STBorder.NIL);
        // Create object for tc (wrapped in JAXBElement)
        Tc tc5 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped5 = wmlObjectFactory.createTrTc(tc5);
        tr.getContent().add( tcWrapped5);
        // Create object for p
        P p8 = wmlObjectFactory.createP();
        tc5.getContent().add( p8);
        // Create object for pPr
        PPr ppr7 = wmlObjectFactory.createPPr();
        p8.setPPr(ppr7);
        // Create object for rPr
        ParaRPr pararpr7 = wmlObjectFactory.createParaRPr();
        ppr7.setRPr(pararpr7);
        // Create object for sz
        HpsMeasure hpsmeasure21 = wmlObjectFactory.createHpsMeasure();
        pararpr7.setSz(hpsmeasure21);
        hpsmeasure21.setVal( BigInteger.valueOf( 24) );
        // Create object for rFonts
        RFonts rfonts15 = wmlObjectFactory.createRFonts();
        pararpr7.setRFonts(rfonts15);
        rfonts15.setAscii( "Times New Roman");
        rfonts15.setCs( "Times New Roman");
        rfonts15.setEastAsia( "Times New Roman");
        rfonts15.setHAnsi( "Times New Roman");
        // Create object for szCs
        HpsMeasure hpsmeasure22 = wmlObjectFactory.createHpsMeasure();
        pararpr7.setSzCs(hpsmeasure22);
        hpsmeasure22.setVal( BigInteger.valueOf( 24) );
        // Create object for jc
        Jc jc3 = wmlObjectFactory.createJc();
        ppr7.setJc(jc3);
        jc3.setVal(org.docx4j.wml.JcEnumeration.RIGHT);
        // Create object for r
        R r9 = wmlObjectFactory.createR();
        p8.getContent().add( r9);
        // Create object for rPr
        RPr rpr9 = wmlObjectFactory.createRPr();
        r9.setRPr(rpr9);
        // Create object for sz
        HpsMeasure hpsmeasure23 = wmlObjectFactory.createHpsMeasure();
        rpr9.setSz(hpsmeasure23);
        hpsmeasure23.setVal( BigInteger.valueOf( 24) );
        // Create object for rFonts
        RFonts rfonts16 = wmlObjectFactory.createRFonts();
        rpr9.setRFonts(rfonts16);
        rfonts16.setAscii( "Times New Roman");
        rfonts16.setCs( "Times New Roman");
        rfonts16.setEastAsia( "Times New Roman");
        rfonts16.setHAnsi( "Times New Roman");
        // Create object for szCs
        HpsMeasure hpsmeasure24 = wmlObjectFactory.createHpsMeasure();
        rpr9.setSzCs(hpsmeasure24);
        hpsmeasure24.setVal( BigInteger.valueOf( 24) );
        // Create object for t (wrapped in JAXBElement)
        Text text9 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped9 = wmlObjectFactory.createRT(text9);
        r9.getContent().add( textWrapped9);
        text9.setValue( "ID:");
        // Create object for tcPr
        TcPr tcpr5 = wmlObjectFactory.createTcPr();
        tc5.setTcPr(tcpr5);
        // Create object for vAlign
        CTVerticalJc verticaljc5 = wmlObjectFactory.createCTVerticalJc();
        tcpr5.setVAlign(verticaljc5);
        verticaljc5.setVal(org.docx4j.wml.STVerticalJc.BOTTOM);
        // Create object for tcW
        TblWidth tblwidth5 = wmlObjectFactory.createTblWidth();
        tcpr5.setTcW(tblwidth5);
        tblwidth5.setW( BigInteger.valueOf( 1503) );
        tblwidth5.setType( "dxa");
        // Create object for tcBorders
        TcPrInner.TcBorders tcprinnertcborders5 = wmlObjectFactory.createTcPrInnerTcBorders();
        tcpr5.setTcBorders(tcprinnertcborders5);
        // Create object for top
        CTBorder border17 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders5.setTop(border17);
        border17.setVal(org.docx4j.wml.STBorder.NIL);
        // Create object for bottom
        CTBorder border18 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders5.setBottom(border18);
        border18.setVal(org.docx4j.wml.STBorder.NIL);
        // Create object for left
        CTBorder border19 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders5.setLeft(border19);
        border19.setVal(org.docx4j.wml.STBorder.NIL);
        // Create object for right
        CTBorder border20 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders5.setRight(border20);
        border20.setVal(org.docx4j.wml.STBorder.NIL);
        // Create object for tc (wrapped in JAXBElement)
        Tc tc6 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped6 = wmlObjectFactory.createTrTc(tc6);
        tr.getContent().add( tcWrapped6);
        // Create object for p
        P p9 = wmlObjectFactory.createP();
        tc6.getContent().add( p9);
        // Create object for pPr
        PPr ppr8 = wmlObjectFactory.createPPr();
        p9.setPPr(ppr8);
        // Create object for rPr
        ParaRPr pararpr8 = wmlObjectFactory.createParaRPr();
        ppr8.setRPr(pararpr8);
        // Create object for sz
        HpsMeasure hpsmeasure25 = wmlObjectFactory.createHpsMeasure();
        pararpr8.setSz(hpsmeasure25);
        hpsmeasure25.setVal( BigInteger.valueOf( 24) );
        // Create object for rFonts
        RFonts rfonts17 = wmlObjectFactory.createRFonts();
        pararpr8.setRFonts(rfonts17);
        rfonts17.setAscii( "Times New Roman");
        rfonts17.setCs( "Times New Roman");
        rfonts17.setEastAsia( "Times New Roman");
        rfonts17.setHAnsi( "Times New Roman");
        // Create object for szCs
        HpsMeasure hpsmeasure26 = wmlObjectFactory.createHpsMeasure();
        pararpr8.setSzCs(hpsmeasure26);
        hpsmeasure26.setVal( BigInteger.valueOf( 24) );
        // Create object for r
        R r10 = wmlObjectFactory.createR();
        p9.getContent().add( r10);
        // Create object for rPr
        RPr rpr10 = wmlObjectFactory.createRPr();
        r10.setRPr(rpr10);
        // Create object for sz
        HpsMeasure hpsmeasure27 = wmlObjectFactory.createHpsMeasure();
        rpr10.setSz(hpsmeasure27);
        hpsmeasure27.setVal( BigInteger.valueOf( 24) );
        // Create object for rFonts
        RFonts rfonts18 = wmlObjectFactory.createRFonts();
        rpr10.setRFonts(rfonts18);
        rfonts18.setAscii( "Times New Roman");
        rfonts18.setCs( "Times New Roman");
        rfonts18.setEastAsia( "Times New Roman");
        rfonts18.setHAnsi( "Times New Roman");
        // Create object for szCs
        HpsMeasure hpsmeasure28 = wmlObjectFactory.createHpsMeasure();
        rpr10.setSzCs(hpsmeasure28);
        hpsmeasure28.setVal( BigInteger.valueOf( 24) );
        // Create object for t (wrapped in JAXBElement)
        Text text10 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped10 = wmlObjectFactory.createRT(text10);
        r10.getContent().add( textWrapped10);
        text10.setValue( assessment.getProfile().getId());
        // Create object for tcPr
        TcPr tcpr6 = wmlObjectFactory.createTcPr();
        tc6.setTcPr(tcpr6);
        // Create object for vAlign
        CTVerticalJc verticaljc6 = wmlObjectFactory.createCTVerticalJc();
        tcpr6.setVAlign(verticaljc6);
        verticaljc6.setVal(org.docx4j.wml.STVerticalJc.BOTTOM);
        // Create object for tcW
        TblWidth tblwidth6 = wmlObjectFactory.createTblWidth();
        tcpr6.setTcW(tblwidth6);
        tblwidth6.setW( BigInteger.valueOf( 1980) );
        tblwidth6.setType( "dxa");
        // Create object for tcBorders
        TcPrInner.TcBorders tcprinnertcborders6 = wmlObjectFactory.createTcPrInnerTcBorders();
        tcpr6.setTcBorders(tcprinnertcborders6);
        // Create object for top
        CTBorder border21 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders6.setTop(border21);
        border21.setVal(org.docx4j.wml.STBorder.NIL);
        // Create object for bottom
        CTBorder border22 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders6.setBottom(border22);
        border22.setVal(org.docx4j.wml.STBorder.SINGLE);
        border22.setSz( BigInteger.valueOf( 4) );
        border22.setColor( "000000");
        border22.setSpace( BigInteger.valueOf( 0) );
        // Create object for left
        CTBorder border23 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders6.setLeft(border23);
        border23.setVal(org.docx4j.wml.STBorder.NIL);
        // Create object for right
        CTBorder border24 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders6.setRight(border24);
        border24.setVal(org.docx4j.wml.STBorder.NIL);
        // Create object for trPr
        TrPr trpr = wmlObjectFactory.createTrPr();
        tr.setTrPr(trpr);
        // Create object for trHeight (wrapped in JAXBElement)
        CTHeight height = wmlObjectFactory.createCTHeight();
        JAXBElement<org.docx4j.wml.CTHeight> heightWrapped = wmlObjectFactory.createCTTrPrBaseTrHeight(height);
        trpr.getCnfStyleOrDivIdOrGridBefore().add( heightWrapped);
        height.setVal( BigInteger.valueOf( 320) );
        // Create object for jc (wrapped in JAXBElement)
        Jc jc4 = wmlObjectFactory.createJc();
        JAXBElement<org.docx4j.wml.Jc> jcWrapped = wmlObjectFactory.createCTTrPrBaseJc(jc4);
        trpr.getCnfStyleOrDivIdOrGridBefore().add( jcWrapped);
        jc4.setVal(org.docx4j.wml.JcEnumeration.CENTER);
        // Create object for tr
        Tr tr2 = wmlObjectFactory.createTr();
        tbl.getContent().add( tr2);
        // Create object for tc (wrapped in JAXBElement)
        Tc tc7 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped7 = wmlObjectFactory.createTrTc(tc7);
        tr2.getContent().add( tcWrapped7);
        // Create object for p
        P p10 = wmlObjectFactory.createP();
        tc7.getContent().add( p10);
        // Create object for pPr
        PPr ppr9 = wmlObjectFactory.createPPr();
        p10.setPPr(ppr9);
        // Create object for rPr
        ParaRPr pararpr9 = wmlObjectFactory.createParaRPr();
        ppr9.setRPr(pararpr9);
        // Create object for sz
        HpsMeasure hpsmeasure29 = wmlObjectFactory.createHpsMeasure();
        pararpr9.setSz(hpsmeasure29);
        hpsmeasure29.setVal( BigInteger.valueOf( 24) );
        // Create object for rFonts
        RFonts rfonts19 = wmlObjectFactory.createRFonts();
        pararpr9.setRFonts(rfonts19);
        rfonts19.setAscii( "Times New Roman");
        rfonts19.setCs( "Times New Roman");
        rfonts19.setEastAsia( "Times New Roman");
        rfonts19.setHAnsi( "Times New Roman");
        // Create object for szCs
        HpsMeasure hpsmeasure30 = wmlObjectFactory.createHpsMeasure();
        pararpr9.setSzCs(hpsmeasure30);
        hpsmeasure30.setVal( BigInteger.valueOf( 24) );
        // Create object for jc
        Jc jc5 = wmlObjectFactory.createJc();
        ppr9.setJc(jc5);
        jc5.setVal(org.docx4j.wml.JcEnumeration.RIGHT);
        // Create object for r
        R r11 = wmlObjectFactory.createR();
        p10.getContent().add( r11);
        // Create object for rPr
        RPr rpr11 = wmlObjectFactory.createRPr();
        r11.setRPr(rpr11);
        // Create object for sz
        HpsMeasure hpsmeasure31 = wmlObjectFactory.createHpsMeasure();
        rpr11.setSz(hpsmeasure31);
        hpsmeasure31.setVal( BigInteger.valueOf( 24) );
        // Create object for rFonts
        RFonts rfonts20 = wmlObjectFactory.createRFonts();
        rpr11.setRFonts(rfonts20);
        rfonts20.setAscii( "Times New Roman");
        rfonts20.setCs( "Times New Roman");
        rfonts20.setEastAsia( "Times New Roman");
        rfonts20.setHAnsi( "Times New Roman");
        // Create object for szCs
        HpsMeasure hpsmeasure32 = wmlObjectFactory.createHpsMeasure();
        rpr11.setSzCs(hpsmeasure32);
        hpsmeasure32.setVal( BigInteger.valueOf( 24) );
        // Create object for t (wrapped in JAXBElement)
        Text text11 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped11 = wmlObjectFactory.createRT(text11);
        r11.getContent().add( textWrapped11);
        text11.setValue( "Topic:");
        // Create object for tcPr
        TcPr tcpr7 = wmlObjectFactory.createTcPr();
        tc7.setTcPr(tcpr7);
        // Create object for vAlign
        CTVerticalJc verticaljc7 = wmlObjectFactory.createCTVerticalJc();
        tcpr7.setVAlign(verticaljc7);
        verticaljc7.setVal(org.docx4j.wml.STVerticalJc.BOTTOM);
        // Create object for tcW
        TblWidth tblwidth7 = wmlObjectFactory.createTblWidth();
        tcpr7.setTcW(tblwidth7);
        tblwidth7.setW( BigInteger.valueOf( 1560) );
        tblwidth7.setType( "dxa");
        // Create object for tcBorders
        TcPrInner.TcBorders tcprinnertcborders7 = wmlObjectFactory.createTcPrInnerTcBorders();
        tcpr7.setTcBorders(tcprinnertcborders7);
        // Create object for top
        CTBorder border25 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders7.setTop(border25);
        border25.setVal(org.docx4j.wml.STBorder.NIL);
        // Create object for bottom
        CTBorder border26 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders7.setBottom(border26);
        border26.setVal(org.docx4j.wml.STBorder.NIL);
        // Create object for left
        CTBorder border27 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders7.setLeft(border27);
        border27.setVal(org.docx4j.wml.STBorder.NIL);
        // Create object for right
        CTBorder border28 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders7.setRight(border28);
        border28.setVal(org.docx4j.wml.STBorder.NIL);
        // Create object for tc (wrapped in JAXBElement)
        Tc tc8 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped8 = wmlObjectFactory.createTrTc(tc8);
        tr2.getContent().add( tcWrapped8);
        // Create object for p
        P p11 = wmlObjectFactory.createP();
        tc8.getContent().add( p11);
        // Create object for pPr
        PPr ppr10 = wmlObjectFactory.createPPr();
        p11.setPPr(ppr10);
        // Create object for rPr
        ParaRPr pararpr10 = wmlObjectFactory.createParaRPr();
        ppr10.setRPr(pararpr10);
        // Create object for sz
        HpsMeasure hpsmeasure33 = wmlObjectFactory.createHpsMeasure();
        pararpr10.setSz(hpsmeasure33);
        hpsmeasure33.setVal( BigInteger.valueOf( 24) );
        // Create object for rFonts
        RFonts rfonts21 = wmlObjectFactory.createRFonts();
        pararpr10.setRFonts(rfonts21);
        rfonts21.setAscii( "Times New Roman");
        rfonts21.setCs( "Times New Roman");
        rfonts21.setEastAsia( "Times New Roman");
        rfonts21.setHAnsi( "Times New Roman");
        // Create object for szCs
        HpsMeasure hpsmeasure34 = wmlObjectFactory.createHpsMeasure();
        pararpr10.setSzCs(hpsmeasure34);
        hpsmeasure34.setVal( BigInteger.valueOf( 24) );
        // Create object for r
        R r12 = wmlObjectFactory.createR();
        p11.getContent().add( r12);
        // Create object for rPr
        RPr rpr12 = wmlObjectFactory.createRPr();
        r12.setRPr(rpr12);
        // Create object for sz
        HpsMeasure hpsmeasure35 = wmlObjectFactory.createHpsMeasure();
        rpr12.setSz(hpsmeasure35);
        hpsmeasure35.setVal( BigInteger.valueOf( 24) );
        // Create object for rFonts
        RFonts rfonts22 = wmlObjectFactory.createRFonts();
        rpr12.setRFonts(rfonts22);
        rfonts22.setAscii( "Times New Roman");
        rfonts22.setCs( "Times New Roman");
        rfonts22.setEastAsia( "Times New Roman");
        rfonts22.setHAnsi( "Times New Roman");
        // Create object for szCs
        HpsMeasure hpsmeasure36 = wmlObjectFactory.createHpsMeasure();
        rpr12.setSzCs(hpsmeasure36);
        hpsmeasure36.setVal( BigInteger.valueOf( 24) );
        // Create object for t (wrapped in JAXBElement)
        Text text12 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped12 = wmlObjectFactory.createRT(text12);
        r12.getContent().add( textWrapped12);
        text12.setValue( assessment.getProfile().getTopic());
        // Create object for tcPr
        TcPr tcpr8 = wmlObjectFactory.createTcPr();
        tc8.setTcPr(tcpr8);
        // Create object for vAlign
        CTVerticalJc verticaljc8 = wmlObjectFactory.createCTVerticalJc();
        tcpr8.setVAlign(verticaljc8);
        verticaljc8.setVal(org.docx4j.wml.STVerticalJc.BOTTOM);
        // Create object for gridSpan
        TcPrInner.GridSpan tcprinnergridspan = wmlObjectFactory.createTcPrInnerGridSpan();
        tcpr8.setGridSpan(tcprinnergridspan);
        tcprinnergridspan.setVal( BigInteger.valueOf( 5) );
        // Create object for tcW
        TblWidth tblwidth8 = wmlObjectFactory.createTblWidth();
        tcpr8.setTcW(tblwidth8);
        tblwidth8.setW( BigInteger.valueOf( 8647) );
        tblwidth8.setType( "dxa");
        // Create object for tcBorders
        TcPrInner.TcBorders tcprinnertcborders8 = wmlObjectFactory.createTcPrInnerTcBorders();
        tcpr8.setTcBorders(tcprinnertcborders8);
        // Create object for top
        CTBorder border29 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders8.setTop(border29);
        border29.setVal(org.docx4j.wml.STBorder.NIL);
        // Create object for bottom
        CTBorder border30 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders8.setBottom(border30);
        border30.setVal(org.docx4j.wml.STBorder.SINGLE);
        border30.setSz( BigInteger.valueOf( 4) );
        border30.setColor( "000000");
        border30.setSpace( BigInteger.valueOf( 0) );
        // Create object for left
        CTBorder border31 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders8.setLeft(border31);
        border31.setVal(org.docx4j.wml.STBorder.NIL);
        // Create object for right
        CTBorder border32 = wmlObjectFactory.createCTBorder();
        tcprinnertcborders8.setRight(border32);
        border32.setVal(org.docx4j.wml.STBorder.NIL);
        // Create object for trPr
        TrPr trpr2 = wmlObjectFactory.createTrPr();
        tr2.setTrPr(trpr2);
        // Create object for trHeight (wrapped in JAXBElement)
        CTHeight height2 = wmlObjectFactory.createCTHeight();
        JAXBElement<org.docx4j.wml.CTHeight> heightWrapped2 = wmlObjectFactory.createCTTrPrBaseTrHeight(height2);
        trpr2.getCnfStyleOrDivIdOrGridBefore().add( heightWrapped2);
        height2.setVal( BigInteger.valueOf( 680) );
        // Create object for jc (wrapped in JAXBElement)
        Jc jc6 = wmlObjectFactory.createJc();
        JAXBElement<org.docx4j.wml.Jc> jcWrapped2 = wmlObjectFactory.createCTTrPrBaseJc(jc6);
        trpr2.getCnfStyleOrDivIdOrGridBefore().add( jcWrapped2);
        jc6.setVal(org.docx4j.wml.JcEnumeration.CENTER);
        // Create object for tblPr
        TblPr tblpr = wmlObjectFactory.createTblPr();
        tbl.setTblPr(tblpr);
        // Create object for tblStyle
        CTTblPrBase.TblStyle tblprbasetblstyle = wmlObjectFactory.createCTTblPrBaseTblStyle();
        tblpr.setTblStyle(tblprbasetblstyle);
        tblprbasetblstyle.setVal( "a");
        // Create object for tblBorders
        TblBorders tblborders = wmlObjectFactory.createTblBorders();
        tblpr.setTblBorders(tblborders);
        // Create object for insideH
        CTBorder border33 = wmlObjectFactory.createCTBorder();
        tblborders.setInsideH(border33);
        border33.setVal(org.docx4j.wml.STBorder.SINGLE);
        border33.setSz( BigInteger.valueOf( 4) );
        border33.setColor( "000000");
        border33.setSpace( BigInteger.valueOf( 0) );
        // Create object for top
        CTBorder border34 = wmlObjectFactory.createCTBorder();
        tblborders.setTop(border34);
        border34.setVal(org.docx4j.wml.STBorder.SINGLE);
        border34.setSz( BigInteger.valueOf( 4) );
        border34.setColor( "000000");
        border34.setSpace( BigInteger.valueOf( 0) );
        // Create object for bottom
        CTBorder border35 = wmlObjectFactory.createCTBorder();
        tblborders.setBottom(border35);
        border35.setVal(org.docx4j.wml.STBorder.SINGLE);
        border35.setSz( BigInteger.valueOf( 4) );
        border35.setColor( "000000");
        border35.setSpace( BigInteger.valueOf( 0) );
        // Create object for insideV
        CTBorder border36 = wmlObjectFactory.createCTBorder();
        tblborders.setInsideV(border36);
        border36.setVal(org.docx4j.wml.STBorder.SINGLE);
        border36.setSz( BigInteger.valueOf( 4) );
        border36.setColor( "000000");
        border36.setSpace( BigInteger.valueOf( 0) );
        // Create object for right
        CTBorder border37 = wmlObjectFactory.createCTBorder();
        tblborders.setRight(border37);
        border37.setVal(org.docx4j.wml.STBorder.SINGLE);
        border37.setSz( BigInteger.valueOf( 4) );
        border37.setColor( "000000");
        border37.setSpace( BigInteger.valueOf( 0) );
        // Create object for left
        CTBorder border38 = wmlObjectFactory.createCTBorder();
        tblborders.setLeft(border38);
        border38.setVal(org.docx4j.wml.STBorder.SINGLE);
        border38.setSz( BigInteger.valueOf( 4) );
        border38.setColor( "000000");
        border38.setSpace( BigInteger.valueOf( 0) );
        // Create object for jc
        Jc jc7 = wmlObjectFactory.createJc();
        tblpr.setJc(jc7);
        jc7.setVal(org.docx4j.wml.JcEnumeration.CENTER);
        // Create object for tblW
        TblWidth tblwidth9 = wmlObjectFactory.createTblWidth();
        tblpr.setTblW(tblwidth9);
        tblwidth9.setW( BigInteger.valueOf( 10207) );
        tblwidth9.setType( "dxa");
        // Create object for tblLayout
        CTTblLayoutType tbllayouttype = wmlObjectFactory.createCTTblLayoutType();
        tblpr.setTblLayout(tbllayouttype);
        tbllayouttype.setType(org.docx4j.wml.STTblLayoutType.FIXED);
        // Create object for tblLook
        CTTblLook tbllook = wmlObjectFactory.createCTTblLook();
        tblpr.setTblLook(tbllook);
        tbllook.setVal( "0400");
        tbllook.setFirstColumn(org.docx4j.sharedtypes.STOnOff.ZERO);
        tbllook.setFirstRow(org.docx4j.sharedtypes.STOnOff.ZERO);
        tbllook.setLastColumn(org.docx4j.sharedtypes.STOnOff.ZERO);
        tbllook.setLastRow(org.docx4j.sharedtypes.STOnOff.ZERO);
        tbllook.setNoHBand(org.docx4j.sharedtypes.STOnOff.ZERO);
        tbllook.setNoVBand(org.docx4j.sharedtypes.STOnOff.ONE);
        // Create object for tblGrid
        TblGrid tblgrid = wmlObjectFactory.createTblGrid();
        tbl.setTblGrid(tblgrid);
        // Create object for gridCol
        TblGridCol tblgridcol = wmlObjectFactory.createTblGridCol();
        tblgrid.getGridCol().add( tblgridcol);
        tblgridcol.setW( BigInteger.valueOf( 1560) );
        // Create object for gridCol
        TblGridCol tblgridcol2 = wmlObjectFactory.createTblGridCol();
        tblgrid.getGridCol().add( tblgridcol2);
        tblgridcol2.setW( BigInteger.valueOf( 2158) );
        // Create object for gridCol
        TblGridCol tblgridcol3 = wmlObjectFactory.createTblGridCol();
        tblgrid.getGridCol().add( tblgridcol3);
        tblgridcol3.setW( BigInteger.valueOf( 1503) );
        // Create object for gridCol
        TblGridCol tblgridcol4 = wmlObjectFactory.createTblGridCol();
        tblgrid.getGridCol().add( tblgridcol4);
        tblgridcol4.setW( BigInteger.valueOf( 1503) );
        // Create object for gridCol
        TblGridCol tblgridcol5 = wmlObjectFactory.createTblGridCol();
        tblgrid.getGridCol().add( tblgridcol5);
        tblgridcol5.setW( BigInteger.valueOf( 1503) );
        // Create object for gridCol
        TblGridCol tblgridcol6 = wmlObjectFactory.createTblGridCol();
        tblgrid.getGridCol().add( tblgridcol6);
        tblgridcol6.setW( BigInteger.valueOf( 1980) );
        // Create object for p
        P p12 = wmlObjectFactory.createP();
        body.getContent().add( p12);
        // Create object for p
        P p13 = wmlObjectFactory.createP();
        body.getContent().add( p13);
        // Create object for pPr
        PPr ppr11 = wmlObjectFactory.createPPr();
        p13.setPPr(ppr11);
        // Create object for rPr
        ParaRPr pararpr11 = wmlObjectFactory.createParaRPr();
        ppr11.setRPr(pararpr11);
        // Create object for sz
        HpsMeasure hpsmeasure37 = wmlObjectFactory.createHpsMeasure();
        pararpr11.setSz(hpsmeasure37);
        hpsmeasure37.setVal( BigInteger.valueOf( 24) );
        // Create object for rFonts
        RFonts rfonts23 = wmlObjectFactory.createRFonts();
        pararpr11.setRFonts(rfonts23);
        rfonts23.setAscii( "Times");
        rfonts23.setCs( "Times");
        rfonts23.setEastAsia( "Times");
        rfonts23.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue3 = wmlObjectFactory.createBooleanDefaultTrue();
        pararpr11.setB(booleandefaulttrue3);
        // Create object for szCs
        HpsMeasure hpsmeasure38 = wmlObjectFactory.createHpsMeasure();
        pararpr11.setSzCs(hpsmeasure38);
        hpsmeasure38.setVal( BigInteger.valueOf( 24) );
        // Create object for spacing
        PPrBase.Spacing pprbasespacing = wmlObjectFactory.createPPrBaseSpacing();
        ppr11.setSpacing(pprbasespacing);
        pprbasespacing.setAfter( BigInteger.valueOf( 0) );
        pprbasespacing.setLine( BigInteger.valueOf( 240) );
        pprbasespacing.setLineRule(org.docx4j.wml.STLineSpacingRule.AUTO);

        //------------------------------------------------------------------------------------------
        // todo content and idea - [table]

        // Create object for tbl (wrapped in JAXBElement)
        Tbl tbl2 = wmlObjectFactory.createTbl();
        JAXBElement<org.docx4j.wml.Tbl> tblWrapped2 = wmlObjectFactory.createBodyTbl(tbl2);
        body.getContent().add( tblWrapped2);
        // Create object for tr
        Tr tr3 = wmlObjectFactory.createTr();
        tbl2.getContent().add( tr3);
        // Create object for tc (wrapped in JAXBElement)
        Tc tc9 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped9 = wmlObjectFactory.createTrTc(tc9);
        tr3.getContent().add( tcWrapped9);
        // Create object for p
        P p14 = wmlObjectFactory.createP();
        tc9.getContent().add( p14);
        // Create object for pPr
        PPr ppr12 = wmlObjectFactory.createPPr();
        p14.setPPr(ppr12);
        // Create object for rPr
        ParaRPr pararpr12 = wmlObjectFactory.createParaRPr();
        ppr12.setRPr(pararpr12);
        // Create object for rFonts
        RFonts rfonts24 = wmlObjectFactory.createRFonts();
        pararpr12.setRFonts(rfonts24);
        rfonts24.setAscii( "Times");
        rfonts24.setCs( "Times");
        rfonts24.setEastAsia( "Times");
        rfonts24.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue4 = wmlObjectFactory.createBooleanDefaultTrue();
        pararpr12.setB(booleandefaulttrue4);
        // Create object for r
        R r13 = wmlObjectFactory.createR();
        p14.getContent().add( r13);
        // Create object for rPr
        RPr rpr13 = wmlObjectFactory.createRPr();
        r13.setRPr(rpr13);
        // Create object for rFonts
        RFonts rfonts25 = wmlObjectFactory.createRFonts();
        rpr13.setRFonts(rfonts25);
        rfonts25.setAscii( "Times");
        rfonts25.setCs( "Times");
        rfonts25.setEastAsia( "Times");
        rfonts25.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue5 = wmlObjectFactory.createBooleanDefaultTrue();
        rpr13.setB(booleandefaulttrue5);
        // Create object for t (wrapped in JAXBElement)
        Text text13 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped13 = wmlObjectFactory.createRT(text13);
        r13.getContent().add( textWrapped13);
        text13.setValue( "Content structure / ideas");
        // Create object for tcPr
        TcPr tcpr9 = wmlObjectFactory.createTcPr();
        tc9.setTcPr(tcpr9);
        // Create object for tcW
        TblWidth tblwidth10 = wmlObjectFactory.createTblWidth();
        tcpr9.setTcW(tblwidth10);
        tblwidth10.setW( BigInteger.valueOf( 1815) );
        tblwidth10.setType( "dxa");
        // Create object for tc (wrapped in JAXBElement)
        Tc tc10 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped10 = wmlObjectFactory.createTrTc(tc10);
        tr3.getContent().add( tcWrapped10);
        // Create object for p
        P p15 = wmlObjectFactory.createP();
        tc10.getContent().add( p15);
        // Create object for pPr
        PPr ppr13 = wmlObjectFactory.createPPr();
        p15.setPPr(ppr13);
        // Create object for rPr
        ParaRPr pararpr13 = wmlObjectFactory.createParaRPr();
        ppr13.setRPr(pararpr13);
        // Create object for rFonts
        RFonts rfonts26 = wmlObjectFactory.createRFonts();
        pararpr13.setRFonts(rfonts26);
        rfonts26.setAscii( "Times");
        rfonts26.setCs( "Times");
        rfonts26.setEastAsia( "Times");
        rfonts26.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue6 = wmlObjectFactory.createBooleanDefaultTrue();
        pararpr13.setB(booleandefaulttrue6);
        // Create object for r
        R r14 = wmlObjectFactory.createR();
        p15.getContent().add( r14);
        // Create object for rPr
        RPr rpr14 = wmlObjectFactory.createRPr();
        r14.setRPr(rpr14);
        // Create object for rFonts
        RFonts rfonts27 = wmlObjectFactory.createRFonts();
        rpr14.setRFonts(rfonts27);
        rfonts27.setAscii( "Times");
        rfonts27.setCs( "Times");
        rfonts27.setEastAsia( "Times");
        rfonts27.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue7 = wmlObjectFactory.createBooleanDefaultTrue();
        rpr14.setB(booleandefaulttrue7);
        // Create object for t (wrapped in JAXBElement)
        Text text14 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped14 = wmlObjectFactory.createRT(text14);
        r14.getContent().add( textWrapped14);
        text14.setValue( "Excellent (4)");
        // Create object for tcPr
        TcPr tcpr10 = wmlObjectFactory.createTcPr();
        tc10.setTcPr(tcpr10);
        // Create object for tcW
        TblWidth tblwidth11 = wmlObjectFactory.createTblWidth();
        tcpr10.setTcW(tblwidth11);
        tblwidth11.setW( BigInteger.valueOf( 2098) );
        tblwidth11.setType( "dxa");
        // Create object for tc (wrapped in JAXBElement)
        Tc tc11 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped11 = wmlObjectFactory.createTrTc(tc11);
        tr3.getContent().add( tcWrapped11);
        // Create object for p
        P p16 = wmlObjectFactory.createP();
        tc11.getContent().add( p16);
        // Create object for pPr
        PPr ppr14 = wmlObjectFactory.createPPr();
        p16.setPPr(ppr14);
        // Create object for rPr
        ParaRPr pararpr14 = wmlObjectFactory.createParaRPr();
        ppr14.setRPr(pararpr14);
        // Create object for rFonts
        RFonts rfonts28 = wmlObjectFactory.createRFonts();
        pararpr14.setRFonts(rfonts28);
        rfonts28.setAscii( "Times");
        rfonts28.setCs( "Times");
        rfonts28.setEastAsia( "Times");
        rfonts28.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue8 = wmlObjectFactory.createBooleanDefaultTrue();
        pararpr14.setB(booleandefaulttrue8);
        // Create object for r
        R r15 = wmlObjectFactory.createR();
        p16.getContent().add( r15);
        // Create object for rPr
        RPr rpr15 = wmlObjectFactory.createRPr();
        r15.setRPr(rpr15);
        // Create object for rFonts
        RFonts rfonts29 = wmlObjectFactory.createRFonts();
        rpr15.setRFonts(rfonts29);
        rfonts29.setAscii( "Times");
        rfonts29.setCs( "Times");
        rfonts29.setEastAsia( "Times");
        rfonts29.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue9 = wmlObjectFactory.createBooleanDefaultTrue();
        rpr15.setB(booleandefaulttrue9);
        // Create object for t (wrapped in JAXBElement)
        Text text15 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped15 = wmlObjectFactory.createRT(text15);
        r15.getContent().add( textWrapped15);
        text15.setValue( "Good (3)");
        // Create object for tcPr
        TcPr tcpr11 = wmlObjectFactory.createTcPr();
        tc11.setTcPr(tcpr11);
        // Create object for tcW
        TblWidth tblwidth12 = wmlObjectFactory.createTblWidth();
        tcpr11.setTcW(tblwidth12);
        tblwidth12.setW( BigInteger.valueOf( 2098) );
        tblwidth12.setType( "dxa");
        // Create object for tc (wrapped in JAXBElement)
        Tc tc12 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped12 = wmlObjectFactory.createTrTc(tc12);
        tr3.getContent().add( tcWrapped12);
        // Create object for p
        P p17 = wmlObjectFactory.createP();
        tc12.getContent().add( p17);
        // Create object for pPr
        PPr ppr15 = wmlObjectFactory.createPPr();
        p17.setPPr(ppr15);
        // Create object for rPr
        ParaRPr pararpr15 = wmlObjectFactory.createParaRPr();
        ppr15.setRPr(pararpr15);
        // Create object for rFonts
        RFonts rfonts30 = wmlObjectFactory.createRFonts();
        pararpr15.setRFonts(rfonts30);
        rfonts30.setAscii( "Times");
        rfonts30.setCs( "Times");
        rfonts30.setEastAsia( "Times");
        rfonts30.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue10 = wmlObjectFactory.createBooleanDefaultTrue();
        pararpr15.setB(booleandefaulttrue10);
        // Create object for r
        R r16 = wmlObjectFactory.createR();
        p17.getContent().add( r16);
        // Create object for rPr
        RPr rpr16 = wmlObjectFactory.createRPr();
        r16.setRPr(rpr16);
        // Create object for rFonts
        RFonts rfonts31 = wmlObjectFactory.createRFonts();
        rpr16.setRFonts(rfonts31);
        rfonts31.setAscii( "Times");
        rfonts31.setCs( "Times");
        rfonts31.setEastAsia( "Times");
        rfonts31.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue11 = wmlObjectFactory.createBooleanDefaultTrue();
        rpr16.setB(booleandefaulttrue11);
        // Create object for t (wrapped in JAXBElement)
        Text text16 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped16 = wmlObjectFactory.createRT(text16);
        r16.getContent().add( textWrapped16);
        text16.setValue( "Fair (2)");
        // Create object for tcPr
        TcPr tcpr12 = wmlObjectFactory.createTcPr();
        tc12.setTcPr(tcpr12);
        // Create object for tcW
        TblWidth tblwidth13 = wmlObjectFactory.createTblWidth();
        tcpr12.setTcW(tblwidth13);
        tblwidth13.setW( BigInteger.valueOf( 2098) );
        tblwidth13.setType( "dxa");
        // Create object for tc (wrapped in JAXBElement)
        Tc tc13 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped13 = wmlObjectFactory.createTrTc(tc13);
        tr3.getContent().add( tcWrapped13);
        // Create object for p
        P p18 = wmlObjectFactory.createP();
        tc13.getContent().add( p18);
        // Create object for pPr
        PPr ppr16 = wmlObjectFactory.createPPr();
        p18.setPPr(ppr16);
        // Create object for rPr
        ParaRPr pararpr16 = wmlObjectFactory.createParaRPr();
        ppr16.setRPr(pararpr16);
        // Create object for rFonts
        RFonts rfonts32 = wmlObjectFactory.createRFonts();
        pararpr16.setRFonts(rfonts32);
        rfonts32.setAscii( "Times");
        rfonts32.setCs( "Times");
        rfonts32.setEastAsia( "Times");
        rfonts32.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue12 = wmlObjectFactory.createBooleanDefaultTrue();
        pararpr16.setB(booleandefaulttrue12);
        // Create object for r
        R r17 = wmlObjectFactory.createR();
        p18.getContent().add( r17);
        // Create object for rPr
        RPr rpr17 = wmlObjectFactory.createRPr();
        r17.setRPr(rpr17);
        // Create object for rFonts
        RFonts rfonts33 = wmlObjectFactory.createRFonts();
        rpr17.setRFonts(rfonts33);
        rfonts33.setAscii( "Times");
        rfonts33.setCs( "Times");
        rfonts33.setEastAsia( "Times");
        rfonts33.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue13 = wmlObjectFactory.createBooleanDefaultTrue();
        rpr17.setB(booleandefaulttrue13);
        // Create object for t (wrapped in JAXBElement)
        Text text17 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped17 = wmlObjectFactory.createRT(text17);
        r17.getContent().add( textWrapped17);
        text17.setValue( "Poor (1)");
        // Create object for tcPr
        TcPr tcpr13 = wmlObjectFactory.createTcPr();
        tc13.setTcPr(tcpr13);
        // Create object for tcW
        TblWidth tblwidth14 = wmlObjectFactory.createTblWidth();
        tcpr13.setTcW(tblwidth14);
        tblwidth14.setW( BigInteger.valueOf( 2098) );
        tblwidth14.setType( "dxa");

        //------------------------------------------------------------------------------------------
        // todo focus - [tableRow]

        // Create object for trPr
        TrPr trpr3 = wmlObjectFactory.createTrPr();
        tr3.setTrPr(trpr3);
        // Create object for jc (wrapped in JAXBElement)
        Jc jc8 = wmlObjectFactory.createJc();
        JAXBElement<org.docx4j.wml.Jc> jcWrapped3 = wmlObjectFactory.createCTTrPrBaseJc(jc8);
        trpr3.getCnfStyleOrDivIdOrGridBefore().add( jcWrapped3);
        jc8.setVal(org.docx4j.wml.JcEnumeration.CENTER);
        // Create object for tr
        Tr tr4 = wmlObjectFactory.createTr();
        tbl2.getContent().add( tr4);
        // Create object for tc (wrapped in JAXBElement)
        Tc tc14 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped14 = wmlObjectFactory.createTrTc(tc14);
        tr4.getContent().add( tcWrapped14);
        // Create object for p
        P p19 = wmlObjectFactory.createP();
        tc14.getContent().add( p19);
        // Create object for pPr
        PPr ppr17 = wmlObjectFactory.createPPr();
        p19.setPPr(ppr17);
        // Create object for rPr
        ParaRPr pararpr17 = wmlObjectFactory.createParaRPr();
        ppr17.setRPr(pararpr17);
        // Create object for rFonts
        RFonts rfonts34 = wmlObjectFactory.createRFonts();
        pararpr17.setRFonts(rfonts34);
        rfonts34.setAscii( "Times New Roman");
        rfonts34.setCs( "Times New Roman");
        rfonts34.setEastAsia( "Times New Roman");
        rfonts34.setHAnsi( "Times New Roman");
        // Create object for r
        R r18 = wmlObjectFactory.createR();
        p19.getContent().add( r18);
        // Create object for rPr
        RPr rpr18 = wmlObjectFactory.createRPr();
        r18.setRPr(rpr18);
        // Create object for rFonts
        RFonts rfonts35 = wmlObjectFactory.createRFonts();
        rpr18.setRFonts(rfonts35);
        rfonts35.setAscii( "Times New Roman");
        rfonts35.setCs( "Times New Roman");
        rfonts35.setEastAsia( "Times New Roman");
        rfonts35.setHAnsi( "Times New Roman");
        // Create object for t (wrapped in JAXBElement)
        Text text18 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped18 = wmlObjectFactory.createRT(text18);
        r18.getContent().add( textWrapped18);
        text18.setValue( "Focus");
        // Create object for p
        P p20 = wmlObjectFactory.createP();
        tc14.getContent().add( p20);
        // Create object for pPr
        PPr ppr18 = wmlObjectFactory.createPPr();
        p20.setPPr(ppr18);
        // Create object for rPr
        ParaRPr pararpr18 = wmlObjectFactory.createParaRPr();
        ppr18.setRPr(pararpr18);
        // Create object for rFonts
        RFonts rfonts36 = wmlObjectFactory.createRFonts();
        pararpr18.setRFonts(rfonts36);
        rfonts36.setAscii( "Times New Roman");
        rfonts36.setCs( "Times New Roman");
        rfonts36.setEastAsia( "Times New Roman");
        rfonts36.setHAnsi( "Times New Roman");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue14 = wmlObjectFactory.createBooleanDefaultTrue();
        pararpr18.setB(booleandefaulttrue14);
        // Create object for r
        R r19 = wmlObjectFactory.createR();
        p20.getContent().add( r19);
        // Create object for rPr
        RPr rpr19 = wmlObjectFactory.createRPr();
        r19.setRPr(rpr19);
        // Create object for rFonts
        RFonts rfonts37 = wmlObjectFactory.createRFonts();
        rpr19.setRFonts(rfonts37);
        rfonts37.setAscii( "Times New Roman");
        rfonts37.setCs( "Times New Roman");
        rfonts37.setEastAsia( "Times New Roman");
        rfonts37.setHAnsi( "Times New Roman");
        // Create object for t (wrapped in JAXBElement)
        Text text19 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped19 = wmlObjectFactory.createRT(text19);
        r19.getContent().add( textWrapped19);
        text19.setValue( " ");
        text19.setSpace( "preserve");

        // Create object for tcPr
        TcPr tcpr14 = wmlObjectFactory.createTcPr();
        tc14.setTcPr(tcpr14);
        // Create object for tcW
        TblWidth tblwidth15 = wmlObjectFactory.createTblWidth();
        tcpr14.setTcW(tblwidth15);
        tblwidth15.setW( BigInteger.valueOf( 1815) );
        tblwidth15.setType( "dxa");

        //------------------------------------------------------------------------------------------
        // todo content - focus row
        makeUpContentTableFocusRow(assessment, wmlObjectFactory, tr4);

        //------------------------------------------------------------------------------------------
        // todo organization - [tableRow]

        // Create object for trPr
        TrPr trpr4 = wmlObjectFactory.createTrPr();
        tr4.setTrPr(trpr4);
        // Create object for jc (wrapped in JAXBElement)
        Jc jc9 = wmlObjectFactory.createJc();
        JAXBElement<org.docx4j.wml.Jc> jcWrapped4 = wmlObjectFactory.createCTTrPrBaseJc(jc9);
        trpr4.getCnfStyleOrDivIdOrGridBefore().add( jcWrapped4);
        jc9.setVal(org.docx4j.wml.JcEnumeration.CENTER);
        // Create object for tr
        Tr tr5 = wmlObjectFactory.createTr();
        tbl2.getContent().add( tr5);
        // Create object for tc (wrapped in JAXBElement)
        Tc tc19 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped19 = wmlObjectFactory.createTrTc(tc19);
        tr5.getContent().add( tcWrapped19);
        // Create object for p
        P p28 = wmlObjectFactory.createP();
        tc19.getContent().add( p28);
        // Create object for pPr
        PPr ppr26 = wmlObjectFactory.createPPr();
        p28.setPPr(ppr26);
        // Create object for rPr
        ParaRPr pararpr26 = wmlObjectFactory.createParaRPr();
        ppr26.setRPr(pararpr26);
        // Create object for rFonts
        RFonts rfonts50 = wmlObjectFactory.createRFonts();
        pararpr26.setRFonts(rfonts50);
        rfonts50.setAscii( "Times New Roman");
        rfonts50.setCs( "Times New Roman");
        rfonts50.setEastAsia( "Times New Roman");
        rfonts50.setHAnsi( "Times New Roman");
        // Create object for r
        R r25 = wmlObjectFactory.createR();
        p28.getContent().add( r25);
        // Create object for rPr
        RPr rpr25 = wmlObjectFactory.createRPr();
        r25.setRPr(rpr25);
        // Create object for rFonts
        RFonts rfonts51 = wmlObjectFactory.createRFonts();
        rpr25.setRFonts(rfonts51);
        rfonts51.setAscii( "Times New Roman");
        rfonts51.setCs( "Times New Roman");
        rfonts51.setEastAsia( "Times New Roman");
        rfonts51.setHAnsi( "Times New Roman");
        // Create object for t (wrapped in JAXBElement)
        Text text25 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped25 = wmlObjectFactory.createRT(text25);
        r25.getContent().add( textWrapped25);
        text25.setValue( "Organization");
        // Create object for tcPr
        TcPr tcpr19 = wmlObjectFactory.createTcPr();
        tc19.setTcPr(tcpr19);
        // Create object for tcW
        TblWidth tblwidth20 = wmlObjectFactory.createTblWidth();
        tcpr19.setTcW(tblwidth20);
        tblwidth20.setW( BigInteger.valueOf( 1815) );
        tblwidth20.setType( "dxa");

//        //------------------------------------------------------------------------------------------
//        // todo organization - excellent
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc20 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped20 = wmlObjectFactory.createTrTc(tc20);
//        tr5.getContent().add( tcWrapped20);
//        // Create object for p
//        P p29 = wmlObjectFactory.createP();
//        tc20.getContent().add( p29);
//        // Create object for pPr
//        PPr ppr27 = wmlObjectFactory.createPPr();
//        p29.setPPr(ppr27);
//        // Create object for rPr
//        ParaRPr pararpr27 = wmlObjectFactory.createParaRPr();
//        ppr27.setRPr(pararpr27);
//        // Create object for rFonts
//        RFonts rfonts52 = wmlObjectFactory.createRFonts();
//        pararpr27.setRFonts(rfonts52);
//        rfonts52.setAscii( "Times New Roman");
//        rfonts52.setCs( "Times New Roman");
//        rfonts52.setEastAsia( "Times New Roman");
//        rfonts52.setHAnsi( "Times New Roman");
//        // Create object for b
//        BooleanDefaultTrue booleandefaulttrue18 = wmlObjectFactory.createBooleanDefaultTrue();
//        pararpr27.setB(booleandefaulttrue18);
//        // Create object for spacing
//        PPrBase.Spacing pprbasespacing3 = wmlObjectFactory.createPPrBaseSpacing();
//        ppr27.setSpacing(pprbasespacing3);
//        pprbasespacing3.setAfter( BigInteger.valueOf( 120) );
//        // Create object for r
//        R r26 = wmlObjectFactory.createR();
//        p29.getContent().add( r26);
//        // Create object for rPr
//        RPr rpr26 = wmlObjectFactory.createRPr();
//        r26.setRPr(rpr26);
//        // Create object for rFonts
//        RFonts rfonts53 = wmlObjectFactory.createRFonts();
//        rpr26.setRFonts(rfonts53);
//        rfonts53.setAscii( "Times New Roman");
//        rfonts53.setCs( "Times New Roman");
//        rfonts53.setEastAsia( "Times New Roman");
//        rfonts53.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text26 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped26 = wmlObjectFactory.createRT(text26);
//        r26.getContent().add( textWrapped26);
//        text26.setValue( "Student presents information in logical, interesting sequence that audience follows");
//        // Create object for tcPr
//        TcPr tcpr20 = wmlObjectFactory.createTcPr();
//        tc20.setTcPr(tcpr20);
//        // Create object for tcW
//        TblWidth tblwidth21 = wmlObjectFactory.createTblWidth();
//        tcpr20.setTcW(tblwidth21);
//        tblwidth21.setW( BigInteger.valueOf( 2098) );
//        tblwidth21.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo organization - good
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc21 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped21 = wmlObjectFactory.createTrTc(tc21);
//        tr5.getContent().add( tcWrapped21);
//        // Create object for p
//        P p30 = wmlObjectFactory.createP();
//        tc21.getContent().add( p30);
//        // Create object for pPr
//        PPr ppr28 = wmlObjectFactory.createPPr();
//        p30.setPPr(ppr28);
//        // Create object for rPr
//        ParaRPr pararpr28 = wmlObjectFactory.createParaRPr();
//        ppr28.setRPr(pararpr28);
//        // Create object for rFonts
//        RFonts rfonts54 = wmlObjectFactory.createRFonts();
//        pararpr28.setRFonts(rfonts54);
//        rfonts54.setAscii( "Times New Roman");
//        rfonts54.setCs( "Times New Roman");
//        rfonts54.setEastAsia( "Times New Roman");
//        rfonts54.setHAnsi( "Times New Roman");
//        // Create object for b
//        BooleanDefaultTrue booleandefaulttrue19 = wmlObjectFactory.createBooleanDefaultTrue();
//        pararpr28.setB(booleandefaulttrue19);
//        // Create object for r
//        R r27 = wmlObjectFactory.createR();
//        p30.getContent().add( r27);
//        // Create object for rPr
//        RPr rpr27 = wmlObjectFactory.createRPr();
//        r27.setRPr(rpr27);
//        // Create object for rFonts
//        RFonts rfonts55 = wmlObjectFactory.createRFonts();
//        rpr27.setRFonts(rfonts55);
//        rfonts55.setAscii( "Times New Roman");
//        rfonts55.setCs( "Times New Roman");
//        rfonts55.setEastAsia( "Times New Roman");
//        rfonts55.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text27 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped27 = wmlObjectFactory.createRT(text27);
//        r27.getContent().add( textWrapped27);
//        text27.setValue( "Student presents information in logical sequence that audience can follow");
//        // Create object for tcPr
//        TcPr tcpr21 = wmlObjectFactory.createTcPr();
//        tc21.setTcPr(tcpr21);
//        // Create object for tcW
//        TblWidth tblwidth22 = wmlObjectFactory.createTblWidth();
//        tcpr21.setTcW(tblwidth22);
//        tblwidth22.setW( BigInteger.valueOf( 2098) );
//        tblwidth22.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo organization - fair
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc22 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped22 = wmlObjectFactory.createTrTc(tc22);
//        tr5.getContent().add( tcWrapped22);
//        // Create object for p
//        P p31 = wmlObjectFactory.createP();
//        tc22.getContent().add( p31);
//        // Create object for pPr
//        PPr ppr29 = wmlObjectFactory.createPPr();
//        p31.setPPr(ppr29);
//        // Create object for rPr
//        ParaRPr pararpr29 = wmlObjectFactory.createParaRPr();
//        ppr29.setRPr(pararpr29);
//        // Create object for rFonts
//        RFonts rfonts56 = wmlObjectFactory.createRFonts();
//        pararpr29.setRFonts(rfonts56);
//        rfonts56.setAscii( "Times New Roman");
//        rfonts56.setCs( "Times New Roman");
//        rfonts56.setEastAsia( "Times New Roman");
//        rfonts56.setHAnsi( "Times New Roman");
//        // Create object for r
//        R r28 = wmlObjectFactory.createR();
//        p31.getContent().add( r28);
//        // Create object for rPr
//        RPr rpr28 = wmlObjectFactory.createRPr();
//        r28.setRPr(rpr28);
//        // Create object for rFonts
//        RFonts rfonts57 = wmlObjectFactory.createRFonts();
//        rpr28.setRFonts(rfonts57);
//        rfonts57.setAscii( "Times New Roman");
//        rfonts57.setCs( "Times New Roman");
//        rfonts57.setEastAsia( "Times New Roman");
//        rfonts57.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text28 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped28 = wmlObjectFactory.createRT(text28);
//        r28.getContent().add( textWrapped28);
//        text28.setValue( "Audience has difficulty following because student jumps around.");
//        // Create object for tcPr
//        TcPr tcpr22 = wmlObjectFactory.createTcPr();
//        tc22.setTcPr(tcpr22);
//        // Create object for tcW
//        TblWidth tblwidth23 = wmlObjectFactory.createTblWidth();
//        tcpr22.setTcW(tblwidth23);
//        tblwidth23.setW( BigInteger.valueOf( 2098) );
//        tblwidth23.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo organization - poor
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc23 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped23 = wmlObjectFactory.createTrTc(tc23);
//        tr5.getContent().add( tcWrapped23);
//        // Create object for p
//        P p32 = wmlObjectFactory.createP();
//        tc23.getContent().add( p32);
//        // Create object for pPr
//        PPr ppr30 = wmlObjectFactory.createPPr();
//        p32.setPPr(ppr30);
//        // Create object for rPr
//        ParaRPr pararpr30 = wmlObjectFactory.createParaRPr();
//        ppr30.setRPr(pararpr30);
//        // Create object for rFonts
//        RFonts rfonts58 = wmlObjectFactory.createRFonts();
//        pararpr30.setRFonts(rfonts58);
//        rfonts58.setAscii( "Times New Roman");
//        rfonts58.setCs( "Times New Roman");
//        rfonts58.setEastAsia( "Times New Roman");
//        rfonts58.setHAnsi( "Times New Roman");
//        // Create object for b
//        BooleanDefaultTrue booleandefaulttrue20 = wmlObjectFactory.createBooleanDefaultTrue();
//        pararpr30.setB(booleandefaulttrue20);
//        // Create object for r
//        R r29 = wmlObjectFactory.createR();
//        p32.getContent().add( r29);
//        // Create object for rPr
//        RPr rpr29 = wmlObjectFactory.createRPr();
//        r29.setRPr(rpr29);
//        // Create object for rFonts
//        RFonts rfonts59 = wmlObjectFactory.createRFonts();
//        rpr29.setRFonts(rfonts59);
//        rfonts59.setAscii( "Times New Roman");
//        rfonts59.setCs( "Times New Roman");
//        rfonts59.setEastAsia( "Times New Roman");
//        rfonts59.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text29 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped29 = wmlObjectFactory.createRT(text29);
//        r29.getContent().add( textWrapped29);
//        text29.setValue( "Audience cannot understand because there is no sequence of information");
//        // Create object for tcPr
//        TcPr tcpr23 = wmlObjectFactory.createTcPr();
//        tc23.setTcPr(tcpr23);
//        // Create object for tcW
//        TblWidth tblwidth24 = wmlObjectFactory.createTblWidth();
//        tcpr23.setTcW(tblwidth24);
//        tblwidth24.setW( BigInteger.valueOf( 2098) );
//        tblwidth24.setType( "dxa");

        //------------------------------------------------------------------------------------------
        // todo content - organization row
        makeUpContentTableOrganizationRow(assessment, wmlObjectFactory, tr5);

        //------------------------------------------------------------------------------------------
        // todo visual aids - [tableRow]

        // Create object for trPr
        TrPr trpr5 = wmlObjectFactory.createTrPr();
        tr5.setTrPr(trpr5);
        // Create object for jc (wrapped in JAXBElement)
        Jc jc10 = wmlObjectFactory.createJc();
        JAXBElement<org.docx4j.wml.Jc> jcWrapped5 = wmlObjectFactory.createCTTrPrBaseJc(jc10);
        trpr5.getCnfStyleOrDivIdOrGridBefore().add( jcWrapped5);
        jc10.setVal(org.docx4j.wml.JcEnumeration.CENTER);
        // Create object for tr
        Tr tr6 = wmlObjectFactory.createTr();
        tbl2.getContent().add( tr6);
        // Create object for tc (wrapped in JAXBElement)
        Tc tc24 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped24 = wmlObjectFactory.createTrTc(tc24);
        tr6.getContent().add( tcWrapped24);
        // Create object for p
        P p33 = wmlObjectFactory.createP();
        tc24.getContent().add( p33);
        // Create object for pPr
        PPr ppr31 = wmlObjectFactory.createPPr();
        p33.setPPr(ppr31);
        // Create object for rPr
        ParaRPr pararpr31 = wmlObjectFactory.createParaRPr();
        ppr31.setRPr(pararpr31);
        // Create object for rFonts
        RFonts rfonts60 = wmlObjectFactory.createRFonts();
        pararpr31.setRFonts(rfonts60);
        rfonts60.setAscii( "Times New Roman");
        rfonts60.setCs( "Times New Roman");
        rfonts60.setEastAsia( "Times New Roman");
        rfonts60.setHAnsi( "Times New Roman");
        // Create object for r
        R r30 = wmlObjectFactory.createR();
        p33.getContent().add( r30);
        // Create object for rPr
        RPr rpr30 = wmlObjectFactory.createRPr();
        r30.setRPr(rpr30);
        // Create object for rFonts
        RFonts rfonts61 = wmlObjectFactory.createRFonts();
        rpr30.setRFonts(rfonts61);
        rfonts61.setAscii( "Times New Roman");
        rfonts61.setCs( "Times New Roman");
        rfonts61.setEastAsia( "Times New Roman");
        rfonts61.setHAnsi( "Times New Roman");
        // Create object for t (wrapped in JAXBElement)
        Text text30 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped30 = wmlObjectFactory.createRT(text30);
        r30.getContent().add( textWrapped30);
        text30.setValue( "Visual Aids");
        // Create object for p
        P p34 = wmlObjectFactory.createP();
        tc24.getContent().add( p34);
        // Create object for pPr
        PPr ppr32 = wmlObjectFactory.createPPr();
        p34.setPPr(ppr32);
        // Create object for rPr
        ParaRPr pararpr32 = wmlObjectFactory.createParaRPr();
        ppr32.setRPr(pararpr32);
        // Create object for rFonts
        RFonts rfonts62 = wmlObjectFactory.createRFonts();
        pararpr32.setRFonts(rfonts62);
        rfonts62.setAscii( "Times New Roman");
        rfonts62.setCs( "Times New Roman");
        rfonts62.setEastAsia( "Times New Roman");
        rfonts62.setHAnsi( "Times New Roman");
        // Create object for r
        R r31 = wmlObjectFactory.createR();
        p34.getContent().add( r31);
        // Create object for rPr
        RPr rpr31 = wmlObjectFactory.createRPr();
        r31.setRPr(rpr31);
        // Create object for rFonts
        RFonts rfonts63 = wmlObjectFactory.createRFonts();
        rpr31.setRFonts(rfonts63);
        rfonts63.setAscii( "Times New Roman");
        rfonts63.setCs( "Times New Roman");
        rfonts63.setEastAsia( "Times New Roman");
        rfonts63.setHAnsi( "Times New Roman");
        // Create object for t (wrapped in JAXBElement)
        Text text31 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped31 = wmlObjectFactory.createRT(text31);
        r31.getContent().add( textWrapped31);
        text31.setValue( " ");
        text31.setSpace( "preserve");
        // Create object for p
        P p35 = wmlObjectFactory.createP();
        tc24.getContent().add( p35);
        // Create object for pPr
        PPr ppr33 = wmlObjectFactory.createPPr();
        p35.setPPr(ppr33);
        // Create object for rPr
        ParaRPr pararpr33 = wmlObjectFactory.createParaRPr();
        ppr33.setRPr(pararpr33);
        // Create object for rFonts
        RFonts rfonts64 = wmlObjectFactory.createRFonts();
        pararpr33.setRFonts(rfonts64);
        rfonts64.setAscii( "Times New Roman");
        rfonts64.setCs( "Times New Roman");
        rfonts64.setEastAsia( "Times New Roman");
        rfonts64.setHAnsi( "Times New Roman");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue21 = wmlObjectFactory.createBooleanDefaultTrue();
        pararpr33.setB(booleandefaulttrue21);
        // Create object for tcPr
        TcPr tcpr24 = wmlObjectFactory.createTcPr();
        tc24.setTcPr(tcpr24);
        // Create object for tcW
        TblWidth tblwidth25 = wmlObjectFactory.createTblWidth();
        tcpr24.setTcW(tblwidth25);
        tblwidth25.setW( BigInteger.valueOf( 1815) );
        tblwidth25.setType( "dxa");

//        //------------------------------------------------------------------------------------------
//        // todo visual aids - excellent
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc25 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped25 = wmlObjectFactory.createTrTc(tc25);
//        tr6.getContent().add( tcWrapped25);
//        // Create object for p
//        P p36 = wmlObjectFactory.createP();
//        tc25.getContent().add( p36);
//        // Create object for pPr
//        PPr ppr34 = wmlObjectFactory.createPPr();
//        p36.setPPr(ppr34);
//        // Create object for rPr
//        ParaRPr pararpr34 = wmlObjectFactory.createParaRPr();
//        ppr34.setRPr(pararpr34);
//        // Create object for rFonts
//        RFonts rfonts65 = wmlObjectFactory.createRFonts();
//        pararpr34.setRFonts(rfonts65);
//        rfonts65.setAscii( "Times New Roman");
//        rfonts65.setCs( "Times New Roman");
//        rfonts65.setEastAsia( "Times New Roman");
//        rfonts65.setHAnsi( "Times New Roman");
//        // Create object for spacing
//        PPrBase.Spacing pprbasespacing4 = wmlObjectFactory.createPPrBaseSpacing();
//        ppr34.setSpacing(pprbasespacing4);
//        pprbasespacing4.setAfter( BigInteger.valueOf( 120) );
//        // Create object for r
//        R r32 = wmlObjectFactory.createR();
//        p36.getContent().add( r32);
//        // Create object for rPr
//        RPr rpr32 = wmlObjectFactory.createRPr();
//        r32.setRPr(rpr32);
//        // Create object for rFonts
//        RFonts rfonts66 = wmlObjectFactory.createRFonts();
//        rpr32.setRFonts(rfonts66);
//        rfonts66.setAscii( "Times New Roman");
//        rfonts66.setCs( "Times New Roman");
//        rfonts66.setEastAsia( "Times New Roman");
//        rfonts66.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text32 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped32 = wmlObjectFactory.createRT(text32);
//        r32.getContent().add( textWrapped32);
//        text32.setValue( "Visual aids are readable, clear and professional looking, enhancing the message.");
//        // Create object for tcPr
//        TcPr tcpr25 = wmlObjectFactory.createTcPr();
//        tc25.setTcPr(tcpr25);
//        // Create object for tcW
//        TblWidth tblwidth26 = wmlObjectFactory.createTblWidth();
//        tcpr25.setTcW(tblwidth26);
//        tblwidth26.setW( BigInteger.valueOf( 2098) );
//        tblwidth26.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo visual aids - good
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc26 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped26 = wmlObjectFactory.createTrTc(tc26);
//        tr6.getContent().add( tcWrapped26);
//        // Create object for p
//        P p37 = wmlObjectFactory.createP();
//        tc26.getContent().add( p37);
//        // Create object for pPr
//        PPr ppr35 = wmlObjectFactory.createPPr();
//        p37.setPPr(ppr35);
//        // Create object for rPr
//        ParaRPr pararpr35 = wmlObjectFactory.createParaRPr();
//        ppr35.setRPr(pararpr35);
//        // Create object for rFonts
//        RFonts rfonts67 = wmlObjectFactory.createRFonts();
//        pararpr35.setRFonts(rfonts67);
//        rfonts67.setAscii( "Times New Roman");
//        rfonts67.setCs( "Times New Roman");
//        rfonts67.setEastAsia( "Times New Roman");
//        rfonts67.setHAnsi( "Times New Roman");
//        // Create object for r
//        R r33 = wmlObjectFactory.createR();
//        p37.getContent().add( r33);
//        // Create object for rPr
//        RPr rpr33 = wmlObjectFactory.createRPr();
//        r33.setRPr(rpr33);
//        // Create object for rFonts
//        RFonts rfonts68 = wmlObjectFactory.createRFonts();
//        rpr33.setRFonts(rfonts68);
//        rfonts68.setAscii( "Times New Roman");
//        rfonts68.setCs( "Times New Roman");
//        rfonts68.setEastAsia( "Times New Roman");
//        rfonts68.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text33 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped33 = wmlObjectFactory.createRT(text33);
//        r33.getContent().add( textWrapped33);
//        text33.setValue( "Visual aids are mostly readable, clear and professional looking.");
//        // Create object for tcPr
//        TcPr tcpr26 = wmlObjectFactory.createTcPr();
//        tc26.setTcPr(tcpr26);
//        // Create object for tcW
//        TblWidth tblwidth27 = wmlObjectFactory.createTblWidth();
//        tcpr26.setTcW(tblwidth27);
//        tblwidth27.setW( BigInteger.valueOf( 2098) );
//        tblwidth27.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo visual aids - fair
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc27 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped27 = wmlObjectFactory.createTrTc(tc27);
//        tr6.getContent().add( tcWrapped27);
//        // Create object for p
//        P p38 = wmlObjectFactory.createP();
//        tc27.getContent().add( p38);
//        // Create object for pPr
//        PPr ppr36 = wmlObjectFactory.createPPr();
//        p38.setPPr(ppr36);
//        // Create object for rPr
//        ParaRPr pararpr36 = wmlObjectFactory.createParaRPr();
//        ppr36.setRPr(pararpr36);
//        // Create object for rFonts
//        RFonts rfonts69 = wmlObjectFactory.createRFonts();
//        pararpr36.setRFonts(rfonts69);
//        rfonts69.setAscii( "Times New Roman");
//        rfonts69.setCs( "Times New Roman");
//        rfonts69.setEastAsia( "Times New Roman");
//        rfonts69.setHAnsi( "Times New Roman");
//        // Create object for spacing
//        PPrBase.Spacing pprbasespacing5 = wmlObjectFactory.createPPrBaseSpacing();
//        ppr36.setSpacing(pprbasespacing5);
//        pprbasespacing5.setAfter( BigInteger.valueOf( 120) );
//        // Create object for r
//        R r34 = wmlObjectFactory.createR();
//        p38.getContent().add( r34);
//        // Create object for rPr
//        RPr rpr34 = wmlObjectFactory.createRPr();
//        r34.setRPr(rpr34);
//        // Create object for rFonts
//        RFonts rfonts70 = wmlObjectFactory.createRFonts();
//        rpr34.setRFonts(rfonts70);
//        rfonts70.setAscii( "Times New Roman");
//        rfonts70.setCs( "Times New Roman");
//        rfonts70.setEastAsia( "Times New Roman");
//        rfonts70.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text34 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped34 = wmlObjectFactory.createRT(text34);
//        r34.getContent().add( textWrapped34);
//        text34.setValue( "Significant problems with readability, clarity, professionalism of visual aids.");
//        // Create object for tcPr
//        TcPr tcpr27 = wmlObjectFactory.createTcPr();
//        tc27.setTcPr(tcpr27);
//        // Create object for tcW
//        TblWidth tblwidth28 = wmlObjectFactory.createTblWidth();
//        tcpr27.setTcW(tblwidth28);
//        tblwidth28.setW( BigInteger.valueOf( 2098) );
//        tblwidth28.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo visual aids - poor
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc28 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped28 = wmlObjectFactory.createTrTc(tc28);
//        tr6.getContent().add( tcWrapped28);
//        // Create object for p
//        P p39 = wmlObjectFactory.createP();
//        tc28.getContent().add( p39);
//        // Create object for pPr
//        PPr ppr37 = wmlObjectFactory.createPPr();
//        p39.setPPr(ppr37);
//        // Create object for rPr
//        ParaRPr pararpr37 = wmlObjectFactory.createParaRPr();
//        ppr37.setRPr(pararpr37);
//        // Create object for rFonts
//        RFonts rfonts71 = wmlObjectFactory.createRFonts();
//        pararpr37.setRFonts(rfonts71);
//        rfonts71.setAscii( "Times New Roman");
//        rfonts71.setCs( "Times New Roman");
//        rfonts71.setEastAsia( "Times New Roman");
//        rfonts71.setHAnsi( "Times New Roman");
//        // Create object for r
//        R r35 = wmlObjectFactory.createR();
//        p39.getContent().add( r35);
//        // Create object for rPr
//        RPr rpr35 = wmlObjectFactory.createRPr();
//        r35.setRPr(rpr35);
//        // Create object for rFonts
//        RFonts rfonts72 = wmlObjectFactory.createRFonts();
//        rpr35.setRFonts(rfonts72);
//        rfonts72.setAscii( "Times New Roman");
//        rfonts72.setCs( "Times New Roman");
//        rfonts72.setEastAsia( "Times New Roman");
//        rfonts72.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text35 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped35 = wmlObjectFactory.createRT(text35);
//        r35.getContent().add( textWrapped35);
//        text35.setValue( "Visual aids are all unreadable, unclear and/or unprofessional.");
//        // Create object for tcPr
//        TcPr tcpr28 = wmlObjectFactory.createTcPr();
//        tc28.setTcPr(tcpr28);
//        // Create object for tcW
//        TblWidth tblwidth29 = wmlObjectFactory.createTblWidth();
//        tcpr28.setTcW(tblwidth29);
//        tblwidth29.setW( BigInteger.valueOf( 2098) );
//        tblwidth29.setType( "dxa");

        makeUpContentTableVisualAidsRow(assessment, wmlObjectFactory, tr6);

        //------------------------------------------------------------------------------------------
        // todo qa - [tableRow]
        // Create object for trPr
        TrPr trpr6 = wmlObjectFactory.createTrPr();
        tr6.setTrPr(trpr6);
        // Create object for trHeight (wrapped in JAXBElement)
        CTHeight height3 = wmlObjectFactory.createCTHeight();
        JAXBElement<org.docx4j.wml.CTHeight> heightWrapped3 = wmlObjectFactory.createCTTrPrBaseTrHeight(height3);
        trpr6.getCnfStyleOrDivIdOrGridBefore().add( heightWrapped3);
        height3.setVal( BigInteger.valueOf( 1360) );
        // Create object for jc (wrapped in JAXBElement)
        Jc jc11 = wmlObjectFactory.createJc();
        JAXBElement<org.docx4j.wml.Jc> jcWrapped6 = wmlObjectFactory.createCTTrPrBaseJc(jc11);
        trpr6.getCnfStyleOrDivIdOrGridBefore().add( jcWrapped6);
        jc11.setVal(org.docx4j.wml.JcEnumeration.CENTER);
        // Create object for tr
        Tr tr7 = wmlObjectFactory.createTr();
        tbl2.getContent().add( tr7);
        // Create object for tc (wrapped in JAXBElement)
        Tc tc29 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped29 = wmlObjectFactory.createTrTc(tc29);
        tr7.getContent().add( tcWrapped29);
        // Create object for p
        P p40 = wmlObjectFactory.createP();
        tc29.getContent().add( p40);
        // Create object for pPr
        PPr ppr38 = wmlObjectFactory.createPPr();
        p40.setPPr(ppr38);
        // Create object for rPr
        ParaRPr pararpr38 = wmlObjectFactory.createParaRPr();
        ppr38.setRPr(pararpr38);
        // Create object for rFonts
        RFonts rfonts73 = wmlObjectFactory.createRFonts();
        pararpr38.setRFonts(rfonts73);
        rfonts73.setAscii( "Times New Roman");
        rfonts73.setCs( "Times New Roman");
        rfonts73.setEastAsia( "Times New Roman");
        rfonts73.setHAnsi( "Times New Roman");
        // Create object for r
        R r36 = wmlObjectFactory.createR();
        p40.getContent().add( r36);
        // Create object for rPr
        RPr rpr36 = wmlObjectFactory.createRPr();
        r36.setRPr(rpr36);
        // Create object for rFonts
        RFonts rfonts74 = wmlObjectFactory.createRFonts();
        rpr36.setRFonts(rfonts74);
        rfonts74.setAscii( "Times New Roman");
        rfonts74.setCs( "Times New Roman");
        rfonts74.setEastAsia( "Times New Roman");
        rfonts74.setHAnsi( "Times New Roman");
        // Create object for t (wrapped in JAXBElement)
        Text text36 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped36 = wmlObjectFactory.createRT(text36);
        r36.getContent().add( textWrapped36);
        text36.setValue( "Question & Answer");
        // Create object for p
        P p41 = wmlObjectFactory.createP();
        tc29.getContent().add( p41);
        // Create object for pPr
        PPr ppr39 = wmlObjectFactory.createPPr();
        p41.setPPr(ppr39);
        // Create object for rPr
        ParaRPr pararpr39 = wmlObjectFactory.createParaRPr();
        ppr39.setRPr(pararpr39);
        // Create object for rFonts
        RFonts rfonts75 = wmlObjectFactory.createRFonts();
        pararpr39.setRFonts(rfonts75);
        rfonts75.setAscii( "Times New Roman");
        rfonts75.setCs( "Times New Roman");
        rfonts75.setEastAsia( "Times New Roman");
        rfonts75.setHAnsi( "Times New Roman");
        // Create object for tcPr
        TcPr tcpr29 = wmlObjectFactory.createTcPr();
        tc29.setTcPr(tcpr29);
        // Create object for tcW
        TblWidth tblwidth30 = wmlObjectFactory.createTblWidth();
        tcpr29.setTcW(tblwidth30);
        tblwidth30.setW( BigInteger.valueOf( 1815) );
        tblwidth30.setType( "dxa");

//        //------------------------------------------------------------------------------------------
//        // todo qa - excellent
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc30 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped30 = wmlObjectFactory.createTrTc(tc30);
//        tr7.getContent().add( tcWrapped30);
//        // Create object for p
//        P p42 = wmlObjectFactory.createP();
//        tc30.getContent().add( p42);
//        // Create object for pPr
//        PPr ppr40 = wmlObjectFactory.createPPr();
//        p42.setPPr(ppr40);
//        // Create object for rPr
//        ParaRPr pararpr40 = wmlObjectFactory.createParaRPr();
//        ppr40.setRPr(pararpr40);
//        // Create object for rFonts
//        RFonts rfonts76 = wmlObjectFactory.createRFonts();
//        pararpr40.setRFonts(rfonts76);
//        rfonts76.setAscii( "Times New Roman");
//        rfonts76.setCs( "Times New Roman");
//        rfonts76.setEastAsia( "Times New Roman");
//        rfonts76.setHAnsi( "Times New Roman");
//        // Create object for spacing
//        PPrBase.Spacing pprbasespacing6 = wmlObjectFactory.createPPrBaseSpacing();
//        ppr40.setSpacing(pprbasespacing6);
//        pprbasespacing6.setAfter( BigInteger.valueOf( 120) );
//        // Create object for r
//        R r37 = wmlObjectFactory.createR();
//        p42.getContent().add( r37);
//        // Create object for rPr
//        RPr rpr37 = wmlObjectFactory.createRPr();
//        r37.setRPr(rpr37);
//        // Create object for rFonts
//        RFonts rfonts77 = wmlObjectFactory.createRFonts();
//        rpr37.setRFonts(rfonts77);
//        rfonts77.setAscii( "Times New Roman");
//        rfonts77.setCs( "Times New Roman");
//        rfonts77.setEastAsia( "Times New Roman");
//        rfonts77.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text37 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped37 = wmlObjectFactory.createRT(text37);
//        r37.getContent().add( textWrapped37);
//        text37.setValue( "Speaker has prepared relevant questions for opening up the discussion and is able to stimulate discussion.");
//        // Create object for tcPr
//        TcPr tcpr30 = wmlObjectFactory.createTcPr();
//        tc30.setTcPr(tcpr30);
//        // Create object for tcW
//        TblWidth tblwidth31 = wmlObjectFactory.createTblWidth();
//        tcpr30.setTcW(tblwidth31);
//        tblwidth31.setW( BigInteger.valueOf( 2098) );
//        tblwidth31.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo qa - good
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc31 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped31 = wmlObjectFactory.createTrTc(tc31);
//        tr7.getContent().add( tcWrapped31);
//        // Create object for p
//        P p43 = wmlObjectFactory.createP();
//        tc31.getContent().add( p43);
//        // Create object for pPr
//        PPr ppr41 = wmlObjectFactory.createPPr();
//        p43.setPPr(ppr41);
//        // Create object for rPr
//        ParaRPr pararpr41 = wmlObjectFactory.createParaRPr();
//        ppr41.setRPr(pararpr41);
//        // Create object for rFonts
//        RFonts rfonts78 = wmlObjectFactory.createRFonts();
//        pararpr41.setRFonts(rfonts78);
//        rfonts78.setAscii( "Times New Roman");
//        rfonts78.setCs( "Times New Roman");
//        rfonts78.setEastAsia( "Times New Roman");
//        rfonts78.setHAnsi( "Times New Roman");
//        // Create object for spacing
//        PPrBase.Spacing pprbasespacing7 = wmlObjectFactory.createPPrBaseSpacing();
//        ppr41.setSpacing(pprbasespacing7);
//        pprbasespacing7.setAfter( BigInteger.valueOf( 120) );
//        // Create object for r
//        R r38 = wmlObjectFactory.createR();
//        p43.getContent().add( r38);
//        // Create object for rPr
//        RPr rpr38 = wmlObjectFactory.createRPr();
//        r38.setRPr(rpr38);
//        // Create object for rFonts
//        RFonts rfonts79 = wmlObjectFactory.createRFonts();
//        rpr38.setRFonts(rfonts79);
//        rfonts79.setAscii( "Times New Roman");
//        rfonts79.setCs( "Times New Roman");
//        rfonts79.setEastAsia( "Times New Roman");
//        rfonts79.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text38 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped38 = wmlObjectFactory.createRT(text38);
//        r38.getContent().add( textWrapped38);
//        text38.setValue( "Speaker has prepared relevant questions for opening up the discussion and is somewhat able to stimulate discussion");
//        // Create object for tcPr
//        TcPr tcpr31 = wmlObjectFactory.createTcPr();
//        tc31.setTcPr(tcpr31);
//        // Create object for tcW
//        TblWidth tblwidth32 = wmlObjectFactory.createTblWidth();
//        tcpr31.setTcW(tblwidth32);
//        tblwidth32.setW( BigInteger.valueOf( 2098) );
//        tblwidth32.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo qa - fair
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc32 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped32 = wmlObjectFactory.createTrTc(tc32);
//        tr7.getContent().add( tcWrapped32);
//        // Create object for p
//        P p44 = wmlObjectFactory.createP();
//        tc32.getContent().add( p44);
//        // Create object for pPr
//        PPr ppr42 = wmlObjectFactory.createPPr();
//        p44.setPPr(ppr42);
//        // Create object for rPr
//        ParaRPr pararpr42 = wmlObjectFactory.createParaRPr();
//        ppr42.setRPr(pararpr42);
//        // Create object for rFonts
//        RFonts rfonts80 = wmlObjectFactory.createRFonts();
//        pararpr42.setRFonts(rfonts80);
//        rfonts80.setAscii( "Times New Roman");
//        rfonts80.setCs( "Times New Roman");
//        rfonts80.setEastAsia( "Times New Roman");
//        rfonts80.setHAnsi( "Times New Roman");
//        // Create object for r
//        R r39 = wmlObjectFactory.createR();
//        p44.getContent().add( r39);
//        // Create object for rPr
//        RPr rpr39 = wmlObjectFactory.createRPr();
//        r39.setRPr(rpr39);
//        // Create object for rFonts
//        RFonts rfonts81 = wmlObjectFactory.createRFonts();
//        rpr39.setRFonts(rfonts81);
//        rfonts81.setAscii( "Times New Roman");
//        rfonts81.setCs( "Times New Roman");
//        rfonts81.setEastAsia( "Times New Roman");
//        rfonts81.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text39 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped39 = wmlObjectFactory.createRT(text39);
//        r39.getContent().add( textWrapped39);
//        text39.setValue( "Speaker has prepared questions but is not really able to stimulate discussion");
//        // Create object for p
//        P p45 = wmlObjectFactory.createP();
//        tc32.getContent().add( p45);
//        // Create object for pPr
//        PPr ppr43 = wmlObjectFactory.createPPr();
//        p45.setPPr(ppr43);
//        // Create object for rPr
//        ParaRPr pararpr43 = wmlObjectFactory.createParaRPr();
//        ppr43.setRPr(pararpr43);
//        // Create object for rFonts
//        RFonts rfonts82 = wmlObjectFactory.createRFonts();
//        pararpr43.setRFonts(rfonts82);
//        rfonts82.setAscii( "Times New Roman");
//        rfonts82.setCs( "Times New Roman");
//        rfonts82.setEastAsia( "Times New Roman");
//        rfonts82.setHAnsi( "Times New Roman");
//        // Create object for tcPr
//        TcPr tcpr32 = wmlObjectFactory.createTcPr();
//        tc32.setTcPr(tcpr32);
//        // Create object for tcW
//        TblWidth tblwidth33 = wmlObjectFactory.createTblWidth();
//        tcpr32.setTcW(tblwidth33);
//        tblwidth33.setW( BigInteger.valueOf( 2098) );
//        tblwidth33.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo qa - poor
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc33 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped33 = wmlObjectFactory.createTrTc(tc33);
//        tr7.getContent().add( tcWrapped33);
//        // Create object for p
//        P p46 = wmlObjectFactory.createP();
//        tc33.getContent().add( p46);
//        // Create object for pPr
//        PPr ppr44 = wmlObjectFactory.createPPr();
//        p46.setPPr(ppr44);
//        // Create object for rPr
//        ParaRPr pararpr44 = wmlObjectFactory.createParaRPr();
//        ppr44.setRPr(pararpr44);
//        // Create object for rFonts
//        RFonts rfonts83 = wmlObjectFactory.createRFonts();
//        pararpr44.setRFonts(rfonts83);
//        rfonts83.setAscii( "Times New Roman");
//        rfonts83.setCs( "Times New Roman");
//        rfonts83.setEastAsia( "Times New Roman");
//        rfonts83.setHAnsi( "Times New Roman");
//        // Create object for r
//        R r40 = wmlObjectFactory.createR();
//        p46.getContent().add( r40);
//        // Create object for rPr
//        RPr rpr40 = wmlObjectFactory.createRPr();
//        r40.setRPr(rpr40);
//        // Create object for rFonts
//        RFonts rfonts84 = wmlObjectFactory.createRFonts();
//        rpr40.setRFonts(rfonts84);
//        rfonts84.setAscii( "Times New Roman");
//        rfonts84.setCs( "Times New Roman");
//        rfonts84.setEastAsia( "Times New Roman");
//        rfonts84.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text40 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped40 = wmlObjectFactory.createRT(text40);
//        r40.getContent().add( textWrapped40);
//        text40.setValue( "Speaker has not prepared questions");
//        // Create object for tcPr
//        TcPr tcpr33 = wmlObjectFactory.createTcPr();
//        tc33.setTcPr(tcpr33);
//        // Create object for tcW
//        TblWidth tblwidth34 = wmlObjectFactory.createTblWidth();
//        tcpr33.setTcW(tblwidth34);
//        tblwidth34.setW( BigInteger.valueOf( 2098) );
//        tblwidth34.setType( "dxa");

        makeUpContentTableQaRow(assessment, wmlObjectFactory, tr7);

        //------------------------------------------------------------------------------------------
        // todo language - [table]
        // Create object for trPr
        TrPr trpr7 = wmlObjectFactory.createTrPr();
        tr7.setTrPr(trpr7);

        // Create object for trHeight (wrapped in JAXBElement)
        CTHeight height4 = wmlObjectFactory.createCTHeight();
        JAXBElement<org.docx4j.wml.CTHeight> heightWrapped4 = wmlObjectFactory.createCTTrPrBaseTrHeight(height4);
        trpr7.getCnfStyleOrDivIdOrGridBefore().add( heightWrapped4);
        height4.setVal( BigInteger.valueOf( 680) );
        // Create object for jc (wrapped in JAXBElement)
        Jc jc12 = wmlObjectFactory.createJc();
        JAXBElement<org.docx4j.wml.Jc> jcWrapped7 = wmlObjectFactory.createCTTrPrBaseJc(jc12);
        trpr7.getCnfStyleOrDivIdOrGridBefore().add( jcWrapped7);
        jc12.setVal(org.docx4j.wml.JcEnumeration.CENTER);
        // Create object for tblPr
        TblPr tblpr2 = wmlObjectFactory.createTblPr();
        tbl2.setTblPr(tblpr2);
        // Create object for tblStyle
        CTTblPrBase.TblStyle tblprbasetblstyle2 = wmlObjectFactory.createCTTblPrBaseTblStyle();
        tblpr2.setTblStyle(tblprbasetblstyle2);
        tblprbasetblstyle2.setVal( "a0");
        // Create object for tblBorders
        TblBorders tblborders2 = wmlObjectFactory.createTblBorders();
        tblpr2.setTblBorders(tblborders2);
        // Create object for insideH
        CTBorder border39 = wmlObjectFactory.createCTBorder();
        tblborders2.setInsideH(border39);
        border39.setVal(org.docx4j.wml.STBorder.SINGLE);
        border39.setSz( BigInteger.valueOf( 4) );
        border39.setColor( "000000");
        border39.setSpace( BigInteger.valueOf( 0) );
        // Create object for top
        CTBorder border40 = wmlObjectFactory.createCTBorder();
        tblborders2.setTop(border40);
        border40.setVal(org.docx4j.wml.STBorder.SINGLE);
        border40.setSz( BigInteger.valueOf( 4) );
        border40.setColor( "000000");
        border40.setSpace( BigInteger.valueOf( 0) );
        // Create object for bottom
        CTBorder border41 = wmlObjectFactory.createCTBorder();
        tblborders2.setBottom(border41);
        border41.setVal(org.docx4j.wml.STBorder.SINGLE);
        border41.setSz( BigInteger.valueOf( 4) );
        border41.setColor( "000000");
        border41.setSpace( BigInteger.valueOf( 0) );
        // Create object for insideV
        CTBorder border42 = wmlObjectFactory.createCTBorder();
        tblborders2.setInsideV(border42);
        border42.setVal(org.docx4j.wml.STBorder.SINGLE);
        border42.setSz( BigInteger.valueOf( 4) );
        border42.setColor( "000000");
        border42.setSpace( BigInteger.valueOf( 0) );
        // Create object for right
        CTBorder border43 = wmlObjectFactory.createCTBorder();
        tblborders2.setRight(border43);
        border43.setVal(org.docx4j.wml.STBorder.SINGLE);
        border43.setSz( BigInteger.valueOf( 4) );
        border43.setColor( "000000");
        border43.setSpace( BigInteger.valueOf( 0) );
        // Create object for left
        CTBorder border44 = wmlObjectFactory.createCTBorder();
        tblborders2.setLeft(border44);
        border44.setVal(org.docx4j.wml.STBorder.SINGLE);
        border44.setSz( BigInteger.valueOf( 4) );
        border44.setColor( "000000");
        border44.setSpace( BigInteger.valueOf( 0) );
        // Create object for jc
        Jc jc13 = wmlObjectFactory.createJc();
        tblpr2.setJc(jc13);
        jc13.setVal(org.docx4j.wml.JcEnumeration.CENTER);
        // Create object for tblW
        TblWidth tblwidth35 = wmlObjectFactory.createTblWidth();
        tblpr2.setTblW(tblwidth35);
        tblwidth35.setW( BigInteger.valueOf( 10207) );
        tblwidth35.setType( "dxa");
        // Create object for tblLayout
        CTTblLayoutType tbllayouttype2 = wmlObjectFactory.createCTTblLayoutType();
        tblpr2.setTblLayout(tbllayouttype2);
        tbllayouttype2.setType(org.docx4j.wml.STTblLayoutType.FIXED);
        // Create object for tblLook
        CTTblLook tbllook2 = wmlObjectFactory.createCTTblLook();
        tblpr2.setTblLook(tbllook2);
        tbllook2.setVal( "0400");
        tbllook2.setFirstColumn(org.docx4j.sharedtypes.STOnOff.ZERO);
        tbllook2.setFirstRow(org.docx4j.sharedtypes.STOnOff.ZERO);
        tbllook2.setLastColumn(org.docx4j.sharedtypes.STOnOff.ZERO);
        tbllook2.setLastRow(org.docx4j.sharedtypes.STOnOff.ZERO);
        tbllook2.setNoHBand(org.docx4j.sharedtypes.STOnOff.ZERO);
        tbllook2.setNoVBand(org.docx4j.sharedtypes.STOnOff.ONE);
        // Create object for tblGrid
        TblGrid tblgrid2 = wmlObjectFactory.createTblGrid();
        tbl2.setTblGrid(tblgrid2);
        // Create object for gridCol
        TblGridCol tblgridcol7 = wmlObjectFactory.createTblGridCol();
        tblgrid2.getGridCol().add( tblgridcol7);
        tblgridcol7.setW( BigInteger.valueOf( 1815) );
        // Create object for gridCol
        TblGridCol tblgridcol8 = wmlObjectFactory.createTblGridCol();
        tblgrid2.getGridCol().add( tblgridcol8);
        tblgridcol8.setW( BigInteger.valueOf( 2098) );
        // Create object for gridCol
        TblGridCol tblgridcol9 = wmlObjectFactory.createTblGridCol();
        tblgrid2.getGridCol().add( tblgridcol9);
        tblgridcol9.setW( BigInteger.valueOf( 2098) );
        // Create object for gridCol
        TblGridCol tblgridcol10 = wmlObjectFactory.createTblGridCol();
        tblgrid2.getGridCol().add( tblgridcol10);
        tblgridcol10.setW( BigInteger.valueOf( 2098) );
        // Create object for gridCol
        TblGridCol tblgridcol11 = wmlObjectFactory.createTblGridCol();
        tblgrid2.getGridCol().add( tblgridcol11);
        tblgridcol11.setW( BigInteger.valueOf( 2098) );
        // Create object for p
        P p47 = wmlObjectFactory.createP();
        body.getContent().add( p47);
        // Create object for pPr
        PPr ppr45 = wmlObjectFactory.createPPr();
        p47.setPPr(ppr45);
        // Create object for rPr
        ParaRPr pararpr45 = wmlObjectFactory.createParaRPr();
        ppr45.setRPr(pararpr45);
        // Create object for sz
        HpsMeasure hpsmeasure39 = wmlObjectFactory.createHpsMeasure();
        pararpr45.setSz(hpsmeasure39);
        hpsmeasure39.setVal( BigInteger.valueOf( 24) );
        // Create object for rFonts
        RFonts rfonts85 = wmlObjectFactory.createRFonts();
        pararpr45.setRFonts(rfonts85);
        rfonts85.setAscii( "Times");
        rfonts85.setCs( "Times");
        rfonts85.setEastAsia( "Times");
        rfonts85.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue22 = wmlObjectFactory.createBooleanDefaultTrue();
        pararpr45.setB(booleandefaulttrue22);
        // Create object for szCs
        HpsMeasure hpsmeasure40 = wmlObjectFactory.createHpsMeasure();
        pararpr45.setSzCs(hpsmeasure40);
        hpsmeasure40.setVal( BigInteger.valueOf( 24) );
        // Create object for spacing
        PPrBase.Spacing pprbasespacing8 = wmlObjectFactory.createPPrBaseSpacing();
        ppr45.setSpacing(pprbasespacing8);
        pprbasespacing8.setAfter( BigInteger.valueOf( 0) );
        pprbasespacing8.setLine( BigInteger.valueOf( 240) );
        pprbasespacing8.setLineRule(org.docx4j.wml.STLineSpacingRule.AUTO);
        // Create object for tbl (wrapped in JAXBElement)
        Tbl tbl3 = wmlObjectFactory.createTbl();
        JAXBElement<org.docx4j.wml.Tbl> tblWrapped3 = wmlObjectFactory.createBodyTbl(tbl3);
        body.getContent().add( tblWrapped3);
        // Create object for tr
        Tr tr8 = wmlObjectFactory.createTr();
        tbl3.getContent().add( tr8);
        // Create object for tc (wrapped in JAXBElement)
        Tc tc34 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped34 = wmlObjectFactory.createTrTc(tc34);
        tr8.getContent().add( tcWrapped34);
        // Create object for p
        P p48 = wmlObjectFactory.createP();
        tc34.getContent().add( p48);
        // Create object for pPr
        PPr ppr46 = wmlObjectFactory.createPPr();
        p48.setPPr(ppr46);
        // Create object for rPr
        ParaRPr pararpr46 = wmlObjectFactory.createParaRPr();
        ppr46.setRPr(pararpr46);
        // Create object for rFonts
        RFonts rfonts86 = wmlObjectFactory.createRFonts();
        pararpr46.setRFonts(rfonts86);
        rfonts86.setAscii( "Times");
        rfonts86.setCs( "Times");
        rfonts86.setEastAsia( "Times");
        rfonts86.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue23 = wmlObjectFactory.createBooleanDefaultTrue();
        pararpr46.setB(booleandefaulttrue23);
        // Create object for r
        R r41 = wmlObjectFactory.createR();
        p48.getContent().add( r41);
        // Create object for rPr
        RPr rpr41 = wmlObjectFactory.createRPr();
        r41.setRPr(rpr41);
        // Create object for rFonts
        RFonts rfonts87 = wmlObjectFactory.createRFonts();
        rpr41.setRFonts(rfonts87);
        rfonts87.setAscii( "Times");
        rfonts87.setCs( "Times");
        rfonts87.setEastAsia( "Times");
        rfonts87.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue24 = wmlObjectFactory.createBooleanDefaultTrue();
        rpr41.setB(booleandefaulttrue24);
        // Create object for t (wrapped in JAXBElement)
        Text text41 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped41 = wmlObjectFactory.createRT(text41);
        r41.getContent().add( textWrapped41);
        text41.setValue( "Language and Delivery");
        // Create object for tcPr
        TcPr tcpr34 = wmlObjectFactory.createTcPr();
        tc34.setTcPr(tcpr34);
        // Create object for tcW
        TblWidth tblwidth36 = wmlObjectFactory.createTblWidth();
        tcpr34.setTcW(tblwidth36);
        tblwidth36.setW( BigInteger.valueOf( 1811) );
        tblwidth36.setType( "dxa");
        // Create object for tc (wrapped in JAXBElement)
        Tc tc35 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped35 = wmlObjectFactory.createTrTc(tc35);
        tr8.getContent().add( tcWrapped35);
        // Create object for p
        P p49 = wmlObjectFactory.createP();
        tc35.getContent().add( p49);
        // Create object for pPr
        PPr ppr47 = wmlObjectFactory.createPPr();
        p49.setPPr(ppr47);
        // Create object for rPr
        ParaRPr pararpr47 = wmlObjectFactory.createParaRPr();
        ppr47.setRPr(pararpr47);
        // Create object for rFonts
        RFonts rfonts88 = wmlObjectFactory.createRFonts();
        pararpr47.setRFonts(rfonts88);
        rfonts88.setAscii( "Times");
        rfonts88.setCs( "Times");
        rfonts88.setEastAsia( "Times");
        rfonts88.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue25 = wmlObjectFactory.createBooleanDefaultTrue();
        pararpr47.setB(booleandefaulttrue25);
        // Create object for r
        R r42 = wmlObjectFactory.createR();
        p49.getContent().add( r42);
        // Create object for rPr
        RPr rpr42 = wmlObjectFactory.createRPr();
        r42.setRPr(rpr42);
        // Create object for rFonts
        RFonts rfonts89 = wmlObjectFactory.createRFonts();
        rpr42.setRFonts(rfonts89);
        rfonts89.setAscii( "Times");
        rfonts89.setCs( "Times");
        rfonts89.setEastAsia( "Times");
        rfonts89.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue26 = wmlObjectFactory.createBooleanDefaultTrue();
        rpr42.setB(booleandefaulttrue26);
        // Create object for t (wrapped in JAXBElement)
        Text text42 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped42 = wmlObjectFactory.createRT(text42);
        r42.getContent().add( textWrapped42);
        text42.setValue( "Excellent (4)");
        // Create object for tcPr
        TcPr tcpr35 = wmlObjectFactory.createTcPr();
        tc35.setTcPr(tcpr35);
        // Create object for tcW
        TblWidth tblwidth37 = wmlObjectFactory.createTblWidth();
        tcpr35.setTcW(tblwidth37);
        tblwidth37.setW( BigInteger.valueOf( 2098) );
        tblwidth37.setType( "dxa");
        // Create object for tc (wrapped in JAXBElement)
        Tc tc36 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped36 = wmlObjectFactory.createTrTc(tc36);
        tr8.getContent().add( tcWrapped36);
        // Create object for p
        P p50 = wmlObjectFactory.createP();
        tc36.getContent().add( p50);
        // Create object for pPr
        PPr ppr48 = wmlObjectFactory.createPPr();
        p50.setPPr(ppr48);
        // Create object for rPr
        ParaRPr pararpr48 = wmlObjectFactory.createParaRPr();
        ppr48.setRPr(pararpr48);
        // Create object for rFonts
        RFonts rfonts90 = wmlObjectFactory.createRFonts();
        pararpr48.setRFonts(rfonts90);
        rfonts90.setAscii( "Times");
        rfonts90.setCs( "Times");
        rfonts90.setEastAsia( "Times");
        rfonts90.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue27 = wmlObjectFactory.createBooleanDefaultTrue();
        pararpr48.setB(booleandefaulttrue27);
        // Create object for r
        R r43 = wmlObjectFactory.createR();
        p50.getContent().add( r43);
        // Create object for rPr
        RPr rpr43 = wmlObjectFactory.createRPr();
        r43.setRPr(rpr43);
        // Create object for rFonts
        RFonts rfonts91 = wmlObjectFactory.createRFonts();
        rpr43.setRFonts(rfonts91);
        rfonts91.setAscii( "Times");
        rfonts91.setCs( "Times");
        rfonts91.setEastAsia( "Times");
        rfonts91.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue28 = wmlObjectFactory.createBooleanDefaultTrue();
        rpr43.setB(booleandefaulttrue28);
        // Create object for t (wrapped in JAXBElement)
        Text text43 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped43 = wmlObjectFactory.createRT(text43);
        r43.getContent().add( textWrapped43);
        text43.setValue( "Good (3)");
        // Create object for tcPr
        TcPr tcpr36 = wmlObjectFactory.createTcPr();
        tc36.setTcPr(tcpr36);
        // Create object for tcW
        TblWidth tblwidth38 = wmlObjectFactory.createTblWidth();
        tcpr36.setTcW(tblwidth38);
        tblwidth38.setW( BigInteger.valueOf( 2098) );
        tblwidth38.setType( "dxa");
        // Create object for tc (wrapped in JAXBElement)
        Tc tc37 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped37 = wmlObjectFactory.createTrTc(tc37);
        tr8.getContent().add( tcWrapped37);
        // Create object for p
        P p51 = wmlObjectFactory.createP();
        tc37.getContent().add( p51);
        // Create object for pPr
        PPr ppr49 = wmlObjectFactory.createPPr();
        p51.setPPr(ppr49);
        // Create object for rPr
        ParaRPr pararpr49 = wmlObjectFactory.createParaRPr();
        ppr49.setRPr(pararpr49);
        // Create object for rFonts
        RFonts rfonts92 = wmlObjectFactory.createRFonts();
        pararpr49.setRFonts(rfonts92);
        rfonts92.setAscii( "Times");
        rfonts92.setCs( "Times");
        rfonts92.setEastAsia( "Times");
        rfonts92.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue29 = wmlObjectFactory.createBooleanDefaultTrue();
        pararpr49.setB(booleandefaulttrue29);
        // Create object for r
        R r44 = wmlObjectFactory.createR();
        p51.getContent().add( r44);
        // Create object for rPr
        RPr rpr44 = wmlObjectFactory.createRPr();
        r44.setRPr(rpr44);
        // Create object for rFonts
        RFonts rfonts93 = wmlObjectFactory.createRFonts();
        rpr44.setRFonts(rfonts93);
        rfonts93.setAscii( "Times");
        rfonts93.setCs( "Times");
        rfonts93.setEastAsia( "Times");
        rfonts93.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue30 = wmlObjectFactory.createBooleanDefaultTrue();
        rpr44.setB(booleandefaulttrue30);
        // Create object for t (wrapped in JAXBElement)
        Text text44 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped44 = wmlObjectFactory.createRT(text44);
        r44.getContent().add( textWrapped44);
        text44.setValue( "Fair (2)");
        // Create object for tcPr
        TcPr tcpr37 = wmlObjectFactory.createTcPr();
        tc37.setTcPr(tcpr37);
        // Create object for tcW
        TblWidth tblwidth39 = wmlObjectFactory.createTblWidth();
        tcpr37.setTcW(tblwidth39);
        tblwidth39.setW( BigInteger.valueOf( 2098) );
        tblwidth39.setType( "dxa");
        // Create object for tc (wrapped in JAXBElement)
        Tc tc38 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped38 = wmlObjectFactory.createTrTc(tc38);
        tr8.getContent().add( tcWrapped38);
        // Create object for p
        P p52 = wmlObjectFactory.createP();
        tc38.getContent().add( p52);
        // Create object for pPr
        PPr ppr50 = wmlObjectFactory.createPPr();
        p52.setPPr(ppr50);
        // Create object for rPr
        ParaRPr pararpr50 = wmlObjectFactory.createParaRPr();
        ppr50.setRPr(pararpr50);
        // Create object for rFonts
        RFonts rfonts94 = wmlObjectFactory.createRFonts();
        pararpr50.setRFonts(rfonts94);
        rfonts94.setAscii( "Times");
        rfonts94.setCs( "Times");
        rfonts94.setEastAsia( "Times");
        rfonts94.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue31 = wmlObjectFactory.createBooleanDefaultTrue();
        pararpr50.setB(booleandefaulttrue31);
        // Create object for r
        R r45 = wmlObjectFactory.createR();
        p52.getContent().add( r45);
        // Create object for rPr
        RPr rpr45 = wmlObjectFactory.createRPr();
        r45.setRPr(rpr45);
        // Create object for rFonts
        RFonts rfonts95 = wmlObjectFactory.createRFonts();
        rpr45.setRFonts(rfonts95);
        rfonts95.setAscii( "Times");
        rfonts95.setCs( "Times");
        rfonts95.setEastAsia( "Times");
        rfonts95.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue32 = wmlObjectFactory.createBooleanDefaultTrue();
        rpr45.setB(booleandefaulttrue32);
        // Create object for t (wrapped in JAXBElement)
        Text text45 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped45 = wmlObjectFactory.createRT(text45);
        r45.getContent().add( textWrapped45);
        text45.setValue( "Poor (1)");
        // Create object for tcPr
        TcPr tcpr38 = wmlObjectFactory.createTcPr();
        tc38.setTcPr(tcpr38);
        // Create object for tcW
        TblWidth tblwidth40 = wmlObjectFactory.createTblWidth();
        tcpr38.setTcW(tblwidth40);
        tblwidth40.setW( BigInteger.valueOf( 2098) );
        tblwidth40.setType( "dxa");
        // Create object for trPr
        TrPr trpr8 = wmlObjectFactory.createTrPr();
        tr8.setTrPr(trpr8);

        //------------------------------------------------------------------------------------------
        // todo eyecontact - [tableRow]

        // Create object for jc (wrapped in JAXBElement)
        Jc jc14 = wmlObjectFactory.createJc();
        JAXBElement<org.docx4j.wml.Jc> jcWrapped8 = wmlObjectFactory.createCTTrPrBaseJc(jc14);
        trpr8.getCnfStyleOrDivIdOrGridBefore().add( jcWrapped8);
        jc14.setVal(org.docx4j.wml.JcEnumeration.CENTER);
        // Create object for tr
        Tr tr9 = wmlObjectFactory.createTr();
        tbl3.getContent().add( tr9);
        // Create object for tc (wrapped in JAXBElement)
        Tc tc39 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped39 = wmlObjectFactory.createTrTc(tc39);
        tr9.getContent().add( tcWrapped39);
        // Create object for p
        P p53 = wmlObjectFactory.createP();
        tc39.getContent().add( p53);
        // Create object for pPr
        PPr ppr51 = wmlObjectFactory.createPPr();
        p53.setPPr(ppr51);
        // Create object for rPr
        ParaRPr pararpr51 = wmlObjectFactory.createParaRPr();
        ppr51.setRPr(pararpr51);
        // Create object for rFonts
        RFonts rfonts96 = wmlObjectFactory.createRFonts();
        pararpr51.setRFonts(rfonts96);
        rfonts96.setAscii( "Times New Roman");
        rfonts96.setCs( "Times New Roman");
        rfonts96.setEastAsia( "Times New Roman");
        rfonts96.setHAnsi( "Times New Roman");
        // Create object for r
        R r46 = wmlObjectFactory.createR();
        p53.getContent().add( r46);
        // Create object for rPr
        RPr rpr46 = wmlObjectFactory.createRPr();
        r46.setRPr(rpr46);
        // Create object for rFonts
        RFonts rfonts97 = wmlObjectFactory.createRFonts();
        rpr46.setRFonts(rfonts97);
        rfonts97.setAscii( "Times New Roman");
        rfonts97.setCs( "Times New Roman");
        rfonts97.setEastAsia( "Times New Roman");
        rfonts97.setHAnsi( "Times New Roman");
        // Create object for t (wrapped in JAXBElement)
        Text text46 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped46 = wmlObjectFactory.createRT(text46);
        r46.getContent().add( textWrapped46);
        text46.setValue( "Eye Contact");
        // Create object for p
        P p54 = wmlObjectFactory.createP();
        tc39.getContent().add( p54);
        // Create object for pPr
        PPr ppr52 = wmlObjectFactory.createPPr();
        p54.setPPr(ppr52);
        // Create object for rPr
        ParaRPr pararpr52 = wmlObjectFactory.createParaRPr();
        ppr52.setRPr(pararpr52);
        // Create object for rFonts
        RFonts rfonts98 = wmlObjectFactory.createRFonts();
        pararpr52.setRFonts(rfonts98);
        rfonts98.setAscii( "Times New Roman");
        rfonts98.setCs( "Times New Roman");
        rfonts98.setEastAsia( "Times New Roman");
        rfonts98.setHAnsi( "Times New Roman");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue33 = wmlObjectFactory.createBooleanDefaultTrue();
        pararpr52.setB(booleandefaulttrue33);
        // Create object for tcPr
        TcPr tcpr39 = wmlObjectFactory.createTcPr();
        tc39.setTcPr(tcpr39);
        // Create object for tcW
        TblWidth tblwidth41 = wmlObjectFactory.createTblWidth();
        tcpr39.setTcW(tblwidth41);
        tblwidth41.setW( BigInteger.valueOf( 1811) );
        tblwidth41.setType( "dxa");

//        //------------------------------------------------------------------------------------------
//        // todo eyecontact - excellent
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc40 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped40 = wmlObjectFactory.createTrTc(tc40);
//        tr9.getContent().add( tcWrapped40);
//        // Create object for p
//        P p55 = wmlObjectFactory.createP();
//        tc40.getContent().add( p55);
//        // Create object for pPr
//        PPr ppr53 = wmlObjectFactory.createPPr();
//        p55.setPPr(ppr53);
//        // Create object for rPr
//        ParaRPr pararpr53 = wmlObjectFactory.createParaRPr();
//        ppr53.setRPr(pararpr53);
//        // Create object for rFonts
//        RFonts rfonts99 = wmlObjectFactory.createRFonts();
//        pararpr53.setRFonts(rfonts99);
//        rfonts99.setAscii( "Times New Roman");
//        rfonts99.setCs( "Times New Roman");
//        rfonts99.setEastAsia( "Times New Roman");
//        rfonts99.setHAnsi( "Times New Roman");
//        // Create object for b
//        BooleanDefaultTrue booleandefaulttrue34 = wmlObjectFactory.createBooleanDefaultTrue();
//        pararpr53.setB(booleandefaulttrue34);
//        // Create object for spacing
//        PPrBase.Spacing pprbasespacing9 = wmlObjectFactory.createPPrBaseSpacing();
//        ppr53.setSpacing(pprbasespacing9);
//        pprbasespacing9.setAfter( BigInteger.valueOf( 120) );
//        // Create object for r
//        R r47 = wmlObjectFactory.createR();
//        p55.getContent().add( r47);
//        // Create object for rPr
//        RPr rpr47 = wmlObjectFactory.createRPr();
//        r47.setRPr(rpr47);
//        // Create object for rFonts
//        RFonts rfonts100 = wmlObjectFactory.createRFonts();
//        rpr47.setRFonts(rfonts100);
//        rfonts100.setAscii( "Times New Roman");
//        rfonts100.setCs( "Times New Roman");
//        rfonts100.setEastAsia( "Times New Roman");
//        rfonts100.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text47 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped47 = wmlObjectFactory.createRT(text47);
//        r47.getContent().add( textWrapped47);
//        text47.setValue( "Holds attention of entire audience with the use of direct eye contact, seldom looking at notes");
//        // Create object for tcPr
//        TcPr tcpr40 = wmlObjectFactory.createTcPr();
//        tc40.setTcPr(tcpr40);
//        // Create object for tcW
//        TblWidth tblwidth42 = wmlObjectFactory.createTblWidth();
//        tcpr40.setTcW(tblwidth42);
//        tblwidth42.setW( BigInteger.valueOf( 2098) );
//        tblwidth42.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo eyecontact - good
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc41 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped41 = wmlObjectFactory.createTrTc(tc41);
//        tr9.getContent().add( tcWrapped41);
//        // Create object for p
//        P p56 = wmlObjectFactory.createP();
//        tc41.getContent().add( p56);
//        // Create object for pPr
//        PPr ppr54 = wmlObjectFactory.createPPr();
//        p56.setPPr(ppr54);
//        // Create object for rPr
//        ParaRPr pararpr54 = wmlObjectFactory.createParaRPr();
//        ppr54.setRPr(pararpr54);
//        // Create object for rFonts
//        RFonts rfonts101 = wmlObjectFactory.createRFonts();
//        pararpr54.setRFonts(rfonts101);
//        rfonts101.setAscii( "Times New Roman");
//        rfonts101.setCs( "Times New Roman");
//        rfonts101.setEastAsia( "Times New Roman");
//        rfonts101.setHAnsi( "Times New Roman");
//        // Create object for b
//        BooleanDefaultTrue booleandefaulttrue35 = wmlObjectFactory.createBooleanDefaultTrue();
//        pararpr54.setB(booleandefaulttrue35);
//        // Create object for spacing
//        PPrBase.Spacing pprbasespacing10 = wmlObjectFactory.createPPrBaseSpacing();
//        ppr54.setSpacing(pprbasespacing10);
//        pprbasespacing10.setAfter( BigInteger.valueOf( 120) );
//        // Create object for r
//        R r48 = wmlObjectFactory.createR();
//        p56.getContent().add( r48);
//        // Create object for rPr
//        RPr rpr48 = wmlObjectFactory.createRPr();
//        r48.setRPr(rpr48);
//        // Create object for rFonts
//        RFonts rfonts102 = wmlObjectFactory.createRFonts();
//        rpr48.setRFonts(rfonts102);
//        rfonts102.setAscii( "Times New Roman");
//        rfonts102.setCs( "Times New Roman");
//        rfonts102.setEastAsia( "Times New Roman");
//        rfonts102.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text48 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped48 = wmlObjectFactory.createRT(text48);
//        r48.getContent().add( textWrapped48);
//        text48.setValue( "Consistent use of direct eye contact with audience, but often returns to notes");
//        // Create object for tcPr
//        TcPr tcpr41 = wmlObjectFactory.createTcPr();
//        tc41.setTcPr(tcpr41);
//        // Create object for tcW
//        TblWidth tblwidth43 = wmlObjectFactory.createTblWidth();
//        tcpr41.setTcW(tblwidth43);
//        tblwidth43.setW( BigInteger.valueOf( 2098) );
//        tblwidth43.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo eyecontact - fair
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc42 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped42 = wmlObjectFactory.createTrTc(tc42);
//        tr9.getContent().add( tcWrapped42);
//        // Create object for p
//        P p57 = wmlObjectFactory.createP();
//        tc42.getContent().add( p57);
//        // Create object for pPr
//        PPr ppr55 = wmlObjectFactory.createPPr();
//        p57.setPPr(ppr55);
//        // Create object for rPr
//        ParaRPr pararpr55 = wmlObjectFactory.createParaRPr();
//        ppr55.setRPr(pararpr55);
//        // Create object for rFonts
//        RFonts rfonts103 = wmlObjectFactory.createRFonts();
//        pararpr55.setRFonts(rfonts103);
//        rfonts103.setAscii( "Times New Roman");
//        rfonts103.setCs( "Times New Roman");
//        rfonts103.setEastAsia( "Times New Roman");
//        rfonts103.setHAnsi( "Times New Roman");
//        // Create object for spacing
//        PPrBase.Spacing pprbasespacing11 = wmlObjectFactory.createPPrBaseSpacing();
//        ppr55.setSpacing(pprbasespacing11);
//        pprbasespacing11.setAfter( BigInteger.valueOf( 120) );
//        // Create object for r
//        R r49 = wmlObjectFactory.createR();
//        p57.getContent().add( r49);
//        // Create object for rPr
//        RPr rpr49 = wmlObjectFactory.createRPr();
//        r49.setRPr(rpr49);
//        // Create object for rFonts
//        RFonts rfonts104 = wmlObjectFactory.createRFonts();
//        rpr49.setRFonts(rfonts104);
//        rfonts104.setAscii( "Times New Roman");
//        rfonts104.setCs( "Times New Roman");
//        rfonts104.setEastAsia( "Times New Roman");
//        rfonts104.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text49 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped49 = wmlObjectFactory.createRT(text49);
//        r49.getContent().add( textWrapped49);
//        text49.setValue( "Displays minimal eye contact with audience, while reading mostly from the notes.");
//        // Create object for tcPr
//        TcPr tcpr42 = wmlObjectFactory.createTcPr();
//        tc42.setTcPr(tcpr42);
//        // Create object for tcW
//        TblWidth tblwidth44 = wmlObjectFactory.createTblWidth();
//        tcpr42.setTcW(tblwidth44);
//        tblwidth44.setW( BigInteger.valueOf( 2098) );
//        tblwidth44.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo eyecontact - poor
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc43 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped43 = wmlObjectFactory.createTrTc(tc43);
//        tr9.getContent().add( tcWrapped43);
//        // Create object for p
//        P p58 = wmlObjectFactory.createP();
//        tc43.getContent().add( p58);
//        // Create object for pPr
//        PPr ppr56 = wmlObjectFactory.createPPr();
//        p58.setPPr(ppr56);
//        // Create object for rPr
//        ParaRPr pararpr56 = wmlObjectFactory.createParaRPr();
//        ppr56.setRPr(pararpr56);
//        // Create object for rFonts
//        RFonts rfonts105 = wmlObjectFactory.createRFonts();
//        pararpr56.setRFonts(rfonts105);
//        rfonts105.setAscii( "Times New Roman");
//        rfonts105.setCs( "Times New Roman");
//        rfonts105.setEastAsia( "Times New Roman");
//        rfonts105.setHAnsi( "Times New Roman");
//        // Create object for b
//        BooleanDefaultTrue booleandefaulttrue36 = wmlObjectFactory.createBooleanDefaultTrue();
//        pararpr56.setB(booleandefaulttrue36);
//        // Create object for r
//        R r50 = wmlObjectFactory.createR();
//        p58.getContent().add( r50);
//        // Create object for rPr
//        RPr rpr50 = wmlObjectFactory.createRPr();
//        r50.setRPr(rpr50);
//        // Create object for rFonts
//        RFonts rfonts106 = wmlObjectFactory.createRFonts();
//        rpr50.setRFonts(rfonts106);
//        rfonts106.setAscii( "Times New Roman");
//        rfonts106.setCs( "Times New Roman");
//        rfonts106.setEastAsia( "Times New Roman");
//        rfonts106.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text50 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped50 = wmlObjectFactory.createRT(text50);
//        r50.getContent().add( textWrapped50);
//        text50.setValue( "No eye contact with audience; entire presentation is read from notes");
//        // Create object for tcPr
//        TcPr tcpr43 = wmlObjectFactory.createTcPr();
//        tc43.setTcPr(tcpr43);
//        // Create object for tcW
//        TblWidth tblwidth45 = wmlObjectFactory.createTblWidth();
//        tcpr43.setTcW(tblwidth45);
//        tblwidth45.setW( BigInteger.valueOf( 2098) );
//        tblwidth45.setType( "dxa");

        makeUpLanguageTableEyeContactRow(assessment, wmlObjectFactory, tr9);

        //------------------------------------------------------------------------------------------
        // todo enthusiasm - [tableRow]

        // Create object for trPr
        TrPr trpr9 = wmlObjectFactory.createTrPr();
        tr9.setTrPr(trpr9);
        // Create object for jc (wrapped in JAXBElement)
        Jc jc15 = wmlObjectFactory.createJc();
        JAXBElement<org.docx4j.wml.Jc> jcWrapped9 = wmlObjectFactory.createCTTrPrBaseJc(jc15);
        trpr9.getCnfStyleOrDivIdOrGridBefore().add( jcWrapped9);
        jc15.setVal(org.docx4j.wml.JcEnumeration.CENTER);
        // Create object for tr
        Tr tr10 = wmlObjectFactory.createTr();
        tbl3.getContent().add( tr10);
        // Create object for tc (wrapped in JAXBElement)
        Tc tc44 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped44 = wmlObjectFactory.createTrTc(tc44);
        tr10.getContent().add( tcWrapped44);
        // Create object for p
        P p59 = wmlObjectFactory.createP();
        tc44.getContent().add( p59);
        // Create object for pPr
        PPr ppr57 = wmlObjectFactory.createPPr();
        p59.setPPr(ppr57);
        // Create object for rPr
        ParaRPr pararpr57 = wmlObjectFactory.createParaRPr();
        ppr57.setRPr(pararpr57);
        // Create object for rFonts
        RFonts rfonts107 = wmlObjectFactory.createRFonts();
        pararpr57.setRFonts(rfonts107);
        rfonts107.setAscii( "Times New Roman");
        rfonts107.setCs( "Times New Roman");
        rfonts107.setEastAsia( "Times New Roman");
        rfonts107.setHAnsi( "Times New Roman");
        // Create object for r
        R r51 = wmlObjectFactory.createR();
        p59.getContent().add( r51);
        // Create object for rPr
        RPr rpr51 = wmlObjectFactory.createRPr();
        r51.setRPr(rpr51);
        // Create object for rFonts
        RFonts rfonts108 = wmlObjectFactory.createRFonts();
        rpr51.setRFonts(rfonts108);
        rfonts108.setAscii( "Times New Roman");
        rfonts108.setCs( "Times New Roman");
        rfonts108.setEastAsia( "Times New Roman");
        rfonts108.setHAnsi( "Times New Roman");
        // Create object for t (wrapped in JAXBElement)
        Text text51 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped51 = wmlObjectFactory.createRT(text51);
        r51.getContent().add( textWrapped51);
        text51.setValue( "Enthusiasm");
        // Create object for tcPr
        TcPr tcpr44 = wmlObjectFactory.createTcPr();
        tc44.setTcPr(tcpr44);
        // Create object for tcW
        TblWidth tblwidth46 = wmlObjectFactory.createTblWidth();
        tcpr44.setTcW(tblwidth46);
        tblwidth46.setW( BigInteger.valueOf( 1811) );
        tblwidth46.setType( "dxa");

//        //------------------------------------------------------------------------------------------
//        // todo enthusiasm - excellent
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc45 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped45 = wmlObjectFactory.createTrTc(tc45);
//        tr10.getContent().add( tcWrapped45);
//        // Create object for p
//        P p60 = wmlObjectFactory.createP();
//        tc45.getContent().add( p60);
//        // Create object for pPr
//        PPr ppr58 = wmlObjectFactory.createPPr();
//        p60.setPPr(ppr58);
//        // Create object for rPr
//        ParaRPr pararpr58 = wmlObjectFactory.createParaRPr();
//        ppr58.setRPr(pararpr58);
//        // Create object for rFonts
//        RFonts rfonts109 = wmlObjectFactory.createRFonts();
//        pararpr58.setRFonts(rfonts109);
//        rfonts109.setAscii( "Times New Roman");
//        rfonts109.setCs( "Times New Roman");
//        rfonts109.setEastAsia( "Times New Roman");
//        rfonts109.setHAnsi( "Times New Roman");
//        // Create object for spacing
//        PPrBase.Spacing pprbasespacing12 = wmlObjectFactory.createPPrBaseSpacing();
//        ppr58.setSpacing(pprbasespacing12);
//        pprbasespacing12.setAfter( BigInteger.valueOf( 120) );
//        // Create object for r
//        R r52 = wmlObjectFactory.createR();
//        p60.getContent().add( r52);
//        // Create object for rPr
//        RPr rpr52 = wmlObjectFactory.createRPr();
//        r52.setRPr(rpr52);
//        // Create object for rFonts
//        RFonts rfonts110 = wmlObjectFactory.createRFonts();
//        rpr52.setRFonts(rfonts110);
//        rfonts110.setAscii( "Times New Roman");
//        rfonts110.setCs( "Times New Roman");
//        rfonts110.setEastAsia( "Times New Roman");
//        rfonts110.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text52 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped52 = wmlObjectFactory.createRT(text52);
//        r52.getContent().add( textWrapped52);
//        text52.setValue( "Demonstrates a strong, positive feeling about topic during entire presentation.");
//        // Create object for tcPr
//        TcPr tcpr45 = wmlObjectFactory.createTcPr();
//        tc45.setTcPr(tcpr45);
//        // Create object for tcW
//        TblWidth tblwidth47 = wmlObjectFactory.createTblWidth();
//        tcpr45.setTcW(tblwidth47);
//        tblwidth47.setW( BigInteger.valueOf( 2098) );
//        tblwidth47.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo enthusiasm - good
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc46 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped46 = wmlObjectFactory.createTrTc(tc46);
//        tr10.getContent().add( tcWrapped46);
//        // Create object for p
//        P p61 = wmlObjectFactory.createP();
//        tc46.getContent().add( p61);
//        // Create object for pPr
//        PPr ppr59 = wmlObjectFactory.createPPr();
//        p61.setPPr(ppr59);
//        // Create object for rPr
//        ParaRPr pararpr59 = wmlObjectFactory.createParaRPr();
//        ppr59.setRPr(pararpr59);
//        // Create object for rFonts
//        RFonts rfonts111 = wmlObjectFactory.createRFonts();
//        pararpr59.setRFonts(rfonts111);
//        rfonts111.setAscii( "Times New Roman");
//        rfonts111.setCs( "Times New Roman");
//        rfonts111.setEastAsia( "Times New Roman");
//        rfonts111.setHAnsi( "Times New Roman");
//        // Create object for r
//        R r53 = wmlObjectFactory.createR();
//        p61.getContent().add( r53);
//        // Create object for rPr
//        RPr rpr53 = wmlObjectFactory.createRPr();
//        r53.setRPr(rpr53);
//        // Create object for rFonts
//        RFonts rfonts112 = wmlObjectFactory.createRFonts();
//        rpr53.setRFonts(rfonts112);
//        rfonts112.setAscii( "Times New Roman");
//        rfonts112.setCs( "Times New Roman");
//        rfonts112.setEastAsia( "Times New Roman");
//        rfonts112.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text53 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped53 = wmlObjectFactory.createRT(text53);
//        r53.getContent().add( textWrapped53);
//        text53.setValue( "Mostly shows positive feelings about topic.");
//        // Create object for p
//        P p62 = wmlObjectFactory.createP();
//        tc46.getContent().add( p62);
//        // Create object for pPr
//        PPr ppr60 = wmlObjectFactory.createPPr();
//        p62.setPPr(ppr60);
//        // Create object for rPr
//        ParaRPr pararpr60 = wmlObjectFactory.createParaRPr();
//        ppr60.setRPr(pararpr60);
//        // Create object for rFonts
//        RFonts rfonts113 = wmlObjectFactory.createRFonts();
//        pararpr60.setRFonts(rfonts113);
//        rfonts113.setAscii( "Times New Roman");
//        rfonts113.setCs( "Times New Roman");
//        rfonts113.setEastAsia( "Times New Roman");
//        rfonts113.setHAnsi( "Times New Roman");
//        // Create object for b
//        BooleanDefaultTrue booleandefaulttrue37 = wmlObjectFactory.createBooleanDefaultTrue();
//        pararpr60.setB(booleandefaulttrue37);
//        // Create object for tcPr
//        TcPr tcpr46 = wmlObjectFactory.createTcPr();
//        tc46.setTcPr(tcpr46);
//        // Create object for tcW
//        TblWidth tblwidth48 = wmlObjectFactory.createTblWidth();
//        tcpr46.setTcW(tblwidth48);
//        tblwidth48.setW( BigInteger.valueOf( 2098) );
//        tblwidth48.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo enthusiasm - fair
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc47 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped47 = wmlObjectFactory.createTrTc(tc47);
//        tr10.getContent().add( tcWrapped47);
//        // Create object for p
//        P p63 = wmlObjectFactory.createP();
//        tc47.getContent().add( p63);
//        // Create object for pPr
//        PPr ppr61 = wmlObjectFactory.createPPr();
//        p63.setPPr(ppr61);
//        // Create object for rPr
//        ParaRPr pararpr61 = wmlObjectFactory.createParaRPr();
//        ppr61.setRPr(pararpr61);
//        // Create object for rFonts
//        RFonts rfonts114 = wmlObjectFactory.createRFonts();
//        pararpr61.setRFonts(rfonts114);
//        rfonts114.setAscii( "Times New Roman");
//        rfonts114.setCs( "Times New Roman");
//        rfonts114.setEastAsia( "Times New Roman");
//        rfonts114.setHAnsi( "Times New Roman");
//        // Create object for r
//        R r54 = wmlObjectFactory.createR();
//        p63.getContent().add( r54);
//        // Create object for rPr
//        RPr rpr54 = wmlObjectFactory.createRPr();
//        r54.setRPr(rpr54);
//        // Create object for rFonts
//        RFonts rfonts115 = wmlObjectFactory.createRFonts();
//        rpr54.setRFonts(rfonts115);
//        rfonts115.setAscii( "Times New Roman");
//        rfonts115.setCs( "Times New Roman");
//        rfonts115.setEastAsia( "Times New Roman");
//        rfonts115.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text54 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped54 = wmlObjectFactory.createRT(text54);
//        r54.getContent().add( textWrapped54);
//        text54.setValue( "Shows some negativity toward topic presented.");
//        // Create object for p
//        P p64 = wmlObjectFactory.createP();
//        tc47.getContent().add( p64);
//        // Create object for pPr
//        PPr ppr62 = wmlObjectFactory.createPPr();
//        p64.setPPr(ppr62);
//        // Create object for rPr
//        ParaRPr pararpr62 = wmlObjectFactory.createParaRPr();
//        ppr62.setRPr(pararpr62);
//        // Create object for rFonts
//        RFonts rfonts116 = wmlObjectFactory.createRFonts();
//        pararpr62.setRFonts(rfonts116);
//        rfonts116.setAscii( "Times New Roman");
//        rfonts116.setCs( "Times New Roman");
//        rfonts116.setEastAsia( "Times New Roman");
//        rfonts116.setHAnsi( "Times New Roman");
//        // Create object for b
//        BooleanDefaultTrue booleandefaulttrue38 = wmlObjectFactory.createBooleanDefaultTrue();
//        pararpr62.setB(booleandefaulttrue38);
//        // Create object for tcPr
//        TcPr tcpr47 = wmlObjectFactory.createTcPr();
//        tc47.setTcPr(tcpr47);
//        // Create object for tcW
//        TblWidth tblwidth49 = wmlObjectFactory.createTblWidth();
//        tcpr47.setTcW(tblwidth49);
//        tblwidth49.setW( BigInteger.valueOf( 2098) );
//        tblwidth49.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo enthusiasm - poor
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc48 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped48 = wmlObjectFactory.createTrTc(tc48);
//        tr10.getContent().add( tcWrapped48);
//        // Create object for p
//        P p65 = wmlObjectFactory.createP();
//        tc48.getContent().add( p65);
//        // Create object for pPr
//        PPr ppr63 = wmlObjectFactory.createPPr();
//        p65.setPPr(ppr63);
//        // Create object for rPr
//        ParaRPr pararpr63 = wmlObjectFactory.createParaRPr();
//        ppr63.setRPr(pararpr63);
//        // Create object for rFonts
//        RFonts rfonts117 = wmlObjectFactory.createRFonts();
//        pararpr63.setRFonts(rfonts117);
//        rfonts117.setAscii( "Times New Roman");
//        rfonts117.setCs( "Times New Roman");
//        rfonts117.setEastAsia( "Times New Roman");
//        rfonts117.setHAnsi( "Times New Roman");
//        // Create object for r
//        R r55 = wmlObjectFactory.createR();
//        p65.getContent().add( r55);
//        // Create object for rPr
//        RPr rpr55 = wmlObjectFactory.createRPr();
//        r55.setRPr(rpr55);
//        // Create object for rFonts
//        RFonts rfonts118 = wmlObjectFactory.createRFonts();
//        rpr55.setRFonts(rfonts118);
//        rfonts118.setAscii( "Times New Roman");
//        rfonts118.setCs( "Times New Roman");
//        rfonts118.setEastAsia( "Times New Roman");
//        rfonts118.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text55 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped55 = wmlObjectFactory.createRT(text55);
//        r55.getContent().add( textWrapped55);
//        text55.setValue( "Shows no interest in topic presented.");
//        // Create object for p
//        P p66 = wmlObjectFactory.createP();
//        tc48.getContent().add( p66);
//        // Create object for pPr
//        PPr ppr64 = wmlObjectFactory.createPPr();
//        p66.setPPr(ppr64);
//        // Create object for rPr
//        ParaRPr pararpr64 = wmlObjectFactory.createParaRPr();
//        ppr64.setRPr(pararpr64);
//        // Create object for rFonts
//        RFonts rfonts119 = wmlObjectFactory.createRFonts();
//        pararpr64.setRFonts(rfonts119);
//        rfonts119.setAscii( "Times New Roman");
//        rfonts119.setCs( "Times New Roman");
//        rfonts119.setEastAsia( "Times New Roman");
//        rfonts119.setHAnsi( "Times New Roman");
//        // Create object for b
//        BooleanDefaultTrue booleandefaulttrue39 = wmlObjectFactory.createBooleanDefaultTrue();
//        pararpr64.setB(booleandefaulttrue39);
//        // Create object for tcPr
//        TcPr tcpr48 = wmlObjectFactory.createTcPr();
//        tc48.setTcPr(tcpr48);
//        // Create object for tcW
//        TblWidth tblwidth50 = wmlObjectFactory.createTblWidth();
//        tcpr48.setTcW(tblwidth50);
//        tblwidth50.setW( BigInteger.valueOf( 2098) );
//        tblwidth50.setType( "dxa");

        makeUpLanguageTableEnthusiasmRow(assessment, wmlObjectFactory, tr10);

        //------------------------------------------------------------------------------------------
        // todo elocution - [tableRow]

        // Create object for trPr
        TrPr trpr10 = wmlObjectFactory.createTrPr();
        tr10.setTrPr(trpr10);
        // Create object for jc (wrapped in JAXBElement)
        Jc jc16 = wmlObjectFactory.createJc();
        JAXBElement<org.docx4j.wml.Jc> jcWrapped10 = wmlObjectFactory.createCTTrPrBaseJc(jc16);
        trpr10.getCnfStyleOrDivIdOrGridBefore().add( jcWrapped10);
        jc16.setVal(org.docx4j.wml.JcEnumeration.CENTER);
        // Create object for tr
        Tr tr11 = wmlObjectFactory.createTr();
        tbl3.getContent().add( tr11);
        // Create object for tc (wrapped in JAXBElement)
        Tc tc49 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped49 = wmlObjectFactory.createTrTc(tc49);
        tr11.getContent().add( tcWrapped49);
        // Create object for p
        P p67 = wmlObjectFactory.createP();
        tc49.getContent().add( p67);
        // Create object for pPr
        PPr ppr65 = wmlObjectFactory.createPPr();
        p67.setPPr(ppr65);
        // Create object for rPr
        ParaRPr pararpr65 = wmlObjectFactory.createParaRPr();
        ppr65.setRPr(pararpr65);
        // Create object for rFonts
        RFonts rfonts120 = wmlObjectFactory.createRFonts();
        pararpr65.setRFonts(rfonts120);
        rfonts120.setAscii( "Times New Roman");
        rfonts120.setCs( "Times New Roman");
        rfonts120.setEastAsia( "Times New Roman");
        rfonts120.setHAnsi( "Times New Roman");
        // Create object for r
        R r56 = wmlObjectFactory.createR();
        p67.getContent().add( r56);
        // Create object for rPr
        RPr rpr56 = wmlObjectFactory.createRPr();
        r56.setRPr(rpr56);
        // Create object for rFonts
        RFonts rfonts121 = wmlObjectFactory.createRFonts();
        rpr56.setRFonts(rfonts121);
        rfonts121.setAscii( "Times New Roman");
        rfonts121.setCs( "Times New Roman");
        rfonts121.setEastAsia( "Times New Roman");
        rfonts121.setHAnsi( "Times New Roman");
        // Create object for t (wrapped in JAXBElement)
        Text text56 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped56 = wmlObjectFactory.createRT(text56);
        r56.getContent().add( textWrapped56);
        text56.setValue( "Elocution");
        // Create object for p
        P p68 = wmlObjectFactory.createP();
        tc49.getContent().add( p68);
        // Create object for pPr
        PPr ppr66 = wmlObjectFactory.createPPr();
        p68.setPPr(ppr66);
        // Create object for rPr
        ParaRPr pararpr66 = wmlObjectFactory.createParaRPr();
        ppr66.setRPr(pararpr66);
        // Create object for rFonts
        RFonts rfonts122 = wmlObjectFactory.createRFonts();
        pararpr66.setRFonts(rfonts122);
        rfonts122.setAscii( "Times New Roman");
        rfonts122.setCs( "Times New Roman");
        rfonts122.setEastAsia( "Times New Roman");
        rfonts122.setHAnsi( "Times New Roman");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue40 = wmlObjectFactory.createBooleanDefaultTrue();
        pararpr66.setB(booleandefaulttrue40);
        // Create object for tcPr
        TcPr tcpr49 = wmlObjectFactory.createTcPr();
        tc49.setTcPr(tcpr49);
        // Create object for tcW
        TblWidth tblwidth51 = wmlObjectFactory.createTblWidth();
        tcpr49.setTcW(tblwidth51);
        tblwidth51.setW( BigInteger.valueOf( 1811) );
        tblwidth51.setType( "dxa");

//        //------------------------------------------------------------------------------------------
//        // todo elocution - excellent
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc50 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped50 = wmlObjectFactory.createTrTc(tc50);
//        tr11.getContent().add( tcWrapped50);
//        // Create object for p
//        P p69 = wmlObjectFactory.createP();
//        tc50.getContent().add( p69);
//        // Create object for pPr
//        PPr ppr67 = wmlObjectFactory.createPPr();
//        p69.setPPr(ppr67);
//        // Create object for rPr
//        ParaRPr pararpr67 = wmlObjectFactory.createParaRPr();
//        ppr67.setRPr(pararpr67);
//        // Create object for rFonts
//        RFonts rfonts123 = wmlObjectFactory.createRFonts();
//        pararpr67.setRFonts(rfonts123);
//        rfonts123.setAscii( "Times New Roman");
//        rfonts123.setCs( "Times New Roman");
//        rfonts123.setEastAsia( "Times New Roman");
//        rfonts123.setHAnsi( "Times New Roman");
//        // Create object for b
//        BooleanDefaultTrue booleandefaulttrue41 = wmlObjectFactory.createBooleanDefaultTrue();
//        pararpr67.setB(booleandefaulttrue41);
//        // Create object for r
//        R r57 = wmlObjectFactory.createR();
//        p69.getContent().add( r57);
//        // Create object for rPr
//        RPr rpr57 = wmlObjectFactory.createRPr();
//        r57.setRPr(rpr57);
//        // Create object for rFonts
//        RFonts rfonts124 = wmlObjectFactory.createRFonts();
//        rpr57.setRFonts(rfonts124);
//        rfonts124.setAscii( "Times New Roman");
//        rfonts124.setCs( "Times New Roman");
//        rfonts124.setEastAsia( "Times New Roman");
//        rfonts124.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text57 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped57 = wmlObjectFactory.createRT(text57);
//        r57.getContent().add( textWrapped57);
//        text57.setValue( "Student uses a clear voice so that all audience members can hear presentation");
//        // Create object for tcPr
//        TcPr tcpr50 = wmlObjectFactory.createTcPr();
//        tc50.setTcPr(tcpr50);
//        // Create object for tcW
//        TblWidth tblwidth52 = wmlObjectFactory.createTblWidth();
//        tcpr50.setTcW(tblwidth52);
//        tblwidth52.setW( BigInteger.valueOf( 2098) );
//        tblwidth52.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo elocution - good
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc51 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped51 = wmlObjectFactory.createTrTc(tc51);
//        tr11.getContent().add( tcWrapped51);
//        // Create object for p
//        P p70 = wmlObjectFactory.createP();
//        tc51.getContent().add( p70);
//        // Create object for pPr
//        PPr ppr68 = wmlObjectFactory.createPPr();
//        p70.setPPr(ppr68);
//        // Create object for rPr
//        ParaRPr pararpr68 = wmlObjectFactory.createParaRPr();
//        ppr68.setRPr(pararpr68);
//        // Create object for rFonts
//        RFonts rfonts125 = wmlObjectFactory.createRFonts();
//        pararpr68.setRFonts(rfonts125);
//        rfonts125.setAscii( "Times New Roman");
//        rfonts125.setCs( "Times New Roman");
//        rfonts125.setEastAsia( "Times New Roman");
//        rfonts125.setHAnsi( "Times New Roman");
//        // Create object for r
//        R r58 = wmlObjectFactory.createR();
//        p70.getContent().add( r58);
//        // Create object for rPr
//        RPr rpr58 = wmlObjectFactory.createRPr();
//        r58.setRPr(rpr58);
//        // Create object for rFonts
//        RFonts rfonts126 = wmlObjectFactory.createRFonts();
//        rpr58.setRFonts(rfonts126);
//        rfonts126.setAscii( "Times New Roman");
//        rfonts126.setCs( "Times New Roman");
//        rfonts126.setEastAsia( "Times New Roman");
//        rfonts126.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text58 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped58 = wmlObjectFactory.createRT(text58);
//        r58.getContent().add( textWrapped58);
//        text58.setValue( "Studentâ€™s voice is clear. Most audience members can hear presentation.");
//        // Create object for tcPr
//        TcPr tcpr51 = wmlObjectFactory.createTcPr();
//        tc51.setTcPr(tcpr51);
//        // Create object for tcW
//        TblWidth tblwidth53 = wmlObjectFactory.createTblWidth();
//        tcpr51.setTcW(tblwidth53);
//        tblwidth53.setW( BigInteger.valueOf( 2098) );
//        tblwidth53.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo elocution - fair
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc52 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped52 = wmlObjectFactory.createTrTc(tc52);
//        tr11.getContent().add( tcWrapped52);
//        // Create object for p
//        P p71 = wmlObjectFactory.createP();
//        tc52.getContent().add( p71);
//        // Create object for pPr
//        PPr ppr69 = wmlObjectFactory.createPPr();
//        p71.setPPr(ppr69);
//        // Create object for rPr
//        ParaRPr pararpr69 = wmlObjectFactory.createParaRPr();
//        ppr69.setRPr(pararpr69);
//        // Create object for rFonts
//        RFonts rfonts127 = wmlObjectFactory.createRFonts();
//        pararpr69.setRFonts(rfonts127);
//        rfonts127.setAscii( "Times New Roman");
//        rfonts127.setCs( "Times New Roman");
//        rfonts127.setEastAsia( "Times New Roman");
//        rfonts127.setHAnsi( "Times New Roman");
//        // Create object for r
//        R r59 = wmlObjectFactory.createR();
//        p71.getContent().add( r59);
//        // Create object for rPr
//        RPr rpr59 = wmlObjectFactory.createRPr();
//        r59.setRPr(rpr59);
//        // Create object for rFonts
//        RFonts rfonts128 = wmlObjectFactory.createRFonts();
//        rpr59.setRFonts(rfonts128);
//        rfonts128.setAscii( "Times New Roman");
//        rfonts128.setCs( "Times New Roman");
//        rfonts128.setEastAsia( "Times New Roman");
//        rfonts128.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text59 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped59 = wmlObjectFactory.createRT(text59);
//        r59.getContent().add( textWrapped59);
//        text59.setValue( "Studentâ€™s voice is low. Audience has difficulty hearing presentation.");
//        // Create object for tcPr
//        TcPr tcpr52 = wmlObjectFactory.createTcPr();
//        tc52.setTcPr(tcpr52);
//        // Create object for tcW
//        TblWidth tblwidth54 = wmlObjectFactory.createTblWidth();
//        tcpr52.setTcW(tblwidth54);
//        tblwidth54.setW( BigInteger.valueOf( 2098) );
//        tblwidth54.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo elocution - poor
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc53 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped53 = wmlObjectFactory.createTrTc(tc53);
//        tr11.getContent().add( tcWrapped53);
//        // Create object for p
//        P p72 = wmlObjectFactory.createP();
//        tc53.getContent().add( p72);
//        // Create object for pPr
//        PPr ppr70 = wmlObjectFactory.createPPr();
//        p72.setPPr(ppr70);
//        // Create object for rPr
//        ParaRPr pararpr70 = wmlObjectFactory.createParaRPr();
//        ppr70.setRPr(pararpr70);
//        // Create object for rFonts
//        RFonts rfonts129 = wmlObjectFactory.createRFonts();
//        pararpr70.setRFonts(rfonts129);
//        rfonts129.setAscii( "Times New Roman");
//        rfonts129.setCs( "Times New Roman");
//        rfonts129.setEastAsia( "Times New Roman");
//        rfonts129.setHAnsi( "Times New Roman");
//        // Create object for r
//        R r60 = wmlObjectFactory.createR();
//        p72.getContent().add( r60);
//        // Create object for rPr
//        RPr rpr60 = wmlObjectFactory.createRPr();
//        r60.setRPr(rpr60);
//        // Create object for rFonts
//        RFonts rfonts130 = wmlObjectFactory.createRFonts();
//        rpr60.setRFonts(rfonts130);
//        rfonts130.setAscii( "Times New Roman");
//        rfonts130.setCs( "Times New Roman");
//        rfonts130.setEastAsia( "Times New Roman");
//        rfonts130.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text60 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped60 = wmlObjectFactory.createRT(text60);
//        r60.getContent().add( textWrapped60);
//        text60.setValue( "Student mumbles, speaks too quietly for a majority of audience to hear.");
//        // Create object for tcPr
//        TcPr tcpr53 = wmlObjectFactory.createTcPr();
//        tc53.setTcPr(tcpr53);
//        // Create object for tcW
//        TblWidth tblwidth55 = wmlObjectFactory.createTblWidth();
//        tcpr53.setTcW(tblwidth55);
//        tblwidth55.setW( BigInteger.valueOf( 2098) );
//        tblwidth55.setType( "dxa");

        makeUpLanguageTableElocutionRow(assessment, wmlObjectFactory, tr11);

        //------------------------------------------------------------------------------------------
        // todo technical - [table]

        // Create object for trPr
        TrPr trpr11 = wmlObjectFactory.createTrPr();
        tr11.setTrPr(trpr11);
        // Create object for jc (wrapped in JAXBElement)
        Jc jc17 = wmlObjectFactory.createJc();
        JAXBElement<org.docx4j.wml.Jc> jcWrapped11 = wmlObjectFactory.createCTTrPrBaseJc(jc17);
        trpr11.getCnfStyleOrDivIdOrGridBefore().add( jcWrapped11);
        jc17.setVal(org.docx4j.wml.JcEnumeration.CENTER);
        // Create object for tblPr
        TblPr tblpr3 = wmlObjectFactory.createTblPr();
        tbl3.setTblPr(tblpr3);
        // Create object for tblStyle
        CTTblPrBase.TblStyle tblprbasetblstyle3 = wmlObjectFactory.createCTTblPrBaseTblStyle();
        tblpr3.setTblStyle(tblprbasetblstyle3);
        tblprbasetblstyle3.setVal( "a1");
        // Create object for tblBorders
        TblBorders tblborders3 = wmlObjectFactory.createTblBorders();
        tblpr3.setTblBorders(tblborders3);
        // Create object for insideH
        CTBorder border45 = wmlObjectFactory.createCTBorder();
        tblborders3.setInsideH(border45);
        border45.setVal(org.docx4j.wml.STBorder.SINGLE);
        border45.setSz( BigInteger.valueOf( 4) );
        border45.setColor( "000000");
        border45.setSpace( BigInteger.valueOf( 0) );
        // Create object for top
        CTBorder border46 = wmlObjectFactory.createCTBorder();
        tblborders3.setTop(border46);
        border46.setVal(org.docx4j.wml.STBorder.SINGLE);
        border46.setSz( BigInteger.valueOf( 4) );
        border46.setColor( "000000");
        border46.setSpace( BigInteger.valueOf( 0) );
        // Create object for bottom
        CTBorder border47 = wmlObjectFactory.createCTBorder();
        tblborders3.setBottom(border47);
        border47.setVal(org.docx4j.wml.STBorder.SINGLE);
        border47.setSz( BigInteger.valueOf( 4) );
        border47.setColor( "000000");
        border47.setSpace( BigInteger.valueOf( 0) );
        // Create object for insideV
        CTBorder border48 = wmlObjectFactory.createCTBorder();
        tblborders3.setInsideV(border48);
        border48.setVal(org.docx4j.wml.STBorder.SINGLE);
        border48.setSz( BigInteger.valueOf( 4) );
        border48.setColor( "000000");
        border48.setSpace( BigInteger.valueOf( 0) );
        // Create object for right
        CTBorder border49 = wmlObjectFactory.createCTBorder();
        tblborders3.setRight(border49);
        border49.setVal(org.docx4j.wml.STBorder.SINGLE);
        border49.setSz( BigInteger.valueOf( 4) );
        border49.setColor( "000000");
        border49.setSpace( BigInteger.valueOf( 0) );
        // Create object for left
        CTBorder border50 = wmlObjectFactory.createCTBorder();
        tblborders3.setLeft(border50);
        border50.setVal(org.docx4j.wml.STBorder.SINGLE);
        border50.setSz( BigInteger.valueOf( 4) );
        border50.setColor( "000000");
        border50.setSpace( BigInteger.valueOf( 0) );
        // Create object for jc
        Jc jc18 = wmlObjectFactory.createJc();
        tblpr3.setJc(jc18);
        jc18.setVal(org.docx4j.wml.JcEnumeration.CENTER);
        // Create object for tblW
        TblWidth tblwidth56 = wmlObjectFactory.createTblWidth();
        tblpr3.setTblW(tblwidth56);
        tblwidth56.setW( BigInteger.valueOf( 10203) );
        tblwidth56.setType( "dxa");
        // Create object for tblLayout
        CTTblLayoutType tbllayouttype3 = wmlObjectFactory.createCTTblLayoutType();
        tblpr3.setTblLayout(tbllayouttype3);
        tbllayouttype3.setType(org.docx4j.wml.STTblLayoutType.FIXED);
        // Create object for tblLook
        CTTblLook tbllook3 = wmlObjectFactory.createCTTblLook();
        tblpr3.setTblLook(tbllook3);
        tbllook3.setVal( "0400");
        tbllook3.setFirstColumn(org.docx4j.sharedtypes.STOnOff.ZERO);
        tbllook3.setFirstRow(org.docx4j.sharedtypes.STOnOff.ZERO);
        tbllook3.setLastColumn(org.docx4j.sharedtypes.STOnOff.ZERO);
        tbllook3.setLastRow(org.docx4j.sharedtypes.STOnOff.ZERO);
        tbllook3.setNoHBand(org.docx4j.sharedtypes.STOnOff.ZERO);
        tbllook3.setNoVBand(org.docx4j.sharedtypes.STOnOff.ONE);
        // Create object for tblGrid
        TblGrid tblgrid3 = wmlObjectFactory.createTblGrid();
        tbl3.setTblGrid(tblgrid3);
        // Create object for gridCol
        TblGridCol tblgridcol12 = wmlObjectFactory.createTblGridCol();
        tblgrid3.getGridCol().add( tblgridcol12);
        tblgridcol12.setW( BigInteger.valueOf( 1811) );
        // Create object for gridCol
        TblGridCol tblgridcol13 = wmlObjectFactory.createTblGridCol();
        tblgrid3.getGridCol().add( tblgridcol13);
        tblgridcol13.setW( BigInteger.valueOf( 2098) );
        // Create object for gridCol
        TblGridCol tblgridcol14 = wmlObjectFactory.createTblGridCol();
        tblgrid3.getGridCol().add( tblgridcol14);
        tblgridcol14.setW( BigInteger.valueOf( 2098) );
        // Create object for gridCol
        TblGridCol tblgridcol15 = wmlObjectFactory.createTblGridCol();
        tblgrid3.getGridCol().add( tblgridcol15);
        tblgridcol15.setW( BigInteger.valueOf( 2098) );
        // Create object for gridCol
        TblGridCol tblgridcol16 = wmlObjectFactory.createTblGridCol();
        tblgrid3.getGridCol().add( tblgridcol16);
        tblgridcol16.setW( BigInteger.valueOf( 2098) );
        // Create object for p
        P p73 = wmlObjectFactory.createP();
        body.getContent().add( p73);
        // Create object for pPr
        PPr ppr71 = wmlObjectFactory.createPPr();
        p73.setPPr(ppr71);
        // Create object for rPr
        ParaRPr pararpr71 = wmlObjectFactory.createParaRPr();
        ppr71.setRPr(pararpr71);
        // Create object for rFonts
        RFonts rfonts131 = wmlObjectFactory.createRFonts();
        pararpr71.setRFonts(rfonts131);
        rfonts131.setAscii( "Times");
        rfonts131.setCs( "Times");
        rfonts131.setEastAsia( "Times");
        rfonts131.setHAnsi( "Times");
        // Create object for spacing
        PPrBase.Spacing pprbasespacing13 = wmlObjectFactory.createPPrBaseSpacing();
        ppr71.setSpacing(pprbasespacing13);
        pprbasespacing13.setAfter( BigInteger.valueOf( 0) );
        pprbasespacing13.setLine( BigInteger.valueOf( 240) );
        pprbasespacing13.setLineRule(org.docx4j.wml.STLineSpacingRule.AUTO);
        // Create object for p
        P p74 = wmlObjectFactory.createP();
        body.getContent().add( p74);
        // Create object for pPr
        PPr ppr72 = wmlObjectFactory.createPPr();
        p74.setPPr(ppr72);
        // Create object for rPr
        ParaRPr pararpr72 = wmlObjectFactory.createParaRPr();
        ppr72.setRPr(pararpr72);
        // Create object for rFonts
        RFonts rfonts132 = wmlObjectFactory.createRFonts();
        pararpr72.setRFonts(rfonts132);
        rfonts132.setAscii( "Times");
        rfonts132.setCs( "Times");
        rfonts132.setEastAsia( "Times");
        rfonts132.setHAnsi( "Times");
        // Create object for spacing
        PPrBase.Spacing pprbasespacing14 = wmlObjectFactory.createPPrBaseSpacing();
        ppr72.setSpacing(pprbasespacing14);
        pprbasespacing14.setAfter( BigInteger.valueOf( 0) );
        pprbasespacing14.setLine( BigInteger.valueOf( 240) );
        pprbasespacing14.setLineRule(org.docx4j.wml.STLineSpacingRule.AUTO);
        // Create object for tbl (wrapped in JAXBElement)
        Tbl tbl4 = wmlObjectFactory.createTbl();
        JAXBElement<org.docx4j.wml.Tbl> tblWrapped4 = wmlObjectFactory.createBodyTbl(tbl4);
        body.getContent().add( tblWrapped4);
        // Create object for tr
        Tr tr12 = wmlObjectFactory.createTr();
        tbl4.getContent().add( tr12);
        // Create object for tc (wrapped in JAXBElement)
        Tc tc54 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped54 = wmlObjectFactory.createTrTc(tc54);
        tr12.getContent().add( tcWrapped54);
        // Create object for p
        P p75 = wmlObjectFactory.createP();
        tc54.getContent().add( p75);
        // Create object for pPr
        PPr ppr73 = wmlObjectFactory.createPPr();
        p75.setPPr(ppr73);
        // Create object for rPr
        ParaRPr pararpr73 = wmlObjectFactory.createParaRPr();
        ppr73.setRPr(pararpr73);
        // Create object for rFonts
        RFonts rfonts133 = wmlObjectFactory.createRFonts();
        pararpr73.setRFonts(rfonts133);
        rfonts133.setAscii( "Times");
        rfonts133.setCs( "Times");
        rfonts133.setEastAsia( "Times");
        rfonts133.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue42 = wmlObjectFactory.createBooleanDefaultTrue();
        pararpr73.setB(booleandefaulttrue42);
        // Create object for r
        R r61 = wmlObjectFactory.createR();
        p75.getContent().add( r61);
        // Create object for rPr
        RPr rpr61 = wmlObjectFactory.createRPr();
        r61.setRPr(rpr61);
        // Create object for rFonts
        RFonts rfonts134 = wmlObjectFactory.createRFonts();
        rpr61.setRFonts(rfonts134);
        rfonts134.setAscii( "Times");
        rfonts134.setCs( "Times");
        rfonts134.setEastAsia( "Times");
        rfonts134.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue43 = wmlObjectFactory.createBooleanDefaultTrue();
        rpr61.setB(booleandefaulttrue43);
        // Create object for t (wrapped in JAXBElement)
        Text text61 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped61 = wmlObjectFactory.createRT(text61);
        r61.getContent().add( textWrapped61);
        text61.setValue( "Technical");
        // Create object for tcPr
        TcPr tcpr54 = wmlObjectFactory.createTcPr();
        tc54.setTcPr(tcpr54);
        // Create object for tcW
        TblWidth tblwidth57 = wmlObjectFactory.createTblWidth();
        tcpr54.setTcW(tblwidth57);
        tblwidth57.setW( BigInteger.valueOf( 1811) );
        tblwidth57.setType( "dxa");
        // Create object for tc (wrapped in JAXBElement)
        Tc tc55 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped55 = wmlObjectFactory.createTrTc(tc55);
        tr12.getContent().add( tcWrapped55);
        // Create object for p
        P p76 = wmlObjectFactory.createP();
        tc55.getContent().add( p76);
        // Create object for pPr
        PPr ppr74 = wmlObjectFactory.createPPr();
        p76.setPPr(ppr74);
        // Create object for rPr
        ParaRPr pararpr74 = wmlObjectFactory.createParaRPr();
        ppr74.setRPr(pararpr74);
        // Create object for rFonts
        RFonts rfonts135 = wmlObjectFactory.createRFonts();
        pararpr74.setRFonts(rfonts135);
        rfonts135.setAscii( "Times");
        rfonts135.setCs( "Times");
        rfonts135.setEastAsia( "Times");
        rfonts135.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue44 = wmlObjectFactory.createBooleanDefaultTrue();
        pararpr74.setB(booleandefaulttrue44);
        // Create object for r
        R r62 = wmlObjectFactory.createR();
        p76.getContent().add( r62);
        // Create object for rPr
        RPr rpr62 = wmlObjectFactory.createRPr();
        r62.setRPr(rpr62);
        // Create object for rFonts
        RFonts rfonts136 = wmlObjectFactory.createRFonts();
        rpr62.setRFonts(rfonts136);
        rfonts136.setAscii( "Times");
        rfonts136.setCs( "Times");
        rfonts136.setEastAsia( "Times");
        rfonts136.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue45 = wmlObjectFactory.createBooleanDefaultTrue();
        rpr62.setB(booleandefaulttrue45);
        // Create object for t (wrapped in JAXBElement)
        Text text62 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped62 = wmlObjectFactory.createRT(text62);
        r62.getContent().add( textWrapped62);
        text62.setValue( "Excellent (4)");
        // Create object for tcPr
        TcPr tcpr55 = wmlObjectFactory.createTcPr();
        tc55.setTcPr(tcpr55);
        // Create object for tcW
        TblWidth tblwidth58 = wmlObjectFactory.createTblWidth();
        tcpr55.setTcW(tblwidth58);
        tblwidth58.setW( BigInteger.valueOf( 2098) );
        tblwidth58.setType( "dxa");
        // Create object for tc (wrapped in JAXBElement)
        Tc tc56 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped56 = wmlObjectFactory.createTrTc(tc56);
        tr12.getContent().add( tcWrapped56);
        // Create object for p
        P p77 = wmlObjectFactory.createP();
        tc56.getContent().add( p77);
        // Create object for pPr
        PPr ppr75 = wmlObjectFactory.createPPr();
        p77.setPPr(ppr75);
        // Create object for rPr
        ParaRPr pararpr75 = wmlObjectFactory.createParaRPr();
        ppr75.setRPr(pararpr75);
        // Create object for rFonts
        RFonts rfonts137 = wmlObjectFactory.createRFonts();
        pararpr75.setRFonts(rfonts137);
        rfonts137.setAscii( "Times");
        rfonts137.setCs( "Times");
        rfonts137.setEastAsia( "Times");
        rfonts137.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue46 = wmlObjectFactory.createBooleanDefaultTrue();
        pararpr75.setB(booleandefaulttrue46);
        // Create object for r
        R r63 = wmlObjectFactory.createR();
        p77.getContent().add( r63);
        // Create object for rPr
        RPr rpr63 = wmlObjectFactory.createRPr();
        r63.setRPr(rpr63);
        // Create object for rFonts
        RFonts rfonts138 = wmlObjectFactory.createRFonts();
        rpr63.setRFonts(rfonts138);
        rfonts138.setAscii( "Times");
        rfonts138.setCs( "Times");
        rfonts138.setEastAsia( "Times");
        rfonts138.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue47 = wmlObjectFactory.createBooleanDefaultTrue();
        rpr63.setB(booleandefaulttrue47);
        // Create object for t (wrapped in JAXBElement)
        Text text63 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped63 = wmlObjectFactory.createRT(text63);
        r63.getContent().add( textWrapped63);
        text63.setValue( "Good (3)");
        // Create object for tcPr
        TcPr tcpr56 = wmlObjectFactory.createTcPr();
        tc56.setTcPr(tcpr56);
        // Create object for tcW
        TblWidth tblwidth59 = wmlObjectFactory.createTblWidth();
        tcpr56.setTcW(tblwidth59);
        tblwidth59.setW( BigInteger.valueOf( 2098) );
        tblwidth59.setType( "dxa");
        // Create object for tc (wrapped in JAXBElement)
        Tc tc57 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped57 = wmlObjectFactory.createTrTc(tc57);
        tr12.getContent().add( tcWrapped57);
        // Create object for p
        P p78 = wmlObjectFactory.createP();
        tc57.getContent().add( p78);
        // Create object for pPr
        PPr ppr76 = wmlObjectFactory.createPPr();
        p78.setPPr(ppr76);
        // Create object for rPr
        ParaRPr pararpr76 = wmlObjectFactory.createParaRPr();
        ppr76.setRPr(pararpr76);
        // Create object for rFonts
        RFonts rfonts139 = wmlObjectFactory.createRFonts();
        pararpr76.setRFonts(rfonts139);
        rfonts139.setAscii( "Times");
        rfonts139.setCs( "Times");
        rfonts139.setEastAsia( "Times");
        rfonts139.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue48 = wmlObjectFactory.createBooleanDefaultTrue();
        pararpr76.setB(booleandefaulttrue48);
        // Create object for r
        R r64 = wmlObjectFactory.createR();
        p78.getContent().add( r64);
        // Create object for rPr
        RPr rpr64 = wmlObjectFactory.createRPr();
        r64.setRPr(rpr64);
        // Create object for rFonts
        RFonts rfonts140 = wmlObjectFactory.createRFonts();
        rpr64.setRFonts(rfonts140);
        rfonts140.setAscii( "Times");
        rfonts140.setCs( "Times");
        rfonts140.setEastAsia( "Times");
        rfonts140.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue49 = wmlObjectFactory.createBooleanDefaultTrue();
        rpr64.setB(booleandefaulttrue49);
        // Create object for t (wrapped in JAXBElement)
        Text text64 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped64 = wmlObjectFactory.createRT(text64);
        r64.getContent().add( textWrapped64);
        text64.setValue( "Fair (2)");
        // Create object for tcPr
        TcPr tcpr57 = wmlObjectFactory.createTcPr();
        tc57.setTcPr(tcpr57);
        // Create object for tcW
        TblWidth tblwidth60 = wmlObjectFactory.createTblWidth();
        tcpr57.setTcW(tblwidth60);
        tblwidth60.setW( BigInteger.valueOf( 2098) );
        tblwidth60.setType( "dxa");
        // Create object for tc (wrapped in JAXBElement)
        Tc tc58 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped58 = wmlObjectFactory.createTrTc(tc58);
        tr12.getContent().add( tcWrapped58);
        // Create object for p
        P p79 = wmlObjectFactory.createP();
        tc58.getContent().add( p79);
        // Create object for pPr
        PPr ppr77 = wmlObjectFactory.createPPr();
        p79.setPPr(ppr77);
        // Create object for rPr
        ParaRPr pararpr77 = wmlObjectFactory.createParaRPr();
        ppr77.setRPr(pararpr77);
        // Create object for rFonts
        RFonts rfonts141 = wmlObjectFactory.createRFonts();
        pararpr77.setRFonts(rfonts141);
        rfonts141.setAscii( "Times");
        rfonts141.setCs( "Times");
        rfonts141.setEastAsia( "Times");
        rfonts141.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue50 = wmlObjectFactory.createBooleanDefaultTrue();
        pararpr77.setB(booleandefaulttrue50);
        // Create object for r
        R r65 = wmlObjectFactory.createR();
        p79.getContent().add( r65);
        // Create object for rPr
        RPr rpr65 = wmlObjectFactory.createRPr();
        r65.setRPr(rpr65);
        // Create object for rFonts
        RFonts rfonts142 = wmlObjectFactory.createRFonts();
        rpr65.setRFonts(rfonts142);
        rfonts142.setAscii( "Times");
        rfonts142.setCs( "Times");
        rfonts142.setEastAsia( "Times");
        rfonts142.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue51 = wmlObjectFactory.createBooleanDefaultTrue();
        rpr65.setB(booleandefaulttrue51);
        // Create object for t (wrapped in JAXBElement)
        Text text65 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped65 = wmlObjectFactory.createRT(text65);
        r65.getContent().add( textWrapped65);
        text65.setValue( "Poor (1)");
        // Create object for tcPr
        TcPr tcpr58 = wmlObjectFactory.createTcPr();
        tc58.setTcPr(tcpr58);
        // Create object for tcW
        TblWidth tblwidth61 = wmlObjectFactory.createTblWidth();
        tcpr58.setTcW(tblwidth61);
        tblwidth61.setW( BigInteger.valueOf( 2098) );
        tblwidth61.setType( "dxa");

        //------------------------------------------------------------------------------------------
        // todo knowledge - [tableRow]

        // Create object for trPr
        TrPr trpr12 = wmlObjectFactory.createTrPr();
        tr12.setTrPr(trpr12);
        // Create object for trHeight (wrapped in JAXBElement)
        CTHeight height5 = wmlObjectFactory.createCTHeight();
        JAXBElement<org.docx4j.wml.CTHeight> heightWrapped5 = wmlObjectFactory.createCTTrPrBaseTrHeight(height5);
        trpr12.getCnfStyleOrDivIdOrGridBefore().add( heightWrapped5);
        height5.setVal( BigInteger.valueOf( 280) );
        // Create object for jc (wrapped in JAXBElement)
        Jc jc19 = wmlObjectFactory.createJc();
        JAXBElement<org.docx4j.wml.Jc> jcWrapped12 = wmlObjectFactory.createCTTrPrBaseJc(jc19);
        trpr12.getCnfStyleOrDivIdOrGridBefore().add( jcWrapped12);
        jc19.setVal(org.docx4j.wml.JcEnumeration.CENTER);
        // Create object for tr
        Tr tr13 = wmlObjectFactory.createTr();
        tbl4.getContent().add( tr13);
        // Create object for tc (wrapped in JAXBElement)
        Tc tc59 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped59 = wmlObjectFactory.createTrTc(tc59);
        tr13.getContent().add( tcWrapped59);
        // Create object for p
        P p80 = wmlObjectFactory.createP();
        tc59.getContent().add( p80);
        // Create object for pPr
        PPr ppr78 = wmlObjectFactory.createPPr();
        p80.setPPr(ppr78);
        // Create object for rPr
        ParaRPr pararpr78 = wmlObjectFactory.createParaRPr();
        ppr78.setRPr(pararpr78);
        // Create object for rFonts
        RFonts rfonts143 = wmlObjectFactory.createRFonts();
        pararpr78.setRFonts(rfonts143);
        rfonts143.setAscii( "Times New Roman");
        rfonts143.setCs( "Times New Roman");
        rfonts143.setEastAsia( "Times New Roman");
        rfonts143.setHAnsi( "Times New Roman");
        // Create object for r
        R r66 = wmlObjectFactory.createR();
        p80.getContent().add( r66);
        // Create object for rPr
        RPr rpr66 = wmlObjectFactory.createRPr();
        r66.setRPr(rpr66);
        // Create object for rFonts
        RFonts rfonts144 = wmlObjectFactory.createRFonts();
        rpr66.setRFonts(rfonts144);
        rfonts144.setAscii( "Times New Roman");
        rfonts144.setCs( "Times New Roman");
        rfonts144.setEastAsia( "Times New Roman");
        rfonts144.setHAnsi( "Times New Roman");
        // Create object for t (wrapped in JAXBElement)
        Text text66 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped66 = wmlObjectFactory.createRT(text66);
        r66.getContent().add( textWrapped66);
        text66.setValue( "Knowledge");
        // Create object for tcPr
        TcPr tcpr59 = wmlObjectFactory.createTcPr();
        tc59.setTcPr(tcpr59);
        // Create object for tcW
        TblWidth tblwidth62 = wmlObjectFactory.createTblWidth();
        tcpr59.setTcW(tblwidth62);
        tblwidth62.setW( BigInteger.valueOf( 1811) );
        tblwidth62.setType( "dxa");

//        //------------------------------------------------------------------------------------------
//        // todo knowledge - excellent
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc60 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped60 = wmlObjectFactory.createTrTc(tc60);
//        tr13.getContent().add( tcWrapped60);
//        // Create object for p
//        P p81 = wmlObjectFactory.createP();
//        tc60.getContent().add( p81);
//        // Create object for pPr
//        PPr ppr79 = wmlObjectFactory.createPPr();
//        p81.setPPr(ppr79);
//        // Create object for rPr
//        ParaRPr pararpr79 = wmlObjectFactory.createParaRPr();
//        ppr79.setRPr(pararpr79);
//        // Create object for rFonts
//        RFonts rfonts145 = wmlObjectFactory.createRFonts();
//        pararpr79.setRFonts(rfonts145);
//        rfonts145.setAscii( "Times New Roman");
//        rfonts145.setCs( "Times New Roman");
//        rfonts145.setEastAsia( "Times New Roman");
//        rfonts145.setHAnsi( "Times New Roman");
//        // Create object for b
//        BooleanDefaultTrue booleandefaulttrue52 = wmlObjectFactory.createBooleanDefaultTrue();
//        pararpr79.setB(booleandefaulttrue52);
//        // Create object for r
//        R r67 = wmlObjectFactory.createR();
//        p81.getContent().add( r67);
//        // Create object for rPr
//        RPr rpr67 = wmlObjectFactory.createRPr();
//        r67.setRPr(rpr67);
//        // Create object for rFonts
//        RFonts rfonts146 = wmlObjectFactory.createRFonts();
//        rpr67.setRFonts(rfonts146);
//        rfonts146.setAscii( "Times New Roman");
//        rfonts146.setCs( "Times New Roman");
//        rfonts146.setEastAsia( "Times New Roman");
//        rfonts146.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text67 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped67 = wmlObjectFactory.createRT(text67);
//        r67.getContent().add( textWrapped67);
//        text67.setValue( "Demonstrate clear knowledge and understanding of the subject");
//        // Create object for tcPr
//        TcPr tcpr60 = wmlObjectFactory.createTcPr();
//        tc60.setTcPr(tcpr60);
//        // Create object for tcW
//        TblWidth tblwidth63 = wmlObjectFactory.createTblWidth();
//        tcpr60.setTcW(tblwidth63);
//        tblwidth63.setW( BigInteger.valueOf( 2098) );
//        tblwidth63.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo knowledge - good
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc61 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped61 = wmlObjectFactory.createTrTc(tc61);
//        tr13.getContent().add( tcWrapped61);
//        // Create object for p
//        P p82 = wmlObjectFactory.createP();
//        tc61.getContent().add( p82);
//        // Create object for pPr
//        PPr ppr80 = wmlObjectFactory.createPPr();
//        p82.setPPr(ppr80);
//        // Create object for rPr
//        ParaRPr pararpr80 = wmlObjectFactory.createParaRPr();
//        ppr80.setRPr(pararpr80);
//        // Create object for rFonts
//        RFonts rfonts147 = wmlObjectFactory.createRFonts();
//        pararpr80.setRFonts(rfonts147);
//        rfonts147.setAscii( "Times New Roman");
//        rfonts147.setCs( "Times New Roman");
//        rfonts147.setEastAsia( "Times New Roman");
//        rfonts147.setHAnsi( "Times New Roman");
//        // Create object for b
//        BooleanDefaultTrue booleandefaulttrue53 = wmlObjectFactory.createBooleanDefaultTrue();
//        pararpr80.setB(booleandefaulttrue53);
//        // Create object for r
//        R r68 = wmlObjectFactory.createR();
//        p82.getContent().add( r68);
//        // Create object for rPr
//        RPr rpr68 = wmlObjectFactory.createRPr();
//        r68.setRPr(rpr68);
//        // Create object for rFonts
//        RFonts rfonts148 = wmlObjectFactory.createRFonts();
//        rpr68.setRFonts(rfonts148);
//        rfonts148.setAscii( "Times New Roman");
//        rfonts148.setCs( "Times New Roman");
//        rfonts148.setEastAsia( "Times New Roman");
//        rfonts148.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text68 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped68 = wmlObjectFactory.createRT(text68);
//        r68.getContent().add( textWrapped68);
//        text68.setValue( "Show clear knowledge and understanding of most subject area");
//        // Create object for tcPr
//        TcPr tcpr61 = wmlObjectFactory.createTcPr();
//        tc61.setTcPr(tcpr61);
//        // Create object for tcW
//        TblWidth tblwidth64 = wmlObjectFactory.createTblWidth();
//        tcpr61.setTcW(tblwidth64);
//        tblwidth64.setW( BigInteger.valueOf( 2098) );
//        tblwidth64.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo knowledge - fair
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc62 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped62 = wmlObjectFactory.createTrTc(tc62);
//        tr13.getContent().add( tcWrapped62);
//        // Create object for p
//        P p83 = wmlObjectFactory.createP();
//        tc62.getContent().add( p83);
//        // Create object for pPr
//        PPr ppr81 = wmlObjectFactory.createPPr();
//        p83.setPPr(ppr81);
//        // Create object for rPr
//        ParaRPr pararpr81 = wmlObjectFactory.createParaRPr();
//        ppr81.setRPr(pararpr81);
//        // Create object for rFonts
//        RFonts rfonts149 = wmlObjectFactory.createRFonts();
//        pararpr81.setRFonts(rfonts149);
//        rfonts149.setAscii( "Times New Roman");
//        rfonts149.setCs( "Times New Roman");
//        rfonts149.setEastAsia( "Times New Roman");
//        rfonts149.setHAnsi( "Times New Roman");
//        // Create object for b
//        BooleanDefaultTrue booleandefaulttrue54 = wmlObjectFactory.createBooleanDefaultTrue();
//        pararpr81.setB(booleandefaulttrue54);
//        // Create object for r
//        R r69 = wmlObjectFactory.createR();
//        p83.getContent().add( r69);
//        // Create object for rPr
//        RPr rpr69 = wmlObjectFactory.createRPr();
//        r69.setRPr(rpr69);
//        // Create object for rFonts
//        RFonts rfonts150 = wmlObjectFactory.createRFonts();
//        rpr69.setRFonts(rfonts150);
//        rfonts150.setAscii( "Times New Roman");
//        rfonts150.setCs( "Times New Roman");
//        rfonts150.setEastAsia( "Times New Roman");
//        rfonts150.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text69 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped69 = wmlObjectFactory.createRT(text69);
//        r69.getContent().add( textWrapped69);
//        text69.setValue( "Show some knowledge and understanding of the subject area");
//        // Create object for tcPr
//        TcPr tcpr62 = wmlObjectFactory.createTcPr();
//        tc62.setTcPr(tcpr62);
//        // Create object for tcW
//        TblWidth tblwidth65 = wmlObjectFactory.createTblWidth();
//        tcpr62.setTcW(tblwidth65);
//        tblwidth65.setW( BigInteger.valueOf( 2098) );
//        tblwidth65.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo knowledge - poor
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc63 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped63 = wmlObjectFactory.createTrTc(tc63);
//        tr13.getContent().add( tcWrapped63);
//        // Create object for p
//        P p84 = wmlObjectFactory.createP();
//        tc63.getContent().add( p84);
//        // Create object for pPr
//        PPr ppr82 = wmlObjectFactory.createPPr();
//        p84.setPPr(ppr82);
//        // Create object for rPr
//        ParaRPr pararpr82 = wmlObjectFactory.createParaRPr();
//        ppr82.setRPr(pararpr82);
//        // Create object for rFonts
//        RFonts rfonts151 = wmlObjectFactory.createRFonts();
//        pararpr82.setRFonts(rfonts151);
//        rfonts151.setAscii( "Times New Roman");
//        rfonts151.setCs( "Times New Roman");
//        rfonts151.setEastAsia( "Times New Roman");
//        rfonts151.setHAnsi( "Times New Roman");
//        // Create object for b
//        BooleanDefaultTrue booleandefaulttrue55 = wmlObjectFactory.createBooleanDefaultTrue();
//        pararpr82.setB(booleandefaulttrue55);
//        // Create object for r
//        R r70 = wmlObjectFactory.createR();
//        p84.getContent().add( r70);
//        // Create object for rPr
//        RPr rpr70 = wmlObjectFactory.createRPr();
//        r70.setRPr(rpr70);
//        // Create object for rFonts
//        RFonts rfonts152 = wmlObjectFactory.createRFonts();
//        rpr70.setRFonts(rfonts152);
//        rfonts152.setAscii( "Times New Roman");
//        rfonts152.setCs( "Times New Roman");
//        rfonts152.setEastAsia( "Times New Roman");
//        rfonts152.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text70 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped70 = wmlObjectFactory.createRT(text70);
//        r70.getContent().add( textWrapped70);
//        text70.setValue( "Show no knowledge and understanding of the subject area");
//        // Create object for tcPr
//        TcPr tcpr63 = wmlObjectFactory.createTcPr();
//        tc63.setTcPr(tcpr63);
//        // Create object for tcW
//        TblWidth tblwidth66 = wmlObjectFactory.createTblWidth();
//        tcpr63.setTcW(tblwidth66);
//        tblwidth66.setW( BigInteger.valueOf( 2098) );
//        tblwidth66.setType( "dxa");

        makeUpTechnicalTableKnowledgeRow(assessment, wmlObjectFactory, tr13);


        //------------------------------------------------------------------------------------------
        // todo research - [tableRow]

        // Create object for trPr
        TrPr trpr13 = wmlObjectFactory.createTrPr();
        tr13.setTrPr(trpr13);
        // Create object for trHeight (wrapped in JAXBElement)
        CTHeight height6 = wmlObjectFactory.createCTHeight();
        JAXBElement<org.docx4j.wml.CTHeight> heightWrapped6 = wmlObjectFactory.createCTTrPrBaseTrHeight(height6);
        trpr13.getCnfStyleOrDivIdOrGridBefore().add( heightWrapped6);
        height6.setVal( BigInteger.valueOf( 280) );
        // Create object for jc (wrapped in JAXBElement)
        Jc jc20 = wmlObjectFactory.createJc();
        JAXBElement<org.docx4j.wml.Jc> jcWrapped13 = wmlObjectFactory.createCTTrPrBaseJc(jc20);
        trpr13.getCnfStyleOrDivIdOrGridBefore().add( jcWrapped13);
        jc20.setVal(org.docx4j.wml.JcEnumeration.CENTER);
        // Create object for tr
        Tr tr14 = wmlObjectFactory.createTr();
        tbl4.getContent().add( tr14);
        // Create object for tc (wrapped in JAXBElement)
        Tc tc64 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped64 = wmlObjectFactory.createTrTc(tc64);
        tr14.getContent().add( tcWrapped64);
        // Create object for p
        P p85 = wmlObjectFactory.createP();
        tc64.getContent().add( p85);
        // Create object for pPr
        PPr ppr83 = wmlObjectFactory.createPPr();
        p85.setPPr(ppr83);
        // Create object for rPr
        ParaRPr pararpr83 = wmlObjectFactory.createParaRPr();
        ppr83.setRPr(pararpr83);
        // Create object for rFonts
        RFonts rfonts153 = wmlObjectFactory.createRFonts();
        pararpr83.setRFonts(rfonts153);
        rfonts153.setAscii( "Times New Roman");
        rfonts153.setCs( "Times New Roman");
        rfonts153.setEastAsia( "Times New Roman");
        rfonts153.setHAnsi( "Times New Roman");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue56 = wmlObjectFactory.createBooleanDefaultTrue();
        pararpr83.setB(booleandefaulttrue56);
        // Create object for r
        R r71 = wmlObjectFactory.createR();
        p85.getContent().add( r71);
        // Create object for rPr
        RPr rpr71 = wmlObjectFactory.createRPr();
        r71.setRPr(rpr71);
        // Create object for rFonts
        RFonts rfonts154 = wmlObjectFactory.createRFonts();
        rpr71.setRFonts(rfonts154);
        rfonts154.setAscii( "Times New Roman");
        rfonts154.setCs( "Times New Roman");
        rfonts154.setEastAsia( "Times New Roman");
        rfonts154.setHAnsi( "Times New Roman");
        // Create object for t (wrapped in JAXBElement)
        Text text71 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped71 = wmlObjectFactory.createRT(text71);
        r71.getContent().add( textWrapped71);
        text71.setValue( "Research");
        // Create object for tcPr
        TcPr tcpr64 = wmlObjectFactory.createTcPr();
        tc64.setTcPr(tcpr64);
        // Create object for tcW
        TblWidth tblwidth67 = wmlObjectFactory.createTblWidth();
        tcpr64.setTcW(tblwidth67);
        tblwidth67.setW( BigInteger.valueOf( 1811) );
        tblwidth67.setType( "dxa");

//        //------------------------------------------------------------------------------------------
//        // todo research - excellent
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc65 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped65 = wmlObjectFactory.createTrTc(tc65);
//        tr14.getContent().add( tcWrapped65);
//        // Create object for p
//        P p86 = wmlObjectFactory.createP();
//        tc65.getContent().add( p86);
//        // Create object for pPr
//        PPr ppr84 = wmlObjectFactory.createPPr();
//        p86.setPPr(ppr84);
//        // Create object for rPr
//        ParaRPr pararpr84 = wmlObjectFactory.createParaRPr();
//        ppr84.setRPr(pararpr84);
//        // Create object for rFonts
//        RFonts rfonts155 = wmlObjectFactory.createRFonts();
//        pararpr84.setRFonts(rfonts155);
//        rfonts155.setAscii( "Times New Roman");
//        rfonts155.setCs( "Times New Roman");
//        rfonts155.setEastAsia( "Times New Roman");
//        rfonts155.setHAnsi( "Times New Roman");
//        // Create object for b
//        BooleanDefaultTrue booleandefaulttrue57 = wmlObjectFactory.createBooleanDefaultTrue();
//        pararpr84.setB(booleandefaulttrue57);
//        // Create object for r
//        R r72 = wmlObjectFactory.createR();
//        p86.getContent().add( r72);
//        // Create object for rPr
//        RPr rpr72 = wmlObjectFactory.createRPr();
//        r72.setRPr(rpr72);
//        // Create object for rFonts
//        RFonts rfonts156 = wmlObjectFactory.createRFonts();
//        rpr72.setRFonts(rfonts156);
//        rfonts156.setAscii( "Times New Roman");
//        rfonts156.setCs( "Times New Roman");
//        rfonts156.setEastAsia( "Times New Roman");
//        rfonts156.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text72 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped72 = wmlObjectFactory.createRT(text72);
//        r72.getContent().add( textWrapped72);
//        text72.setValue( "Evidence of thorough research and preparation");
//        // Create object for tcPr
//        TcPr tcpr65 = wmlObjectFactory.createTcPr();
//        tc65.setTcPr(tcpr65);
//        // Create object for tcW
//        TblWidth tblwidth68 = wmlObjectFactory.createTblWidth();
//        tcpr65.setTcW(tblwidth68);
//        tblwidth68.setW( BigInteger.valueOf( 2098) );
//        tblwidth68.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo research - good
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc66 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped66 = wmlObjectFactory.createTrTc(tc66);
//        tr14.getContent().add( tcWrapped66);
//        // Create object for p
//        P p87 = wmlObjectFactory.createP();
//        tc66.getContent().add( p87);
//        // Create object for pPr
//        PPr ppr85 = wmlObjectFactory.createPPr();
//        p87.setPPr(ppr85);
//        // Create object for rPr
//        ParaRPr pararpr85 = wmlObjectFactory.createParaRPr();
//        ppr85.setRPr(pararpr85);
//        // Create object for rFonts
//        RFonts rfonts157 = wmlObjectFactory.createRFonts();
//        pararpr85.setRFonts(rfonts157);
//        rfonts157.setAscii( "Times New Roman");
//        rfonts157.setCs( "Times New Roman");
//        rfonts157.setEastAsia( "Times New Roman");
//        rfonts157.setHAnsi( "Times New Roman");
//        // Create object for b
//        BooleanDefaultTrue booleandefaulttrue58 = wmlObjectFactory.createBooleanDefaultTrue();
//        pararpr85.setB(booleandefaulttrue58);
//        // Create object for r
//        R r73 = wmlObjectFactory.createR();
//        p87.getContent().add( r73);
//        // Create object for rPr
//        RPr rpr73 = wmlObjectFactory.createRPr();
//        r73.setRPr(rpr73);
//        // Create object for rFonts
//        RFonts rfonts158 = wmlObjectFactory.createRFonts();
//        rpr73.setRFonts(rfonts158);
//        rfonts158.setAscii( "Times New Roman");
//        rfonts158.setCs( "Times New Roman");
//        rfonts158.setEastAsia( "Times New Roman");
//        rfonts158.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text73 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped73 = wmlObjectFactory.createRT(text73);
//        r73.getContent().add( textWrapped73);
//        text73.setValue( "Evidence of sufficient research and preparation");
//        // Create object for tcPr
//        TcPr tcpr66 = wmlObjectFactory.createTcPr();
//        tc66.setTcPr(tcpr66);
//        // Create object for tcW
//        TblWidth tblwidth69 = wmlObjectFactory.createTblWidth();
//        tcpr66.setTcW(tblwidth69);
//        tblwidth69.setW( BigInteger.valueOf( 2098) );
//        tblwidth69.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo research - fair
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc67 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped67 = wmlObjectFactory.createTrTc(tc67);
//        tr14.getContent().add( tcWrapped67);
//        // Create object for p
//        P p88 = wmlObjectFactory.createP();
//        tc67.getContent().add( p88);
//        // Create object for pPr
//        PPr ppr86 = wmlObjectFactory.createPPr();
//        p88.setPPr(ppr86);
//        // Create object for rPr
//        ParaRPr pararpr86 = wmlObjectFactory.createParaRPr();
//        ppr86.setRPr(pararpr86);
//        // Create object for rFonts
//        RFonts rfonts159 = wmlObjectFactory.createRFonts();
//        pararpr86.setRFonts(rfonts159);
//        rfonts159.setAscii( "Times New Roman");
//        rfonts159.setCs( "Times New Roman");
//        rfonts159.setEastAsia( "Times New Roman");
//        rfonts159.setHAnsi( "Times New Roman");
//        // Create object for b
//        BooleanDefaultTrue booleandefaulttrue59 = wmlObjectFactory.createBooleanDefaultTrue();
//        pararpr86.setB(booleandefaulttrue59);
//        // Create object for r
//        R r74 = wmlObjectFactory.createR();
//        p88.getContent().add( r74);
//        // Create object for rPr
//        RPr rpr74 = wmlObjectFactory.createRPr();
//        r74.setRPr(rpr74);
//        // Create object for rFonts
//        RFonts rfonts160 = wmlObjectFactory.createRFonts();
//        rpr74.setRFonts(rfonts160);
//        rfonts160.setAscii( "Times New Roman");
//        rfonts160.setCs( "Times New Roman");
//        rfonts160.setEastAsia( "Times New Roman");
//        rfonts160.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text74 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped74 = wmlObjectFactory.createRT(text74);
//        r74.getContent().add( textWrapped74);
//        text74.setValue( "Evidence of some research and preparation");
//        // Create object for tcPr
//        TcPr tcpr67 = wmlObjectFactory.createTcPr();
//        tc67.setTcPr(tcpr67);
//        // Create object for tcW
//        TblWidth tblwidth70 = wmlObjectFactory.createTblWidth();
//        tcpr67.setTcW(tblwidth70);
//        tblwidth70.setW( BigInteger.valueOf( 2098) );
//        tblwidth70.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo research - poor
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc68 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped68 = wmlObjectFactory.createTrTc(tc68);
//        tr14.getContent().add( tcWrapped68);
//        // Create object for p
//        P p89 = wmlObjectFactory.createP();
//        tc68.getContent().add( p89);
//        // Create object for pPr
//        PPr ppr87 = wmlObjectFactory.createPPr();
//        p89.setPPr(ppr87);
//        // Create object for rPr
//        ParaRPr pararpr87 = wmlObjectFactory.createParaRPr();
//        ppr87.setRPr(pararpr87);
//        // Create object for rFonts
//        RFonts rfonts161 = wmlObjectFactory.createRFonts();
//        pararpr87.setRFonts(rfonts161);
//        rfonts161.setAscii( "Times New Roman");
//        rfonts161.setCs( "Times New Roman");
//        rfonts161.setEastAsia( "Times New Roman");
//        rfonts161.setHAnsi( "Times New Roman");
//        // Create object for b
//        BooleanDefaultTrue booleandefaulttrue60 = wmlObjectFactory.createBooleanDefaultTrue();
//        pararpr87.setB(booleandefaulttrue60);
//        // Create object for r
//        R r75 = wmlObjectFactory.createR();
//        p89.getContent().add( r75);
//        // Create object for rPr
//        RPr rpr75 = wmlObjectFactory.createRPr();
//        r75.setRPr(rpr75);
//        // Create object for rFonts
//        RFonts rfonts162 = wmlObjectFactory.createRFonts();
//        rpr75.setRFonts(rfonts162);
//        rfonts162.setAscii( "Times New Roman");
//        rfonts162.setCs( "Times New Roman");
//        rfonts162.setEastAsia( "Times New Roman");
//        rfonts162.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text75 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped75 = wmlObjectFactory.createRT(text75);
//        r75.getContent().add( textWrapped75);
//        text75.setValue( "Evidence of no research and preparation");
//        // Create object for tcPr
//        TcPr tcpr68 = wmlObjectFactory.createTcPr();
//        tc68.setTcPr(tcpr68);
//        // Create object for tcW
//        TblWidth tblwidth71 = wmlObjectFactory.createTblWidth();
//        tcpr68.setTcW(tblwidth71);
//        tblwidth71.setW( BigInteger.valueOf( 2098) );
//        tblwidth71.setType( "dxa");

        makeUpTechnicalTableResearchRow(assessment, wmlObjectFactory, tr14);

        //------------------------------------------------------------------------------------------
        // todo discussion_of_new_idea - [tableRow]

        // Create object for trPr
        TrPr trpr14 = wmlObjectFactory.createTrPr();
        tr14.setTrPr(trpr14);
        // Create object for trHeight (wrapped in JAXBElement)
        CTHeight height7 = wmlObjectFactory.createCTHeight();
        JAXBElement<org.docx4j.wml.CTHeight> heightWrapped7 = wmlObjectFactory.createCTTrPrBaseTrHeight(height7);
        trpr14.getCnfStyleOrDivIdOrGridBefore().add( heightWrapped7);
        height7.setVal( BigInteger.valueOf( 280) );
        // Create object for jc (wrapped in JAXBElement)
        Jc jc21 = wmlObjectFactory.createJc();
        JAXBElement<org.docx4j.wml.Jc> jcWrapped14 = wmlObjectFactory.createCTTrPrBaseJc(jc21);
        trpr14.getCnfStyleOrDivIdOrGridBefore().add( jcWrapped14);
        jc21.setVal(org.docx4j.wml.JcEnumeration.CENTER);
        // Create object for tr
        Tr tr15 = wmlObjectFactory.createTr();
        tbl4.getContent().add( tr15);
        // Create object for tc (wrapped in JAXBElement)
        Tc tc69 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped69 = wmlObjectFactory.createTrTc(tc69);
        tr15.getContent().add( tcWrapped69);
        // Create object for p
        P p90 = wmlObjectFactory.createP();
        tc69.getContent().add( p90);
        // Create object for pPr
        PPr ppr88 = wmlObjectFactory.createPPr();
        p90.setPPr(ppr88);
        // Create object for rPr
        ParaRPr pararpr88 = wmlObjectFactory.createParaRPr();
        ppr88.setRPr(pararpr88);
        // Create object for rFonts
        RFonts rfonts163 = wmlObjectFactory.createRFonts();
        pararpr88.setRFonts(rfonts163);
        rfonts163.setAscii( "Times New Roman");
        rfonts163.setCs( "Times New Roman");
        rfonts163.setEastAsia( "Times New Roman");
        rfonts163.setHAnsi( "Times New Roman");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue61 = wmlObjectFactory.createBooleanDefaultTrue();
        pararpr88.setB(booleandefaulttrue61);
        // Create object for r
        R r76 = wmlObjectFactory.createR();
        p90.getContent().add( r76);
        // Create object for rPr
        RPr rpr76 = wmlObjectFactory.createRPr();
        r76.setRPr(rpr76);
        // Create object for rFonts
        RFonts rfonts164 = wmlObjectFactory.createRFonts();
        rpr76.setRFonts(rfonts164);
        rfonts164.setAscii( "Times New Roman");
        rfonts164.setCs( "Times New Roman");
        rfonts164.setEastAsia( "Times New Roman");
        rfonts164.setHAnsi( "Times New Roman");
        // Create object for t (wrapped in JAXBElement)
        Text text76 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped76 = wmlObjectFactory.createRT(text76);
        r76.getContent().add( textWrapped76);
        text76.setValue( "Discussion of new ideas");
        // Create object for tcPr
        TcPr tcpr69 = wmlObjectFactory.createTcPr();
        tc69.setTcPr(tcpr69);
        // Create object for tcW
        TblWidth tblwidth72 = wmlObjectFactory.createTblWidth();
        tcpr69.setTcW(tblwidth72);
        tblwidth72.setW( BigInteger.valueOf( 1811) );
        tblwidth72.setType( "dxa");

//        //------------------------------------------------------------------------------------------
//        // todo discussion_of_new_idea - excellent
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc70 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped70 = wmlObjectFactory.createTrTc(tc70);
//        tr15.getContent().add( tcWrapped70);
//        // Create object for p
//        P p91 = wmlObjectFactory.createP();
//        tc70.getContent().add( p91);
//        // Create object for pPr
//        PPr ppr89 = wmlObjectFactory.createPPr();
//        p91.setPPr(ppr89);
//        // Create object for rPr
//        ParaRPr pararpr89 = wmlObjectFactory.createParaRPr();
//        ppr89.setRPr(pararpr89);
//        // Create object for rFonts
//        RFonts rfonts165 = wmlObjectFactory.createRFonts();
//        pararpr89.setRFonts(rfonts165);
//        rfonts165.setAscii( "Times New Roman");
//        rfonts165.setCs( "Times New Roman");
//        rfonts165.setEastAsia( "Times New Roman");
//        rfonts165.setHAnsi( "Times New Roman");
//        // Create object for b
//        BooleanDefaultTrue booleandefaulttrue62 = wmlObjectFactory.createBooleanDefaultTrue();
//        pararpr89.setB(booleandefaulttrue62);
//        // Create object for r
//        R r77 = wmlObjectFactory.createR();
//        p91.getContent().add( r77);
//        // Create object for rPr
//        RPr rpr77 = wmlObjectFactory.createRPr();
//        r77.setRPr(rpr77);
//        // Create object for rFonts
//        RFonts rfonts166 = wmlObjectFactory.createRFonts();
//        rpr77.setRFonts(rfonts166);
//        rfonts166.setAscii( "Times New Roman");
//        rfonts166.setCs( "Times New Roman");
//        rfonts166.setEastAsia( "Times New Roman");
//        rfonts166.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text77 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped77 = wmlObjectFactory.createRT(text77);
//        r77.getContent().add( textWrapped77);
//        text77.setValue( "Demonstrate  thorough knowledge while discussing new ideas");
//        // Create object for tcPr
//        TcPr tcpr70 = wmlObjectFactory.createTcPr();
//        tc70.setTcPr(tcpr70);
//        // Create object for tcW
//        TblWidth tblwidth73 = wmlObjectFactory.createTblWidth();
//        tcpr70.setTcW(tblwidth73);
//        tblwidth73.setW( BigInteger.valueOf( 2098) );
//        tblwidth73.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo discussion_of_new_idea - good
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc71 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped71 = wmlObjectFactory.createTrTc(tc71);
//        tr15.getContent().add( tcWrapped71);
//        // Create object for p
//        P p92 = wmlObjectFactory.createP();
//        tc71.getContent().add( p92);
//        // Create object for pPr
//        PPr ppr90 = wmlObjectFactory.createPPr();
//        p92.setPPr(ppr90);
//        // Create object for rPr
//        ParaRPr pararpr90 = wmlObjectFactory.createParaRPr();
//        ppr90.setRPr(pararpr90);
//        // Create object for rFonts
//        RFonts rfonts167 = wmlObjectFactory.createRFonts();
//        pararpr90.setRFonts(rfonts167);
//        rfonts167.setAscii( "Times New Roman");
//        rfonts167.setCs( "Times New Roman");
//        rfonts167.setEastAsia( "Times New Roman");
//        rfonts167.setHAnsi( "Times New Roman");
//        // Create object for b
//        BooleanDefaultTrue booleandefaulttrue63 = wmlObjectFactory.createBooleanDefaultTrue();
//        pararpr90.setB(booleandefaulttrue63);
//        // Create object for r
//        R r78 = wmlObjectFactory.createR();
//        p92.getContent().add( r78);
//        // Create object for rPr
//        RPr rpr78 = wmlObjectFactory.createRPr();
//        r78.setRPr(rpr78);
//        // Create object for rFonts
//        RFonts rfonts168 = wmlObjectFactory.createRFonts();
//        rpr78.setRFonts(rfonts168);
//        rfonts168.setAscii( "Times New Roman");
//        rfonts168.setCs( "Times New Roman");
//        rfonts168.setEastAsia( "Times New Roman");
//        rfonts168.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text78 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped78 = wmlObjectFactory.createRT(text78);
//        r78.getContent().add( textWrapped78);
//        text78.setValue( "Show  sufficient knowledge while discussing new ideas");
//        // Create object for tcPr
//        TcPr tcpr71 = wmlObjectFactory.createTcPr();
//        tc71.setTcPr(tcpr71);
//        // Create object for tcW
//        TblWidth tblwidth74 = wmlObjectFactory.createTblWidth();
//        tcpr71.setTcW(tblwidth74);
//        tblwidth74.setW( BigInteger.valueOf( 2098) );
//        tblwidth74.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo discussion_of_new_idea - fair
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc72 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped72 = wmlObjectFactory.createTrTc(tc72);
//        tr15.getContent().add( tcWrapped72);
//        // Create object for p
//        P p93 = wmlObjectFactory.createP();
//        tc72.getContent().add( p93);
//        // Create object for pPr
//        PPr ppr91 = wmlObjectFactory.createPPr();
//        p93.setPPr(ppr91);
//        // Create object for rPr
//        ParaRPr pararpr91 = wmlObjectFactory.createParaRPr();
//        ppr91.setRPr(pararpr91);
//        // Create object for rFonts
//        RFonts rfonts169 = wmlObjectFactory.createRFonts();
//        pararpr91.setRFonts(rfonts169);
//        rfonts169.setAscii( "Times New Roman");
//        rfonts169.setCs( "Times New Roman");
//        rfonts169.setEastAsia( "Times New Roman");
//        rfonts169.setHAnsi( "Times New Roman");
//        // Create object for b
//        BooleanDefaultTrue booleandefaulttrue64 = wmlObjectFactory.createBooleanDefaultTrue();
//        pararpr91.setB(booleandefaulttrue64);
//        // Create object for r
//        R r79 = wmlObjectFactory.createR();
//        p93.getContent().add( r79);
//        // Create object for rPr
//        RPr rpr79 = wmlObjectFactory.createRPr();
//        r79.setRPr(rpr79);
//        // Create object for rFonts
//        RFonts rfonts170 = wmlObjectFactory.createRFonts();
//        rpr79.setRFonts(rfonts170);
//        rfonts170.setAscii( "Times New Roman");
//        rfonts170.setCs( "Times New Roman");
//        rfonts170.setEastAsia( "Times New Roman");
//        rfonts170.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text79 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped79 = wmlObjectFactory.createRT(text79);
//        r79.getContent().add( textWrapped79);
//        text79.setValue( "Show  some knowledge while discussing new ideas");
//        // Create object for tcPr
//        TcPr tcpr72 = wmlObjectFactory.createTcPr();
//        tc72.setTcPr(tcpr72);
//        // Create object for tcW
//        TblWidth tblwidth75 = wmlObjectFactory.createTblWidth();
//        tcpr72.setTcW(tblwidth75);
//        tblwidth75.setW( BigInteger.valueOf( 2098) );
//        tblwidth75.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo discussion_of_new_idea - poor
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc73 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped73 = wmlObjectFactory.createTrTc(tc73);
//        tr15.getContent().add( tcWrapped73);
//        // Create object for p
//        P p94 = wmlObjectFactory.createP();
//        tc73.getContent().add( p94);
//        // Create object for pPr
//        PPr ppr92 = wmlObjectFactory.createPPr();
//        p94.setPPr(ppr92);
//        // Create object for rPr
//        ParaRPr pararpr92 = wmlObjectFactory.createParaRPr();
//        ppr92.setRPr(pararpr92);
//        // Create object for rFonts
//        RFonts rfonts171 = wmlObjectFactory.createRFonts();
//        pararpr92.setRFonts(rfonts171);
//        rfonts171.setAscii( "Times New Roman");
//        rfonts171.setCs( "Times New Roman");
//        rfonts171.setEastAsia( "Times New Roman");
//        rfonts171.setHAnsi( "Times New Roman");
//        // Create object for b
//        BooleanDefaultTrue booleandefaulttrue65 = wmlObjectFactory.createBooleanDefaultTrue();
//        pararpr92.setB(booleandefaulttrue65);
//        // Create object for r
//        R r80 = wmlObjectFactory.createR();
//        p94.getContent().add( r80);
//        // Create object for rPr
//        RPr rpr80 = wmlObjectFactory.createRPr();
//        r80.setRPr(rpr80);
//        // Create object for rFonts
//        RFonts rfonts172 = wmlObjectFactory.createRFonts();
//        rpr80.setRFonts(rfonts172);
//        rfonts172.setAscii( "Times New Roman");
//        rfonts172.setCs( "Times New Roman");
//        rfonts172.setEastAsia( "Times New Roman");
//        rfonts172.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text80 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped80 = wmlObjectFactory.createRT(text80);
//        r80.getContent().add( textWrapped80);
//        text80.setValue( "Show  no knowledge while discussing new ideas");
//        // Create object for tcPr
//        TcPr tcpr73 = wmlObjectFactory.createTcPr();
//        tc73.setTcPr(tcpr73);
//        // Create object for tcW
//        TblWidth tblwidth76 = wmlObjectFactory.createTblWidth();
//        tcpr73.setTcW(tblwidth76);
//        tblwidth76.setW( BigInteger.valueOf( 2098) );
//        tblwidth76.setType( "dxa");

        makeUpTechnicalTableDiscussionOfNewIdeaRow(assessment, wmlObjectFactory, tr15);

        //------------------------------------------------------------------------------------------
        // todo argument - [tableRow]

        // Create object for trPr
        TrPr trpr15 = wmlObjectFactory.createTrPr();
        tr15.setTrPr(trpr15);
        // Create object for trHeight (wrapped in JAXBElement)
        CTHeight height8 = wmlObjectFactory.createCTHeight();
        JAXBElement<org.docx4j.wml.CTHeight> heightWrapped8 = wmlObjectFactory.createCTTrPrBaseTrHeight(height8);
        trpr15.getCnfStyleOrDivIdOrGridBefore().add( heightWrapped8);
        height8.setVal( BigInteger.valueOf( 280) );
        // Create object for jc (wrapped in JAXBElement)
        Jc jc22 = wmlObjectFactory.createJc();
        JAXBElement<org.docx4j.wml.Jc> jcWrapped15 = wmlObjectFactory.createCTTrPrBaseJc(jc22);
        trpr15.getCnfStyleOrDivIdOrGridBefore().add( jcWrapped15);
        jc22.setVal(org.docx4j.wml.JcEnumeration.CENTER);
        // Create object for tr
        Tr tr16 = wmlObjectFactory.createTr();
        tbl4.getContent().add( tr16);
        // Create object for tc (wrapped in JAXBElement)
        Tc tc74 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped74 = wmlObjectFactory.createTrTc(tc74);
        tr16.getContent().add( tcWrapped74);
        // Create object for p
        P p95 = wmlObjectFactory.createP();
        tc74.getContent().add( p95);
        // Create object for pPr
        PPr ppr93 = wmlObjectFactory.createPPr();
        p95.setPPr(ppr93);
        // Create object for rPr
        ParaRPr pararpr93 = wmlObjectFactory.createParaRPr();
        ppr93.setRPr(pararpr93);
        // Create object for rFonts
        RFonts rfonts173 = wmlObjectFactory.createRFonts();
        pararpr93.setRFonts(rfonts173);
        rfonts173.setAscii( "Times");
        rfonts173.setCs( "Times");
        rfonts173.setEastAsia( "Times");
        rfonts173.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue66 = wmlObjectFactory.createBooleanDefaultTrue();
        pararpr93.setB(booleandefaulttrue66);
        // Create object for r
        R r81 = wmlObjectFactory.createR();
        p95.getContent().add( r81);
        // Create object for rPr
        RPr rpr81 = wmlObjectFactory.createRPr();
        r81.setRPr(rpr81);
        // Create object for rFonts
        RFonts rfonts174 = wmlObjectFactory.createRFonts();
        rpr81.setRFonts(rfonts174);
        rfonts174.setAscii( "Times");
        rfonts174.setCs( "Times");
        rfonts174.setEastAsia( "Times");
        rfonts174.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue67 = wmlObjectFactory.createBooleanDefaultTrue();
        rpr81.setB(booleandefaulttrue67);
        // Create object for t (wrapped in JAXBElement)
        Text text81 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped81 = wmlObjectFactory.createRT(text81);
        r81.getContent().add( textWrapped81);
        text81.setValue( "Argument");
        // Create object for tcPr
        TcPr tcpr74 = wmlObjectFactory.createTcPr();
        tc74.setTcPr(tcpr74);
        // Create object for tcW
        TblWidth tblwidth77 = wmlObjectFactory.createTblWidth();
        tcpr74.setTcW(tblwidth77);
        tblwidth77.setW( BigInteger.valueOf( 1811) );
        tblwidth77.setType( "dxa");

//        //------------------------------------------------------------------------------------------
//        // todo argument - excellent
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc75 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped75 = wmlObjectFactory.createTrTc(tc75);
//        tr16.getContent().add( tcWrapped75);
//        // Create object for p
//        P p96 = wmlObjectFactory.createP();
//        tc75.getContent().add( p96);
//        // Create object for pPr
//        PPr ppr94 = wmlObjectFactory.createPPr();
//        p96.setPPr(ppr94);
//        // Create object for rPr
//        ParaRPr pararpr94 = wmlObjectFactory.createParaRPr();
//        ppr94.setRPr(pararpr94);
//        // Create object for rFonts
//        RFonts rfonts175 = wmlObjectFactory.createRFonts();
//        pararpr94.setRFonts(rfonts175);
//        rfonts175.setAscii( "Times New Roman");
//        rfonts175.setCs( "Times New Roman");
//        rfonts175.setEastAsia( "Times New Roman");
//        rfonts175.setHAnsi( "Times New Roman");
//        // Create object for b
//        BooleanDefaultTrue booleandefaulttrue68 = wmlObjectFactory.createBooleanDefaultTrue();
//        pararpr94.setB(booleandefaulttrue68);
//        // Create object for r
//        R r82 = wmlObjectFactory.createR();
//        p96.getContent().add( r82);
//        // Create object for rPr
//        RPr rpr82 = wmlObjectFactory.createRPr();
//        r82.setRPr(rpr82);
//        // Create object for rFonts
//        RFonts rfonts176 = wmlObjectFactory.createRFonts();
//        rpr82.setRFonts(rfonts176);
//        rfonts176.setAscii( "Times New Roman");
//        rfonts176.setCs( "Times New Roman");
//        rfonts176.setEastAsia( "Times New Roman");
//        rfonts176.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text82 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped82 = wmlObjectFactory.createRT(text82);
//        r82.getContent().add( textWrapped82);
//        text82.setValue( "Opinion set out in a concise and persuasive manner");
//        // Create object for tcPr
//        TcPr tcpr75 = wmlObjectFactory.createTcPr();
//        tc75.setTcPr(tcpr75);
//        // Create object for tcW
//        TblWidth tblwidth78 = wmlObjectFactory.createTblWidth();
//        tcpr75.setTcW(tblwidth78);
//        tblwidth78.setW( BigInteger.valueOf( 2098) );
//        tblwidth78.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo argument - good
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc76 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped76 = wmlObjectFactory.createTrTc(tc76);
//        tr16.getContent().add( tcWrapped76);
//        // Create object for p
//        P p97 = wmlObjectFactory.createP();
//        tc76.getContent().add( p97);
//        // Create object for pPr
//        PPr ppr95 = wmlObjectFactory.createPPr();
//        p97.setPPr(ppr95);
//        // Create object for rPr
//        ParaRPr pararpr95 = wmlObjectFactory.createParaRPr();
//        ppr95.setRPr(pararpr95);
//        // Create object for rFonts
//        RFonts rfonts177 = wmlObjectFactory.createRFonts();
//        pararpr95.setRFonts(rfonts177);
//        rfonts177.setAscii( "Times New Roman");
//        rfonts177.setCs( "Times New Roman");
//        rfonts177.setEastAsia( "Times New Roman");
//        rfonts177.setHAnsi( "Times New Roman");
//        // Create object for b
//        BooleanDefaultTrue booleandefaulttrue69 = wmlObjectFactory.createBooleanDefaultTrue();
//        pararpr95.setB(booleandefaulttrue69);
//        // Create object for r
//        R r83 = wmlObjectFactory.createR();
//        p97.getContent().add( r83);
//        // Create object for rPr
//        RPr rpr83 = wmlObjectFactory.createRPr();
//        r83.setRPr(rpr83);
//        // Create object for rFonts
//        RFonts rfonts178 = wmlObjectFactory.createRFonts();
//        rpr83.setRFonts(rfonts178);
//        rfonts178.setAscii( "Times New Roman");
//        rfonts178.setCs( "Times New Roman");
//        rfonts178.setEastAsia( "Times New Roman");
//        rfonts178.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text83 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped83 = wmlObjectFactory.createRT(text83);
//        r83.getContent().add( textWrapped83);
//        text83.setValue( "Opinion is not concise and persuasive manner");
//        // Create object for tcPr
//        TcPr tcpr76 = wmlObjectFactory.createTcPr();
//        tc76.setTcPr(tcpr76);
//        // Create object for tcW
//        TblWidth tblwidth79 = wmlObjectFactory.createTblWidth();
//        tcpr76.setTcW(tblwidth79);
//        tblwidth79.setW( BigInteger.valueOf( 2098) );
//        tblwidth79.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo argument - fair
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc77 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped77 = wmlObjectFactory.createTrTc(tc77);
//        tr16.getContent().add( tcWrapped77);
//        // Create object for p
//        P p98 = wmlObjectFactory.createP();
//        tc77.getContent().add( p98);
//        // Create object for pPr
//        PPr ppr96 = wmlObjectFactory.createPPr();
//        p98.setPPr(ppr96);
//        // Create object for rPr
//        ParaRPr pararpr96 = wmlObjectFactory.createParaRPr();
//        ppr96.setRPr(pararpr96);
//        // Create object for rFonts
//        RFonts rfonts179 = wmlObjectFactory.createRFonts();
//        pararpr96.setRFonts(rfonts179);
//        rfonts179.setAscii( "Times New Roman");
//        rfonts179.setCs( "Times New Roman");
//        rfonts179.setEastAsia( "Times New Roman");
//        rfonts179.setHAnsi( "Times New Roman");
//        // Create object for b
//        BooleanDefaultTrue booleandefaulttrue70 = wmlObjectFactory.createBooleanDefaultTrue();
//        pararpr96.setB(booleandefaulttrue70);
//        // Create object for r
//        R r84 = wmlObjectFactory.createR();
//        p98.getContent().add( r84);
//        // Create object for rPr
//        RPr rpr84 = wmlObjectFactory.createRPr();
//        r84.setRPr(rpr84);
//        // Create object for rFonts
//        RFonts rfonts180 = wmlObjectFactory.createRFonts();
//        rpr84.setRFonts(rfonts180);
//        rfonts180.setAscii( "Times New Roman");
//        rfonts180.setCs( "Times New Roman");
//        rfonts180.setEastAsia( "Times New Roman");
//        rfonts180.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text84 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped84 = wmlObjectFactory.createRT(text84);
//        r84.getContent().add( textWrapped84);
//        text84.setValue( "Opinion is clearly demonstrated but not persuasive ");
//        text84.setSpace( "preserve");
//        // Create object for tcPr
//        TcPr tcpr77 = wmlObjectFactory.createTcPr();
//        tc77.setTcPr(tcpr77);
//        // Create object for tcW
//        TblWidth tblwidth80 = wmlObjectFactory.createTblWidth();
//        tcpr77.setTcW(tblwidth80);
//        tblwidth80.setW( BigInteger.valueOf( 2098) );
//        tblwidth80.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo argument - poor
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc78 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped78 = wmlObjectFactory.createTrTc(tc78);
//        tr16.getContent().add( tcWrapped78);
//        // Create object for p
//        P p99 = wmlObjectFactory.createP();
//        tc78.getContent().add( p99);
//        // Create object for pPr
//        PPr ppr97 = wmlObjectFactory.createPPr();
//        p99.setPPr(ppr97);
//        // Create object for rPr
//        ParaRPr pararpr97 = wmlObjectFactory.createParaRPr();
//        ppr97.setRPr(pararpr97);
//        // Create object for rFonts
//        RFonts rfonts181 = wmlObjectFactory.createRFonts();
//        pararpr97.setRFonts(rfonts181);
//        rfonts181.setAscii( "Times New Roman");
//        rfonts181.setCs( "Times New Roman");
//        rfonts181.setEastAsia( "Times New Roman");
//        rfonts181.setHAnsi( "Times New Roman");
//        // Create object for b
//        BooleanDefaultTrue booleandefaulttrue71 = wmlObjectFactory.createBooleanDefaultTrue();
//        pararpr97.setB(booleandefaulttrue71);
//        // Create object for r
//        R r85 = wmlObjectFactory.createR();
//        p99.getContent().add( r85);
//        // Create object for rPr
//        RPr rpr85 = wmlObjectFactory.createRPr();
//        r85.setRPr(rpr85);
//        // Create object for rFonts
//        RFonts rfonts182 = wmlObjectFactory.createRFonts();
//        rpr85.setRFonts(rfonts182);
//        rfonts182.setAscii( "Times New Roman");
//        rfonts182.setCs( "Times New Roman");
//        rfonts182.setEastAsia( "Times New Roman");
//        rfonts182.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text85 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped85 = wmlObjectFactory.createRT(text85);
//        r85.getContent().add( textWrapped85);
//        text85.setValue( "Opinion is not demonstrated or highlighted");
//        // Create object for tcPr
//        TcPr tcpr78 = wmlObjectFactory.createTcPr();
//        tc78.setTcPr(tcpr78);
//        // Create object for tcW
//        TblWidth tblwidth81 = wmlObjectFactory.createTblWidth();
//        tcpr78.setTcW(tblwidth81);
//        tblwidth81.setW( BigInteger.valueOf( 2098) );
//        tblwidth81.setType( "dxa");

        makeUpTechnicalTableArgumentRow(assessment, wmlObjectFactory, tr16);

        //------------------------------------------------------------------------------------------
        // todo questions - [tableRow]

        // Create object for trPr
        TrPr trpr16 = wmlObjectFactory.createTrPr();
        tr16.setTrPr(trpr16);
        // Create object for trHeight (wrapped in JAXBElement)
        CTHeight height9 = wmlObjectFactory.createCTHeight();
        JAXBElement<org.docx4j.wml.CTHeight> heightWrapped9 = wmlObjectFactory.createCTTrPrBaseTrHeight(height9);
        trpr16.getCnfStyleOrDivIdOrGridBefore().add( heightWrapped9);
        height9.setVal( BigInteger.valueOf( 280) );
        // Create object for jc (wrapped in JAXBElement)
        Jc jc23 = wmlObjectFactory.createJc();
        JAXBElement<org.docx4j.wml.Jc> jcWrapped16 = wmlObjectFactory.createCTTrPrBaseJc(jc23);
        trpr16.getCnfStyleOrDivIdOrGridBefore().add( jcWrapped16);
        jc23.setVal(org.docx4j.wml.JcEnumeration.CENTER);
        // Create object for tr
        Tr tr17 = wmlObjectFactory.createTr();
        tbl4.getContent().add( tr17);
        // Create object for tc (wrapped in JAXBElement)
        Tc tc79 = wmlObjectFactory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped79 = wmlObjectFactory.createTrTc(tc79);
        tr17.getContent().add( tcWrapped79);
        // Create object for p
        P p100 = wmlObjectFactory.createP();
        tc79.getContent().add( p100);
        // Create object for pPr
        PPr ppr98 = wmlObjectFactory.createPPr();
        p100.setPPr(ppr98);
        // Create object for rPr
        ParaRPr pararpr98 = wmlObjectFactory.createParaRPr();
        ppr98.setRPr(pararpr98);
        // Create object for rFonts
        RFonts rfonts183 = wmlObjectFactory.createRFonts();
        pararpr98.setRFonts(rfonts183);
        rfonts183.setAscii( "Times");
        rfonts183.setCs( "Times");
        rfonts183.setEastAsia( "Times");
        rfonts183.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue72 = wmlObjectFactory.createBooleanDefaultTrue();
        pararpr98.setB(booleandefaulttrue72);
        // Create object for r
        R r86 = wmlObjectFactory.createR();
        p100.getContent().add( r86);
        // Create object for rPr
        RPr rpr86 = wmlObjectFactory.createRPr();
        r86.setRPr(rpr86);
        // Create object for rFonts
        RFonts rfonts184 = wmlObjectFactory.createRFonts();
        rpr86.setRFonts(rfonts184);
        rfonts184.setAscii( "Times");
        rfonts184.setCs( "Times");
        rfonts184.setEastAsia( "Times");
        rfonts184.setHAnsi( "Times");
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue73 = wmlObjectFactory.createBooleanDefaultTrue();
        rpr86.setB(booleandefaulttrue73);
        // Create object for t (wrapped in JAXBElement)
        Text text86 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped86 = wmlObjectFactory.createRT(text86);
        r86.getContent().add( textWrapped86);
        text86.setValue( "Questions");
        // Create object for tcPr
        TcPr tcpr79 = wmlObjectFactory.createTcPr();
        tc79.setTcPr(tcpr79);
        // Create object for tcW
        TblWidth tblwidth82 = wmlObjectFactory.createTblWidth();
        tcpr79.setTcW(tblwidth82);
        tblwidth82.setW( BigInteger.valueOf( 1811) );
        tblwidth82.setType( "dxa");

//        //------------------------------------------------------------------------------------------
//        // todo questions - excellent
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc80 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped80 = wmlObjectFactory.createTrTc(tc80);
//        tr17.getContent().add( tcWrapped80);
//        // Create object for p
//        P p101 = wmlObjectFactory.createP();
//        tc80.getContent().add( p101);
//        // Create object for pPr
//        PPr ppr99 = wmlObjectFactory.createPPr();
//        p101.setPPr(ppr99);
//        // Create object for rPr
//        ParaRPr pararpr99 = wmlObjectFactory.createParaRPr();
//        ppr99.setRPr(pararpr99);
//        // Create object for rFonts
//        RFonts rfonts185 = wmlObjectFactory.createRFonts();
//        pararpr99.setRFonts(rfonts185);
//        rfonts185.setAscii( "Times New Roman");
//        rfonts185.setCs( "Times New Roman");
//        rfonts185.setEastAsia( "Times New Roman");
//        rfonts185.setHAnsi( "Times New Roman");
//        // Create object for b
//        BooleanDefaultTrue booleandefaulttrue74 = wmlObjectFactory.createBooleanDefaultTrue();
//        pararpr99.setB(booleandefaulttrue74);
//        // Create object for r
//        R r87 = wmlObjectFactory.createR();
//        p101.getContent().add( r87);
//        // Create object for rPr
//        RPr rpr87 = wmlObjectFactory.createRPr();
//        r87.setRPr(rpr87);
//        // Create object for rFonts
//        RFonts rfonts186 = wmlObjectFactory.createRFonts();
//        rpr87.setRFonts(rfonts186);
//        rfonts186.setAscii( "Times New Roman");
//        rfonts186.setCs( "Times New Roman");
//        rfonts186.setEastAsia( "Times New Roman");
//        rfonts186.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text87 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped87 = wmlObjectFactory.createRT(text87);
//        r87.getContent().add( textWrapped87);
//        text87.setValue( "Responded very well to technical questions");
//        // Create object for r
//        R r88 = wmlObjectFactory.createR();
//        p101.getContent().add( r88);
//        // Create object for t (wrapped in JAXBElement)
//        Text text88 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped88 = wmlObjectFactory.createRT(text88);
//        r88.getContent().add( textWrapped88);
//        text88.setValue( " ");
//        text88.setSpace( "preserve");
//        // Create object for tcPr
//        TcPr tcpr80 = wmlObjectFactory.createTcPr();
//        tc80.setTcPr(tcpr80);
//        // Create object for tcW
//        TblWidth tblwidth83 = wmlObjectFactory.createTblWidth();
//        tcpr80.setTcW(tblwidth83);
//        tblwidth83.setW( BigInteger.valueOf( 2098) );
//        tblwidth83.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo questions - good
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc81 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped81 = wmlObjectFactory.createTrTc(tc81);
//        tr17.getContent().add( tcWrapped81);
//        // Create object for p
//        P p102 = wmlObjectFactory.createP();
//        tc81.getContent().add( p102);
//        // Create object for pPr
//        PPr ppr100 = wmlObjectFactory.createPPr();
//        p102.setPPr(ppr100);
//        // Create object for rPr
//        ParaRPr pararpr100 = wmlObjectFactory.createParaRPr();
//        ppr100.setRPr(pararpr100);
//        // Create object for rFonts
//        RFonts rfonts187 = wmlObjectFactory.createRFonts();
//        pararpr100.setRFonts(rfonts187);
//        rfonts187.setAscii( "Times New Roman");
//        rfonts187.setCs( "Times New Roman");
//        rfonts187.setEastAsia( "Times New Roman");
//        rfonts187.setHAnsi( "Times New Roman");
//        // Create object for b
//        BooleanDefaultTrue booleandefaulttrue75 = wmlObjectFactory.createBooleanDefaultTrue();
//        pararpr100.setB(booleandefaulttrue75);
//        // Create object for r
//        R r89 = wmlObjectFactory.createR();
//        p102.getContent().add( r89);
//        // Create object for rPr
//        RPr rpr88 = wmlObjectFactory.createRPr();
//        r89.setRPr(rpr88);
//        // Create object for rFonts
//        RFonts rfonts188 = wmlObjectFactory.createRFonts();
//        rpr88.setRFonts(rfonts188);
//        rfonts188.setAscii( "Times New Roman");
//        rfonts188.setCs( "Times New Roman");
//        rfonts188.setEastAsia( "Times New Roman");
//        rfonts188.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text89 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped89 = wmlObjectFactory.createRT(text89);
//        r89.getContent().add( textWrapped89);
//        text89.setValue( "Could answer most technical questions related to the presentation");
//        // Create object for tcPr
//        TcPr tcpr81 = wmlObjectFactory.createTcPr();
//        tc81.setTcPr(tcpr81);
//        // Create object for tcW
//        TblWidth tblwidth84 = wmlObjectFactory.createTblWidth();
//        tcpr81.setTcW(tblwidth84);
//        tblwidth84.setW( BigInteger.valueOf( 2098) );
//        tblwidth84.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo questions - fair
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc82 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped82 = wmlObjectFactory.createTrTc(tc82);
//        tr17.getContent().add( tcWrapped82);
//        // Create object for p
//        P p103 = wmlObjectFactory.createP();
//        tc82.getContent().add( p103);
//        // Create object for pPr
//        PPr ppr101 = wmlObjectFactory.createPPr();
//        p103.setPPr(ppr101);
//        // Create object for rPr
//        ParaRPr pararpr101 = wmlObjectFactory.createParaRPr();
//        ppr101.setRPr(pararpr101);
//        // Create object for rFonts
//        RFonts rfonts189 = wmlObjectFactory.createRFonts();
//        pararpr101.setRFonts(rfonts189);
//        rfonts189.setAscii( "Times New Roman");
//        rfonts189.setCs( "Times New Roman");
//        rfonts189.setEastAsia( "Times New Roman");
//        rfonts189.setHAnsi( "Times New Roman");
//        // Create object for b
//        BooleanDefaultTrue booleandefaulttrue76 = wmlObjectFactory.createBooleanDefaultTrue();
//        pararpr101.setB(booleandefaulttrue76);
//        // Create object for r
//        R r90 = wmlObjectFactory.createR();
//        p103.getContent().add( r90);
//        // Create object for rPr
//        RPr rpr89 = wmlObjectFactory.createRPr();
//        r90.setRPr(rpr89);
//        // Create object for rFonts
//        RFonts rfonts190 = wmlObjectFactory.createRFonts();
//        rpr89.setRFonts(rfonts190);
//        rfonts190.setAscii( "Times New Roman");
//        rfonts190.setCs( "Times New Roman");
//        rfonts190.setEastAsia( "Times New Roman");
//        rfonts190.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text90 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped90 = wmlObjectFactory.createRT(text90);
//        r90.getContent().add( textWrapped90);
//        text90.setValue( "Could answer some technical questions related to the presentation");
//        // Create object for tcPr
//        TcPr tcpr82 = wmlObjectFactory.createTcPr();
//        tc82.setTcPr(tcpr82);
//        // Create object for tcW
//        TblWidth tblwidth85 = wmlObjectFactory.createTblWidth();
//        tcpr82.setTcW(tblwidth85);
//        tblwidth85.setW( BigInteger.valueOf( 2098) );
//        tblwidth85.setType( "dxa");
//
//        //------------------------------------------------------------------------------------------
//        // todo questions - poor
//
//        // Create object for tc (wrapped in JAXBElement)
//        Tc tc83 = wmlObjectFactory.createTc();
//        JAXBElement<org.docx4j.wml.Tc> tcWrapped83 = wmlObjectFactory.createTrTc(tc83);
//        tr17.getContent().add( tcWrapped83);
//        // Create object for p
//        P p104 = wmlObjectFactory.createP();
//        tc83.getContent().add( p104);
//        // Create object for pPr
//        PPr ppr102 = wmlObjectFactory.createPPr();
//        p104.setPPr(ppr102);
//        // Create object for rPr
//        ParaRPr pararpr102 = wmlObjectFactory.createParaRPr();
//        ppr102.setRPr(pararpr102);
//        // Create object for rFonts
//        RFonts rfonts191 = wmlObjectFactory.createRFonts();
//        pararpr102.setRFonts(rfonts191);
//        rfonts191.setAscii( "Times New Roman");
//        rfonts191.setCs( "Times New Roman");
//        rfonts191.setEastAsia( "Times New Roman");
//        rfonts191.setHAnsi( "Times New Roman");
//        // Create object for r
//        R r91 = wmlObjectFactory.createR();
//        p104.getContent().add( r91);
//        // Create object for rPr
//        RPr rpr90 = wmlObjectFactory.createRPr();
//        r91.setRPr(rpr90);
//        // Create object for rFonts
//        RFonts rfonts192 = wmlObjectFactory.createRFonts();
//        rpr90.setRFonts(rfonts192);
//        rfonts192.setAscii( "Times New Roman");
//        rfonts192.setCs( "Times New Roman");
//        rfonts192.setEastAsia( "Times New Roman");
//        rfonts192.setHAnsi( "Times New Roman");
//        // Create object for t (wrapped in JAXBElement)
//        Text text91 = wmlObjectFactory.createText();
//        JAXBElement<org.docx4j.wml.Text> textWrapped91 = wmlObjectFactory.createRT(text91);
//        r91.getContent().add( textWrapped91);
//        text91.setValue( "Could not answer any technical questions related to the presentation");
//        // Create object for tcPr
//        TcPr tcpr83 = wmlObjectFactory.createTcPr();
//        tc83.setTcPr(tcpr83);
//        // Create object for tcW
//        TblWidth tblwidth86 = wmlObjectFactory.createTblWidth();
//        tcpr83.setTcW(tblwidth86);
//        tblwidth86.setW( BigInteger.valueOf( 2098) );
//        tblwidth86.setType( "dxa");

        makeUpTechnicalTableQuestionsRow(assessment, wmlObjectFactory, tr17);


        // Create object for trPr
        TrPr trpr17 = wmlObjectFactory.createTrPr();
        tr17.setTrPr(trpr17);
        // Create object for trHeight (wrapped in JAXBElement)
        CTHeight height10 = wmlObjectFactory.createCTHeight();
        JAXBElement<org.docx4j.wml.CTHeight> heightWrapped10 = wmlObjectFactory.createCTTrPrBaseTrHeight(height10);
        trpr17.getCnfStyleOrDivIdOrGridBefore().add( heightWrapped10);
        height10.setVal( BigInteger.valueOf( 280) );
        // Create object for jc (wrapped in JAXBElement)
        Jc jc24 = wmlObjectFactory.createJc();
        JAXBElement<org.docx4j.wml.Jc> jcWrapped17 = wmlObjectFactory.createCTTrPrBaseJc(jc24);
        trpr17.getCnfStyleOrDivIdOrGridBefore().add( jcWrapped17);
        jc24.setVal(org.docx4j.wml.JcEnumeration.CENTER);
        // Create object for tblPr
        TblPr tblpr4 = wmlObjectFactory.createTblPr();
        tbl4.setTblPr(tblpr4);
        // Create object for tblStyle
        CTTblPrBase.TblStyle tblprbasetblstyle4 = wmlObjectFactory.createCTTblPrBaseTblStyle();
        tblpr4.setTblStyle(tblprbasetblstyle4);
        tblprbasetblstyle4.setVal( "a2");
        // Create object for tblBorders
        TblBorders tblborders4 = wmlObjectFactory.createTblBorders();
        tblpr4.setTblBorders(tblborders4);
        // Create object for insideH
        CTBorder border51 = wmlObjectFactory.createCTBorder();
        tblborders4.setInsideH(border51);
        border51.setVal(org.docx4j.wml.STBorder.SINGLE);
        border51.setSz( BigInteger.valueOf( 4) );
        border51.setColor( "000000");
        border51.setSpace( BigInteger.valueOf( 0) );
        // Create object for top
        CTBorder border52 = wmlObjectFactory.createCTBorder();
        tblborders4.setTop(border52);
        border52.setVal(org.docx4j.wml.STBorder.SINGLE);
        border52.setSz( BigInteger.valueOf( 4) );
        border52.setColor( "000000");
        border52.setSpace( BigInteger.valueOf( 0) );
        // Create object for bottom
        CTBorder border53 = wmlObjectFactory.createCTBorder();
        tblborders4.setBottom(border53);
        border53.setVal(org.docx4j.wml.STBorder.SINGLE);
        border53.setSz( BigInteger.valueOf( 4) );
        border53.setColor( "000000");
        border53.setSpace( BigInteger.valueOf( 0) );
        // Create object for insideV
        CTBorder border54 = wmlObjectFactory.createCTBorder();
        tblborders4.setInsideV(border54);
        border54.setVal(org.docx4j.wml.STBorder.SINGLE);
        border54.setSz( BigInteger.valueOf( 4) );
        border54.setColor( "000000");
        border54.setSpace( BigInteger.valueOf( 0) );
        // Create object for right
        CTBorder border55 = wmlObjectFactory.createCTBorder();
        tblborders4.setRight(border55);
        border55.setVal(org.docx4j.wml.STBorder.SINGLE);
        border55.setSz( BigInteger.valueOf( 4) );
        border55.setColor( "000000");
        border55.setSpace( BigInteger.valueOf( 0) );
        // Create object for left
        CTBorder border56 = wmlObjectFactory.createCTBorder();
        tblborders4.setLeft(border56);
        border56.setVal(org.docx4j.wml.STBorder.SINGLE);
        border56.setSz( BigInteger.valueOf( 4) );
        border56.setColor( "000000");
        border56.setSpace( BigInteger.valueOf( 0) );
        // Create object for jc
        Jc jc25 = wmlObjectFactory.createJc();
        tblpr4.setJc(jc25);
        jc25.setVal(org.docx4j.wml.JcEnumeration.CENTER);
        // Create object for tblW
        TblWidth tblwidth87 = wmlObjectFactory.createTblWidth();
        tblpr4.setTblW(tblwidth87);
        tblwidth87.setW( BigInteger.valueOf( 10203) );
        tblwidth87.setType( "dxa");
        // Create object for tblLayout
        CTTblLayoutType tbllayouttype4 = wmlObjectFactory.createCTTblLayoutType();
        tblpr4.setTblLayout(tbllayouttype4);
        tbllayouttype4.setType(org.docx4j.wml.STTblLayoutType.FIXED);
        // Create object for tblLook
        CTTblLook tbllook4 = wmlObjectFactory.createCTTblLook();
        tblpr4.setTblLook(tbllook4);
        tbllook4.setVal( "0400");
        tbllook4.setFirstColumn(org.docx4j.sharedtypes.STOnOff.ZERO);
        tbllook4.setFirstRow(org.docx4j.sharedtypes.STOnOff.ZERO);
        tbllook4.setLastColumn(org.docx4j.sharedtypes.STOnOff.ZERO);
        tbllook4.setLastRow(org.docx4j.sharedtypes.STOnOff.ZERO);
        tbllook4.setNoHBand(org.docx4j.sharedtypes.STOnOff.ZERO);
        tbllook4.setNoVBand(org.docx4j.sharedtypes.STOnOff.ONE);
        // Create object for tblGrid
        TblGrid tblgrid4 = wmlObjectFactory.createTblGrid();
        tbl4.setTblGrid(tblgrid4);
        // Create object for gridCol
        TblGridCol tblgridcol17 = wmlObjectFactory.createTblGridCol();
        tblgrid4.getGridCol().add( tblgridcol17);
        tblgridcol17.setW( BigInteger.valueOf( 1811) );
        // Create object for gridCol
        TblGridCol tblgridcol18 = wmlObjectFactory.createTblGridCol();
        tblgrid4.getGridCol().add( tblgridcol18);
        tblgridcol18.setW( BigInteger.valueOf( 2098) );
        // Create object for gridCol
        TblGridCol tblgridcol19 = wmlObjectFactory.createTblGridCol();
        tblgrid4.getGridCol().add( tblgridcol19);
        tblgridcol19.setW( BigInteger.valueOf( 2098) );
        // Create object for gridCol
        TblGridCol tblgridcol20 = wmlObjectFactory.createTblGridCol();
        tblgrid4.getGridCol().add( tblgridcol20);
        tblgridcol20.setW( BigInteger.valueOf( 2098) );
        // Create object for gridCol
        TblGridCol tblgridcol21 = wmlObjectFactory.createTblGridCol();
        tblgrid4.getGridCol().add( tblgridcol21);
        tblgridcol21.setW( BigInteger.valueOf( 2098) );
        // Create object for p
        P p105 = wmlObjectFactory.createP();
        body.getContent().add( p105);
        // Create object for pPr
        PPr ppr103 = wmlObjectFactory.createPPr();
        p105.setPPr(ppr103);
        // Create object for rPr
        ParaRPr pararpr103 = wmlObjectFactory.createParaRPr();
        ppr103.setRPr(pararpr103);
        // Create object for rFonts
        RFonts rfonts193 = wmlObjectFactory.createRFonts();
        pararpr103.setRFonts(rfonts193);
        rfonts193.setAscii( "Times");
        rfonts193.setCs( "Times");
        rfonts193.setEastAsia( "Times");
        rfonts193.setHAnsi( "Times");
        // Create object for spacing
        PPrBase.Spacing pprbasespacing15 = wmlObjectFactory.createPPrBaseSpacing();
        ppr103.setSpacing(pprbasespacing15);
        pprbasespacing15.setAfter( BigInteger.valueOf( 0) );
        pprbasespacing15.setLine( BigInteger.valueOf( 240) );
        pprbasespacing15.setLineRule(org.docx4j.wml.STLineSpacingRule.AUTO);
        // Create object for p
        P p106 = wmlObjectFactory.createP();
        body.getContent().add( p106);
        // Create object for pPr
        PPr ppr104 = wmlObjectFactory.createPPr();
        p106.setPPr(ppr104);
        // Create object for jc
        Jc jc26 = wmlObjectFactory.createJc();
        ppr104.setJc(jc26);
        jc26.setVal(org.docx4j.wml.JcEnumeration.CENTER);
        // Create object for spacing
        PPrBase.Spacing pprbasespacing16 = wmlObjectFactory.createPPrBaseSpacing();
        ppr104.setSpacing(pprbasespacing16);
        pprbasespacing16.setAfter( BigInteger.valueOf( 0) );
        pprbasespacing16.setLine( BigInteger.valueOf( 240) );
        pprbasespacing16.setLineRule(org.docx4j.wml.STLineSpacingRule.AUTO);
        // Create object for r
        R r92 = wmlObjectFactory.createR();
        p106.getContent().add( r92);
        // Create object for rPr
        RPr rpr91 = wmlObjectFactory.createRPr();
        r92.setRPr(rpr91);
        // Create object for sz
        HpsMeasure hpsmeasure41 = wmlObjectFactory.createHpsMeasure();
        rpr91.setSz(hpsmeasure41);
        hpsmeasure41.setVal( BigInteger.valueOf( 16) );
        // Create object for rFonts
        RFonts rfonts194 = wmlObjectFactory.createRFonts();
        rpr91.setRFonts(rfonts194);
        rfonts194.setAscii( "Helvetica Neue");
        rfonts194.setCs( "Helvetica Neue");
        rfonts194.setEastAsia( "Helvetica Neue");
        rfonts194.setHAnsi( "Helvetica Neue");
        // Create object for i
        BooleanDefaultTrue booleandefaulttrue77 = wmlObjectFactory.createBooleanDefaultTrue();
        rpr91.setI(booleandefaulttrue77);
        // Create object for szCs
        HpsMeasure hpsmeasure42 = wmlObjectFactory.createHpsMeasure();
        rpr91.setSzCs(hpsmeasure42);
        hpsmeasure42.setVal( BigInteger.valueOf( 16) );
        // Create object for t (wrapped in JAXBElement)
        Text text92 = wmlObjectFactory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped92 = wmlObjectFactory.createRT(text92);
        r92.getContent().add( textWrapped92);
        text92.setValue( "Adapted with enhancement from Â© 2004 National Council of Teachers of English/International Reading Association");
        // Create object for p
        P p107 = wmlObjectFactory.createP();
        body.getContent().add( p107);
        // Create object for pPr
        PPr ppr105 = wmlObjectFactory.createPPr();
        p107.setPPr(ppr105);
        // Create object for rPr
        ParaRPr pararpr104 = wmlObjectFactory.createParaRPr();
        ppr105.setRPr(pararpr104);
        // Create object for sz
        HpsMeasure hpsmeasure43 = wmlObjectFactory.createHpsMeasure();
        pararpr104.setSz(hpsmeasure43);
        hpsmeasure43.setVal( BigInteger.valueOf( 28) );
        // Create object for b
        BooleanDefaultTrue booleandefaulttrue78 = wmlObjectFactory.createBooleanDefaultTrue();
        pararpr104.setB(booleandefaulttrue78);
        // Create object for color
        Color color = wmlObjectFactory.createColor();
        pararpr104.setColor(color);
        color.setVal( "2E75B5");
        // Create object for szCs
        HpsMeasure hpsmeasure44 = wmlObjectFactory.createHpsMeasure();
        pararpr104.setSzCs(hpsmeasure44);
        hpsmeasure44.setVal( BigInteger.valueOf( 28) );
        // Create object for sectPr
        SectPr sectpr = wmlObjectFactory.createSectPr();
        body.setSectPr(sectpr);
        // Create object for pgNumType
        CTPageNumber pagenumber = wmlObjectFactory.createCTPageNumber();
        sectpr.setPgNumType(pagenumber);
        pagenumber.setStart( BigInteger.valueOf( 1) );
        // Create object for pgSz
        SectPr.PgSz sectprpgsz = wmlObjectFactory.createSectPrPgSz();
        sectpr.setPgSz(sectprpgsz);
        sectprpgsz.setH( BigInteger.valueOf( 16838) );
        sectprpgsz.setW( BigInteger.valueOf( 11906) );
        // Create object for pgMar
        SectPr.PgMar sectprpgmar = wmlObjectFactory.createSectPrPgMar();
        sectpr.setPgMar(sectprpgmar);
        sectprpgmar.setGutter( BigInteger.valueOf( 0) );
        sectprpgmar.setLeft( BigInteger.valueOf( 1440) );
        sectprpgmar.setRight( BigInteger.valueOf( 1440) );
        sectprpgmar.setTop( BigInteger.valueOf( 1440) );
        sectprpgmar.setBottom( BigInteger.valueOf( 720) );
        sectprpgmar.setHeader( BigInteger.valueOf( 706) );
        sectprpgmar.setFooter( BigInteger.valueOf( 706) );
        // Create object for cols
        CTColumns columns = wmlObjectFactory.createCTColumns();
        sectpr.setCols(columns);
        columns.setSpace( BigInteger.valueOf( 720) );
        document.setIgnorable( "w14 w15 w16se w16cid wp14");

        return document;
    }


//    public void newTableCell(org.docx4j.wml.ObjectFactory factory,
//                             Tr tableRow,
//                             RPr rpr,
//                             org.docx4j.wml.JcEnumeration enumeration,
//                             String content,
//                             boolean hasBackgroundColor,
//                             String backgroundColor) {
//
//        Tc tableCell = factory.createTc();
//        P p = factory.createP();
//
//        setParagraphAlign(factory, p, enumeration);
//
//        Text t = factory.createText();
//        t.setValue(content);
//        R run = factory.createR();
//        run.setRPr(rpr);
//        run.getContent().add(t);
//        p.getContent().add(run);
//        tableCell.getContent().add(p);
//
//        if (hasBackgroundColor) {
//            TcPr tcPr = tableCell.getTcPr();
//            if (tcPr == null) {
//                tcPr = factory.createTcPr();
//            }
//            CTShd shd = tcPr.getShd();
//            if (shd == null) {
//                shd = factory.createCTShd();
//            }
//            shd.setColor("auto");
//            shd.setFill(backgroundColor);
//            tcPr.setShd(shd);
//            tableCell.setTcPr(tcPr);
//        }
//
//        tableRow.getContent().add(tableCell);
//    }

    private void newTableCell(org.docx4j.wml.ObjectFactory factory,
                              Tr tableRow,
                              String content) {
        newTableCell(factory, tableRow, content, false);
    }

    private void newTableCell(org.docx4j.wml.ObjectFactory factory,
                              Tr tableRow,
                              String content,
                              boolean active) {

        // Create object for tc (wrapped in JAXBElement)
        Tc tableCell = factory.createTc();
        JAXBElement<org.docx4j.wml.Tc> tcWrapped15 = factory.createTrTc(tableCell);
        tableRow.getContent().add( tcWrapped15);

        // Create object for p
        P p21 = factory.createP();
        tableCell.getContent().add( p21);

        // Create object for pPr
        PPr ppr19 = factory.createPPr();
        p21.setPPr(ppr19);

        // Create object for rPr
        ParaRPr pararpr19 = factory.createParaRPr();
        ppr19.setRPr(pararpr19);

        // Create object for rFonts
        RFonts rfonts38 = factory.createRFonts();
        pararpr19.setRFonts(rfonts38);
        rfonts38.setAscii( "Times New Roman");
        rfonts38.setCs( "Times New Roman");
        rfonts38.setEastAsia( "Times New Roman");
        rfonts38.setHAnsi( "Times New Roman");

        // Create object for spacing
        PPrBase.Spacing pprbasespacing2 = factory.createPPrBaseSpacing();
        ppr19.setSpacing(pprbasespacing2);
        pprbasespacing2.setAfter( BigInteger.valueOf( 120) );

        // Create object for r
        R r20 = factory.createR();
        p21.getContent().add( r20);

        // Create object for rPr
        RPr rpr20 = factory.createRPr();
        r20.setRPr(rpr20);

        // Create object for rFonts
        RFonts rfonts39 = factory.createRFonts();
        rpr20.setRFonts(rfonts39);
        rfonts39.setAscii( "Times New Roman");
        rfonts39.setCs( "Times New Roman");
        rfonts39.setEastAsia( "Times New Roman");
        rfonts39.setHAnsi( "Times New Roman");

        // Create object for t (wrapped in JAXBElement)
        Text text20 = factory.createText();
        JAXBElement<org.docx4j.wml.Text> textWrapped20 = factory.createRT(text20);
        r20.getContent().add( textWrapped20);
        text20.setValue(content);

        // Create object for tcPr
        TcPr tcpr15 = factory.createTcPr();
        tableCell.setTcPr(tcpr15);

        // Create object for tcW
        TblWidth tblwidth16 = factory.createTblWidth();
        tcpr15.setTcW(tblwidth16);
        tblwidth16.setW( BigInteger.valueOf( 2098) );
        tblwidth16.setType( "dxa");

        // Create object for shd
        if (active) {
            CTShd shd = factory.createCTShd();
            tcpr15.setShd(shd);
            shd.setFill( "D0CECE");
            shd.setColor( "auto");
            shd.setVal(org.docx4j.wml.STShd.CLEAR);
        }
    }

    private void makeUpContentTableFocusRow(MarksAssessment assessment,
                                            org.docx4j.wml.ObjectFactory factory,
                                            Tr tableRow) {

        // <score, isActive>
        LinkedHashMap<Integer, Boolean> temp = new LinkedHashMap<>();
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_focus_excellent, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_focus_good, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_focus_fair, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_focus_poor, false);

        int score = assessment.getMarkSets().get(TABLE1_NAME)
                .getMarks().get(AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_content_focus));

        switch (score) {
            case 4: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_focus_excellent, true);
            } break;
            case 3: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_focus_good, true);
            } break;
            case 2: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_focus_fair, true);
            } break;
            case 1: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_focus_poor, true);
            } break;
        }

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_content_focus_excellent),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_content_focus_excellent));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_content_focus_good),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_content_focus_good));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_content_focus_fair),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_content_focus_fair));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_content_focus_poor),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_content_focus_poor));
    }

    private void makeUpContentTableOrganizationRow(MarksAssessment assessment,
                                            org.docx4j.wml.ObjectFactory factory,
                                            Tr tableRow) {

        // <score, isActive>
        LinkedHashMap<Integer, Boolean> temp = new LinkedHashMap<>();
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_organization_excellent, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_organization_good, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_organization_fair, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_organization_poor, false);

        int score = assessment.getMarkSets().get(TABLE1_NAME)
                .getMarks().get(AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_content_organization));

        switch (score) {
            case 4: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_organization_excellent, true);
            } break;
            case 3: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_organization_good, true);
            } break;
            case 2: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_organization_fair, true);
            } break;
            case 1: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_organization_poor, true);
            } break;
        }

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_content_organization_excellent),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_content_organization_excellent));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_content_organization_good),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_content_organization_good));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_content_organization_fair),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_content_organization_fair));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_content_organization_poor),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_content_organization_poor));
    }

    private void makeUpContentTableVisualAidsRow(MarksAssessment assessment,
                                                 org.docx4j.wml.ObjectFactory factory,
                                                 Tr tableRow) {

        // <score, isActive>
        LinkedHashMap<Integer, Boolean> temp = new LinkedHashMap<>();
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_visual_aids_excellent, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_visual_aids_good, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_visual_aids_fair, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_visual_aids_poor, false);

        int score = assessment.getMarkSets().get(TABLE1_NAME)
                .getMarks().get(AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_content_visual_aids));

        switch (score) {
            case 4: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_visual_aids_excellent, true);
            } break;
            case 3: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_visual_aids_good, true);
            } break;
            case 2: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_visual_aids_fair, true);
            } break;
            case 1: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_visual_aids_poor, true);
            } break;
        }

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_content_visual_aids_excellent),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_content_visual_aids_excellent));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_content_visual_aids_good),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_content_visual_aids_good));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_content_visual_aids_fair),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_content_visual_aids_fair));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_content_visual_aids_poor),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_content_visual_aids_poor));
    }

    private void makeUpContentTableQaRow(MarksAssessment assessment,
                                                 org.docx4j.wml.ObjectFactory factory,
                                                 Tr tableRow) {

        // <score, isActive>
        LinkedHashMap<Integer, Boolean> temp = new LinkedHashMap<>();
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_qa_excellent, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_qa_good, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_qa_fair, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_qa_poor, false);

        int score = assessment.getMarkSets().get(TABLE1_NAME)
                .getMarks().get(AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_content_qa));

        switch (score) {
            case 4: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_qa_excellent, true);
            } break;
            case 3: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_qa_good, true);
            } break;
            case 2: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_qa_fair, true);
            } break;
            case 1: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_content_qa_poor, true);
            } break;
        }

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_content_qa_excellent),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_content_qa_excellent));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_content_qa_good),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_content_qa_good));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_content_qa_fair),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_content_qa_fair));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_content_qa_poor),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_content_qa_poor));
    }

    private void makeUpLanguageTableEyeContactRow(MarksAssessment assessment,
                                         org.docx4j.wml.ObjectFactory factory,
                                         Tr tableRow) {

        // <score, isActive>
        LinkedHashMap<Integer, Boolean> temp = new LinkedHashMap<>();
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_language_eye_contact_excellent, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_language_eye_contact_good, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_language_eye_contact_fair, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_language_eye_contact_poor, false);

        int score = assessment.getMarkSets().get(TABLE2_NAME)
                .getMarks().get(AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_language_eye_contact));

        switch (score) {
            case 4: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_language_eye_contact_excellent, true);
            } break;
            case 3: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_language_eye_contact_good, true);
            } break;
            case 2: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_language_eye_contact_fair, true);
            } break;
            case 1: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_language_eye_contact_poor, true);
            } break;
        }

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_language_eye_contact_excellent),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_language_eye_contact_excellent));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_language_eye_contact_good),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_language_eye_contact_good));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_language_eye_contact_fair),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_language_eye_contact_fair));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_language_eye_contact_poor),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_language_eye_contact_poor));
    }

    private void makeUpLanguageTableEnthusiasmRow(MarksAssessment assessment,
                                                  org.docx4j.wml.ObjectFactory factory,
                                                  Tr tableRow) {

        // <score, isActive>
        LinkedHashMap<Integer, Boolean> temp = new LinkedHashMap<>();
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_language_enthusiasm_excellent, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_language_enthusiasm_good, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_language_enthusiasm_fair, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_language_enthusiasm_poor, false);

        int score = assessment.getMarkSets().get(TABLE2_NAME)
                .getMarks().get(AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_language_enthusiasm));

        switch (score) {
            case 4: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_language_enthusiasm_excellent, true);
            } break;
            case 3: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_language_enthusiasm_good, true);
            } break;
            case 2: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_language_enthusiasm_fair, true);
            } break;
            case 1: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_language_enthusiasm_poor, true);
            } break;
        }

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_language_enthusiasm_excellent),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_language_enthusiasm_excellent));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_language_enthusiasm_good),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_language_enthusiasm_good));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_language_enthusiasm_fair),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_language_enthusiasm_fair));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_language_enthusiasm_poor),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_language_enthusiasm_poor));
    }

    private void makeUpLanguageTableElocutionRow(MarksAssessment assessment,
                                                  org.docx4j.wml.ObjectFactory factory,
                                                  Tr tableRow) {

        // <score, isActive>
        LinkedHashMap<Integer, Boolean> temp = new LinkedHashMap<>();
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_language_elocution_excellent, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_language_elocution_good, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_language_elocution_fair, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_language_elocution_poor, false);

        int score = assessment.getMarkSets().get(TABLE2_NAME)
                .getMarks().get(AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_language_elocution));

        switch (score) {
            case 4: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_language_elocution_excellent, true);
            } break;
            case 3: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_language_elocution_good, true);
            } break;
            case 2: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_language_elocution_fair, true);
            } break;
            case 1: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_language_elocution_poor, true);
            } break;
        }

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_language_elocution_excellent),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_language_elocution_excellent));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_language_elocution_good),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_language_elocution_good));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_language_elocution_fair),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_language_elocution_fair));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_language_elocution_poor),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_language_elocution_poor));
    }

    private void makeUpTechnicalTableKnowledgeRow(MarksAssessment assessment,
                                                 org.docx4j.wml.ObjectFactory factory,
                                                 Tr tableRow) {

        // <score, isActive>
        LinkedHashMap<Integer, Boolean> temp = new LinkedHashMap<>();
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_knowledge_excellent, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_knowledge_good, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_knowledge_fair, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_knowledge_poor, false);

        int score = assessment.getMarkSets().get(TABLE3_NAME)
                .getMarks().get(AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_technical_knowledge));

        switch (score) {
            case 4: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_knowledge_excellent, true);
            } break;
            case 3: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_knowledge_good, true);
            } break;
            case 2: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_knowledge_fair, true);
            } break;
            case 1: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_knowledge_poor, true);
            } break;
        }

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_technical_knowledge_excellent),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_technical_knowledge_excellent));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_technical_knowledge_good),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_technical_knowledge_good));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_technical_knowledge_fair),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_technical_knowledge_fair));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_technical_knowledge_poor),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_technical_knowledge_poor));
    }

    private void makeUpTechnicalTableResearchRow(MarksAssessment assessment,
                                                  org.docx4j.wml.ObjectFactory factory,
                                                  Tr tableRow) {

        // <score, isActive>
        LinkedHashMap<Integer, Boolean> temp = new LinkedHashMap<>();
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_research_excellent, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_research_good, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_research_fair, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_research_poor, false);

        int score = assessment.getMarkSets().get(TABLE3_NAME)
                .getMarks().get(AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_technical_research));

        switch (score) {
            case 4: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_research_excellent, true);
            } break;
            case 3: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_research_good, true);
            } break;
            case 2: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_research_fair, true);
            } break;
            case 1: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_research_poor, true);
            } break;
        }

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_technical_research_excellent),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_technical_research_excellent));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_technical_research_good),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_technical_research_good));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_technical_research_fair),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_technical_research_fair));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_technical_research_poor),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_technical_research_poor));
    }

    private void makeUpTechnicalTableDiscussionOfNewIdeaRow(MarksAssessment assessment,
                                                 org.docx4j.wml.ObjectFactory factory,
                                                 Tr tableRow) {

        // <score, isActive>
        LinkedHashMap<Integer, Boolean> temp = new LinkedHashMap<>();
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_discussion_of_new_idea_excellent, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_discussion_of_new_idea_good, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_discussion_of_new_idea_fair, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_discussion_of_new_idea_poor, false);

        int score = assessment.getMarkSets().get(TABLE3_NAME)
                .getMarks().get(AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_technical_discussion_of_new_idea));

        switch (score) {
            case 4: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_discussion_of_new_idea_excellent, true);
            } break;
            case 3: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_discussion_of_new_idea_good, true);
            } break;
            case 2: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_discussion_of_new_idea_fair, true);
            } break;
            case 1: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_discussion_of_new_idea_poor, true);
            } break;
        }

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_technical_discussion_of_new_idea_excellent),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_technical_discussion_of_new_idea_excellent));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_technical_discussion_of_new_idea_good),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_technical_discussion_of_new_idea_good));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_technical_discussion_of_new_idea_fair),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_technical_discussion_of_new_idea_fair));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_technical_discussion_of_new_idea_poor),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_technical_discussion_of_new_idea_poor));
    }

    private void makeUpTechnicalTableArgumentRow(MarksAssessment assessment,
                                                 org.docx4j.wml.ObjectFactory factory,
                                                 Tr tableRow) {

        // <score, isActive>
        LinkedHashMap<Integer, Boolean> temp = new LinkedHashMap<>();
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_argument_excellent, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_argument_good, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_argument_fair, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_argument_poor, false);

        int score = assessment.getMarkSets().get(TABLE3_NAME)
                .getMarks().get(AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_technical_argument));

        switch (score) {
            case 4: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_argument_excellent, true);
            } break;
            case 3: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_argument_good, true);
            } break;
            case 2: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_argument_fair, true);
            } break;
            case 1: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_argument_poor, true);
            } break;
        }

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_technical_argument_excellent),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_technical_argument_excellent));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_technical_argument_good),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_technical_argument_good));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_technical_argument_fair),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_technical_argument_fair));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_technical_argument_poor),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_technical_argument_poor));
    }

    private void makeUpTechnicalTableQuestionsRow(MarksAssessment assessment,
                                                  org.docx4j.wml.ObjectFactory factory,
                                                  Tr tableRow) {

        // <score, isActive>
        LinkedHashMap<Integer, Boolean> temp = new LinkedHashMap<>();
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_questions_excellent, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_questions_good, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_questions_fair, false);
        temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_questions_poor, false);

        int score = assessment.getMarkSets().get(TABLE3_NAME)
                .getMarks().get(AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_technical_questions));

        switch (score) {
            case 4: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_questions_excellent, true);
            } break;
            case 3: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_questions_good, true);
            } break;
            case 2: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_questions_fair, true);
            } break;
            case 1: {
                temp.put(com.ais.uxclass.jhlee.R.string.assessor_technical_questions_poor, true);
            } break;
        }

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_technical_questions_excellent),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_technical_questions_excellent));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_technical_questions_good),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_technical_questions_good));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_technical_questions_fair),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_technical_questions_fair));

        newTableCell(factory, tableRow,
                AndroidContext.Companion.string(com.ais.uxclass.jhlee.R.string.assessor_technical_questions_poor),
                temp.get(com.ais.uxclass.jhlee.R.string.assessor_technical_questions_poor));
    }
}
