package com.ais.uxclass.jhlee.dagger.scopes

import javax.inject.Scope

@Scope
@Retention
annotation class PerFragment
