package com.ais.uxclass.jhlee.base

import android.support.v7.app.AppCompatActivity

/**
 * Date 2018. 1. 9.
 * Author Jun-hyoung, Lee
 */
abstract class BaseActivity : AppCompatActivity() {

}