package com.ais.uxclass.jhlee

import android.support.multidex.MultiDexApplication
import com.ais.uxclass.jhlee.app.assessor.domain.WordFileManager
import com.ais.uxclass.jhlee.base.AndroidContext
import com.ais.uxclass.jhlee.dagger.components.AppComponent
import com.ais.uxclass.jhlee.dagger.components.DaggerAppComponent
import com.ais.uxclass.jhlee.dagger.modules.AppModule
import android.os.StrictMode



/**
 * Date 2018. 1. 8.
 * Author Jun-hyoung, Lee
 */
class MyApplication : MultiDexApplication() {

    val appComponent: AppComponent
        get() = DaggerAppComponent.builder().appModule(AppModule(this)).build()

    override fun onCreate() {
        super.onCreate()

        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())


        AndroidContext.initialize(this)

        WordFileManager.instance.initialize()

        setUpInjector()
    }

    private fun setUpInjector() {
        this.appComponent.inject(this)
    }

}