package com.ais.uxclass.jhlee.app.assessor.domain.model

data class ProfileSet(
        var name: String,
        var id: String,
        var semsester: String,
        var topic: String
)