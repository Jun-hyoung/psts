package com.ais.uxclass.jhlee.dagger.components

import android.support.v4.app.Fragment

import dagger.Component
import com.ais.uxclass.jhlee.base.BaseFragment
import com.ais.uxclass.jhlee.dagger.modules.FragmentModule
import com.ais.uxclass.jhlee.dagger.scopes.PerFragment

@PerFragment
@Component(dependencies = [(AppComponent::class)], modules = arrayOf(FragmentModule::class))
interface FragmentComponent {

    fun inject(fragment: BaseFragment)

    fun fragment(): Fragment
}
