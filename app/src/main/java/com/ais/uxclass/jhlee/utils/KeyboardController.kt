package com.ais.uxclass.jhlee.utils

import android.content.Context
import android.graphics.Rect
import android.os.Build
import android.view.View
import android.view.ViewTreeObserver
import android.view.inputmethod.InputMethodManager
import com.ais.uxclass.jhlee.base.AndroidContext
import java.lang.ref.WeakReference

/**
 * Date 2018. 2. 1.
 * Author Jun-hyoung, Lee (niceguy0315@hotmail.com)
 */
class KeyboardController private constructor() {

    private object Holder { val INSTANCE = KeyboardController() }

    fun show(view: View) {
        val imm = AndroidContext.application().getSystemService(Context.INPUT_METHOD_SERVICE )
                as InputMethodManager
        imm.showSoftInput(view, 0)
    }

    fun hide(view: View) {
        val imm = AndroidContext.application().getSystemService(Context.INPUT_METHOD_SERVICE )
                as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun isVisible(view: View): Boolean {
        val windowVisibleDisplayFrame = Rect()
        val threshold = Math.round(Units.dp2px(KeyboardController.KEYBOARD_VISIBLE_THRESHOLD))

        view.getWindowVisibleDisplayFrame(windowVisibleDisplayFrame)
        val visibleDecorViewHeight = windowVisibleDisplayFrame.height()

        val diff = view.height - visibleDecorViewHeight
        return diff > threshold
    }

    fun subscribe(decorView: View, listener: VisibilityEventListener): Unsubscriber {
        val globalLayoutListener = object : ViewTreeObserver.OnGlobalLayoutListener {
            private val windowVisibleDisplayFrame = Rect()
            private val threshold = Math.round(Units.dp2px(KeyboardController.KEYBOARD_VISIBLE_THRESHOLD))
            private var wasOpened = false

            override fun onGlobalLayout() {
                decorView.getWindowVisibleDisplayFrame(windowVisibleDisplayFrame)
                val visibleDecorViewHeight = windowVisibleDisplayFrame.height()

                val diff = decorView.height - visibleDecorViewHeight
                val isOpen = diff > threshold

                if (isOpen == wasOpened) {
                    // keyboard state has not changed
                    return
                }

                wasOpened = isOpen

                listener.onVisibilityChanged(isOpen)
            }
        }

        decorView.viewTreeObserver.addOnGlobalLayoutListener(globalLayoutListener)

        return Unsubscriber(decorView, globalLayoutListener)
    }


    class Unsubscriber constructor(decorView: View, globalLayoutListener: ViewTreeObserver.OnGlobalLayoutListener) {
        private val weakRefView: WeakReference<View> = WeakReference(decorView)
        private val weakRefListener: WeakReference<ViewTreeObserver.OnGlobalLayoutListener> = WeakReference(globalLayoutListener)

        fun unsubscribe() {
            val view = weakRefView.get()
            val listener = weakRefListener.get()

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                view?.viewTreeObserver!!.removeOnGlobalLayoutListener(listener)
            } else {
                view?.viewTreeObserver!!.removeGlobalOnLayoutListener(listener)
            }

            weakRefView.clear()
            weakRefListener.clear()
        }

        fun isVisible(): Boolean {
            weakRefView.get()?.let { return instance.isVisible(it) }
            return false
        }
    }

    interface VisibilityEventListener {

        fun onVisibilityChanged(isOpen: Boolean)
    }

    companion object {
        private const val KEYBOARD_VISIBLE_THRESHOLD: Float = 150f

        val instance: KeyboardController by lazy { Holder.INSTANCE }
    }
}