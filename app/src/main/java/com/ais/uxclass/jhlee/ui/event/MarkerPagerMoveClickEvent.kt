package com.ais.uxclass.jhlee.ui.event

data class MarkerPagerMoveClickEvent(val pageType: Int)