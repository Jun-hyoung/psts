package com.ais.uxclass.jhlee.app.assessor

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewPager
import android.view.View
import com.ais.uxclass.jhlee.R
import com.ais.uxclass.jhlee.mvp.MvpFragmentView
import com.ais.uxclass.jhlee.ui.event.CleanUpPagerViewEvent
import com.ais.uxclass.jhlee.ui.event.MarkerPagerMoveClickEvent
import com.ais.uxclass.jhlee.ui.event.RxEventManager
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_marker_pager.*

class MarkerPagerFragmentView : MvpFragmentView<MarkerPagerView, MarkerPagerPresenter<MarkerPagerView>>(),
        MarkerPagerView {

    private var disposables: CompositeDisposable? = null


    //----------------------------------------------------------------------------------------------
    // overrides

    override fun getLayoutResourceId() = R.layout.fragment_marker_pager

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpViews()
        setUpEventListener()
    }

    override fun onDestroy() {
        super.onDestroy()
        releaseEventListener()
    }


    //----------------------------------------------------------------------------------------------
    // implements: MvpFragmentView

    override fun onCreatePresenter() = MarkerPagerPresenter<MarkerPagerView>()


    //----------------------------------------------------------------------------------------------
    // methods

    private fun setUpViews() {
        pager.offscreenPageLimit = PAGE_COUNT
        pager.adapter = MarkerPagerAdapter(childFragmentManager)
        pager.addOnPageChangeListener(object: ViewPager.SimpleOnPageChangeListener() {

            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)

                when (position) {
                    0 -> indicator.selectedColor = ContextCompat.getColor(context!!, R.color.indicator_color1)
                    1 -> indicator.selectedColor = ContextCompat.getColor(context!!, R.color.indicator_color2)
                    2 -> indicator.selectedColor = ContextCompat.getColor(context!!, R.color.indicator_color3)
                }
            }
        })

        indicator.setViewPager(pager)
    }

    private fun releaseEventListener() {
        if (disposables != null && !disposables!!.isDisposed) {
            disposables!!.dispose()
        }
    }

    private fun setUpEventListener() {
        releaseEventListener()

        disposables = CompositeDisposable()

        // profile view's start button clicked.
        disposables!!.add(RxEventManager.instance.subscribe(MarkerPagerMoveClickEvent::class.java) {
            pager.setCurrentItem(it.pageType, true)
        }!!)

        // profile view's start button clicked.
        disposables!!.add(RxEventManager.instance.subscribe(CleanUpPagerViewEvent::class.java) {
            pager.setCurrentItem(0, true)
        }!!)
    }


    private class MarkerPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {

        private val creators
                = arrayOf(PageCreator(PAGE_CONTENT), PageCreator(PAGE_LANGUAGE), PageCreator(PAGE_TECHNICAL))

        override fun getCount(): Int = creators.size

        override fun getItem(position: Int): Fragment = creators[position].create()

        override fun getItemPosition(`object`: Any): Int = POSITION_NONE
    }


    companion object {
        fun create() = MarkerPagerFragmentView()

        // TODO enumeration
        const val PAGE_CONTENT: Int = 0
        const val PAGE_LANGUAGE: Int = 1
        const val PAGE_TECHNICAL: Int = 2
        const val PAGE_COUNT: Int = 3

        class PageCreator(private val pageType: Int) {

            fun create(): Fragment = when (pageType) {
                PAGE_CONTENT -> ContentPageFragmentView.create()
                PAGE_LANGUAGE -> LanguagePageFragmentView.create()
                PAGE_TECHNICAL -> TechnicalPageFragmentView.create()
                else -> throw IndexOutOfBoundsException()
            }
        }
    }

}