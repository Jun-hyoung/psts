package com.ais.uxclass.jhlee.app.assessor

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ais.uxclass.jhlee.R
import com.ais.uxclass.jhlee.app.assessor.domain.MarkSetManager
import com.ais.uxclass.jhlee.base.AndroidContext
import com.ais.uxclass.jhlee.mvp.MvpFragmentView
import com.ais.uxclass.jhlee.ui.event.CleanUpPagerViewEvent
import com.ais.uxclass.jhlee.ui.event.MarkerPagerMoveClickEvent
import com.ais.uxclass.jhlee.ui.event.RxEventManager
import com.willy.ratingbar.BaseRatingBar
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_content_page.*
import kotlinx.android.synthetic.main.layout_score_description.view.*
import kotlinx.android.synthetic.main.pager_child_item_layout.view.*


class ContentPageFragmentView : MvpFragmentView<ContentPageView, ContentPagePresenter<ContentPageView>>(),
        ContentPageView {

    private lateinit var markSetManager: MarkSetManager

    private lateinit var category: String

    private var nextViewEnabled: Boolean = false
        set (value) {
            nextView.isEnabled = value
        }

    private var disposables: CompositeDisposable? = null

    private val handler = Handler()

    private val nextViewRunner = Runnable { childPagerView.currentItem = childPagerView.currentItem + 1 }

    private val nextPageRunner = Runnable { RxEventManager.instance.post(MarkerPagerMoveClickEvent(MarkerPagerFragmentView.PAGE_LANGUAGE)) }


    //----------------------------------------------------------------------------------------------
    // overrides

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        markSetManager = context as MarkSetManager
        category = AndroidContext.string(R.string.assessor_category_content_title)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setUpEventListener()
    }

    override fun getLayoutResourceId() = R.layout.fragment_content_page

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpViews()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        handler.removeCallbacks(nextViewRunner)
    }

    override fun onDestroy() {
        super.onDestroy()

        releaseEventListener()
    }


    //----------------------------------------------------------------------------------------------
    // implements: MvpFragmentView

    override fun onCreatePresenter() = ContentPagePresenter<ContentPageView>()


    //----------------------------------------------------------------------------------------------
    // methods

    private fun setUpViews() {
        childPagerView.adapter = PagerChildAdapter()

        nextView.setOnClickListener {
            RxEventManager.instance.post(MarkerPagerMoveClickEvent(MarkerPagerFragmentView.PAGE_LANGUAGE))
        }
    }

    private fun releaseEventListener() {
        if (disposables != null && !disposables!!.isDisposed) {
            disposables!!.dispose()
        }
    }

    private fun setUpEventListener() {
        releaseEventListener()

        disposables = CompositeDisposable()

        // profile view's start button clicked.
        disposables!!.add(RxEventManager.instance.subscribe(CleanUpPagerViewEvent::class.java) {
            clearViews()
        }!!)
    }

    private fun clearViews() {
        childPagerView.currentItem = 0
        childPagerView.adapter?.notifyDataSetChanged()
    }

    private fun getPageName(pageType: Int): String = when (pageType) {
        PAGE_TYPE_FOCUS -> AndroidContext.string(R.string.assessor_content_focus)
        PAGE_TYPE_ORGANIZATION -> AndroidContext.string(R.string.assessor_content_organization)
        PAGE_TYPE_VISUAL_AIDS -> AndroidContext.string(R.string.assessor_content_visual_aids)
        PAGE_TYPE_QA -> AndroidContext.string(R.string.assessor_content_qa)
        else -> {
            throw IndexOutOfBoundsException()
        }
    }


    private inner class PagerChildAdapter : PagerAdapter() {

        override fun getCount(): Int {
            return PAGE_TYPE_COUNT
        }

        override fun isViewFromObject(view: View, any: Any): Boolean {
            return view == any
        }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val view = LayoutInflater.from(container.context)
                    .inflate(R.layout.pager_child_item_layout, container, false)

            when (position) {
                PAGE_TYPE_FOCUS -> {
                    view.itemTitleView.setText(R.string.assessor_content_focus)
                    view.score4ContentView.setText(R.string.assessor_content_focus_excellent)
                    view.score3ContentView.setText(R.string.assessor_content_focus_good)
                    view.score2ContentView.setText(R.string.assessor_content_focus_fair)
                    view.score1ContentView.setText(R.string.assessor_content_focus_poor)
                    initScoreView(PAGE_TYPE_FOCUS, view.itemScoreView)
                }
                PAGE_TYPE_ORGANIZATION -> {
                    view.itemTitleView.setText(R.string.assessor_content_organization)
                    view.score4ContentView.setText(R.string.assessor_content_organization_excellent)
                    view.score3ContentView.setText(R.string.assessor_content_organization_good)
                    view.score2ContentView.setText(R.string.assessor_content_organization_fair)
                    view.score1ContentView.setText(R.string.assessor_content_organization_poor)
                    initScoreView(PAGE_TYPE_ORGANIZATION, view.itemScoreView)
                }
                PAGE_TYPE_VISUAL_AIDS -> {
                    view.itemTitleView.setText(R.string.assessor_content_visual_aids)
                    view.score4ContentView.setText(R.string.assessor_content_visual_aids_excellent)
                    view.score3ContentView.setText(R.string.assessor_content_visual_aids_good)
                    view.score2ContentView.setText(R.string.assessor_content_visual_aids_fair)
                    view.score1ContentView.setText(R.string.assessor_content_visual_aids_poor)
                    initScoreView(PAGE_TYPE_VISUAL_AIDS, view.itemScoreView)
                }
                PAGE_TYPE_QA -> {
                    view.itemTitleView.setText(R.string.assessor_content_qa)
                    view.score4ContentView.setText(R.string.assessor_content_qa_excellent)
                    view.score3ContentView.setText(R.string.assessor_content_qa_good)
                    view.score2ContentView.setText(R.string.assessor_content_qa_fair)
                    view.score1ContentView.setText(R.string.assessor_content_qa_poor)
                    initScoreView(PAGE_TYPE_QA, view.itemScoreView)
                }
            }

            container.addView(view)
            return view
        }

        override fun getItemPosition(`object`: Any): Int {
            return PagerAdapter.POSITION_NONE
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            val view: View = `object` as View
            container.removeView(view)

            view.itemScoreView.setOnRatingChangeListener(null)
        }

        private fun initScoreView(pageType: Int, scoreView: BaseRatingBar) {
            scoreView.setOnRatingChangeListener { view, rating ->
                markScoreView(pageType, view, rating)
            }

            markSetManager.getMark(category, getPageName(pageType))?.let {
                scoreView.rating = it.toFloat()
            }
        }

        private fun markScoreView(pageType: Int, scoreView: BaseRatingBar, rating: Float) {
            val name = getPageName(pageType)

            if (markSetManager.getMark(category, name) != null) {
                if (rating == 0.0f) {
                    markSetManager.removeMark(category, name)
                } else {
                    markSetManager.putMark(category, name, rating.toInt())
                }
            } else if (rating > 0) {
                markSetManager.putMark(category, name, rating.toInt())

                if (markSetManager.getMarkSet(category).marks.size != PAGE_TYPE_COUNT) {
                    handler.postDelayed(nextViewRunner, 300)
                } else {
                    // if it's last page?
                    handler.postDelayed(nextPageRunner, 450)
                }
            }

            nextViewEnabled = (markSetManager.getMarkSet(category).marks.size == PAGE_TYPE_COUNT)
        }
    }


    companion object {
        fun create() = ContentPageFragmentView()

        const val PAGE_TYPE_FOCUS: Int = 0
        const val PAGE_TYPE_ORGANIZATION: Int = 1
        const val PAGE_TYPE_VISUAL_AIDS: Int = 2
        const val PAGE_TYPE_QA: Int = 3
        const val PAGE_TYPE_COUNT: Int = 4
    }

}