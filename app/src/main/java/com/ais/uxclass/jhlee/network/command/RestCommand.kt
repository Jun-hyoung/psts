package com.ais.uxclass.jhlee.network.command

import io.reactivex.Observable

abstract class RestCommand {
    abstract fun build(): Observable<*>
}