package com.ais.uxclass.jhlee.app.assessor

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.ais.uxclass.jhlee.R
import com.ais.uxclass.jhlee.app.assessor.domain.model.ProfileSet
import com.ais.uxclass.jhlee.mvp.MvpFragmentView
import com.ais.uxclass.jhlee.ui.event.CleanUpPagerViewEvent
import com.ais.uxclass.jhlee.ui.event.ProfileStartViewClickEvent
import com.ais.uxclass.jhlee.ui.event.RxEventManager
import com.ais.uxclass.jhlee.utils.KeyboardController
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_profile.*

/**
 * Date 2018. 9. 22.
 * Author Jun-Hyoung, Lee (niceguy0315@hotmail.com)
 */
class ProfileFragmentView : MvpFragmentView<ProfileView, ProfilePresenter<ProfileView>>(),
        ProfileView {

    private var disposables: CompositeDisposable? = null


    //----------------------------------------------------------------------------------------------
    // overrides

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setUpEventListener()
    }

    override fun getLayoutResourceId() = R.layout.fragment_profile

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpViews()
    }

    override fun onDestroy() {
        super.onDestroy()

        releaseEventListener()
    }


    //----------------------------------------------------------------------------------------------
    // implements: MvpFragmentView

    override fun onCreatePresenter() = ProfilePresenter<ProfileView>()


    //----------------------------------------------------------------------------------------------
    // methods

    private fun setUpViews() {

        nameView.addTextChangedListener(object : TextWatcher {

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                validate()
            }

            override fun afterTextChanged(s: Editable?) {

            }
        })

        idView.addTextChangedListener(object : TextWatcher {

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                validate()
            }

            override fun afterTextChanged(s: Editable?) {

            }
        })

        semesterView.addTextChangedListener(object : TextWatcher {

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                validate()
            }

            override fun afterTextChanged(s: Editable?) {

            }
        })

        topicView.addTextChangedListener(object : TextWatcher {

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                validate()
            }

            override fun afterTextChanged(s: Editable?) {

            }
        })

        startView.setOnClickListener {
            doAssess()
        }

        rootContainer.setOnClickListener { _ ->
            activity?.currentFocus?.let {
                KeyboardController.instance.hide(it)
            }
        }
    }

    private fun releaseEventListener() {
        if (disposables != null && !disposables!!.isDisposed) {
            disposables!!.dispose()
        }
    }

    private fun setUpEventListener() {
        releaseEventListener()

        disposables = CompositeDisposable()

        // profile view's start button clicked.
        disposables!!.add(RxEventManager.instance.subscribe(CleanUpPagerViewEvent::class.java) {
            clearViews()
        }!!)
    }

    private fun doAssess() {
        if (validate()) {
            RxEventManager.instance.post(ProfileStartViewClickEvent(ProfileSet(
                    nameView.text!!.toString(),
                    idView.text!!.toString(),
                    semesterView.text!!.toString(),
                    topicView.text!!.toString()
            )))
        }
    }

    private fun validate(): Boolean {
        val isValid = nameView.text!!.isNotEmpty() && idView.text!!.isNotEmpty()
                && semesterView.text!!.isNotEmpty() && topicView.text!!.isNotEmpty()

        startView.isEnabled = isValid
        return isValid
    }

    private fun clearViews() {
        nameView.requestFocus()
        nameView.text?.clear()
        idView.text?.clear()
        semesterView.text?.clear()
        topicView.text?.clear()
        validate()
    }


    companion object {
        fun create() = ProfileFragmentView()
    }
}