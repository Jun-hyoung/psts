package com.ais.uxclass.jhlee.app.assessor.domain.model

data class Marks(val category: String, val marks: HashMap<String, Int> = HashMap())