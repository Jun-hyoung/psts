package com.ais.uxclass.jhlee.app.assessor.domain

import android.os.Environment
import android.util.Log
import com.ais.uxclass.jhlee.AppCache
import com.ais.uxclass.jhlee.app.assessor.domain.model.MarksAssessment
import com.ais.uxclass.jhlee.app.assessor.domain.model.ProfileSet
import com.ais.uxclass.jhlee.base.AndroidContext
import io.reactivex.BackpressureStrategy
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import org.docx4j.openpackaging.packages.WordprocessingMLPackage
import org.docx4j.openpackaging.parts.WordprocessingML.MainDocumentPart
import java.io.File
import java.util.*
import java.util.concurrent.Callable


class WordFileManager private constructor(){

    private object Holder { val INSTANCE = WordFileManager() }

    public var isReady: Boolean = false
        private set(value) {
            field = value
        }

    private var isWorking: Boolean = false

    private lateinit var wordPackage: WordprocessingMLPackage

    private var workers: MutableList<Runnable> = LinkedList()


    fun build(assessment: MarksAssessment): File {
        val folder = File(AndroidContext.application().getExternalFilesDir(null), AppCache.DOCX_FOLDER_PATH)
        if (!folder.exists()) {
            folder.mkdirs()
        }

//        val mainDocumentPart = wordPackage.mainDocumentPart
//        mainDocumentPart.addStyledParagraphOfText("Title", "Hello World!")
//        mainDocumentPart.addParagraphOfText("Welcome To Baeldung")

//        val wmlObjectFactory = Context.getWmlObjectFactory()
//        val paragraph = wmlObjectFactory.createP()
//        val run = wmlObjectFactory.createR()
//        val text = wmlObjectFactory.createText()
//        text.value = "Welcome To Baeldung"
//        run.content.add(text)
//        paragraph.content.add(run)

        // tables
//        val writableWidthTwips = wordPackage.documentModel.sections[0].pageDimensions.writableWidthTwips
//        val columnNumber = 3
//        val tbl = TblFactory.createTable(3, 3, writableWidthTwips / columnNumber)
//        val rows = tbl.content
//        for (row in rows) {
//            val tr = row as Tr
//            val cells = tr.content
//            for (cell in cells) {
//                val td = cell as Tc
//                td.content.add(paragraph)
//            }
//        }

//        val tbl = wmlObjectFactory.createTbl()
//        val tblWrapped = wmlObjectFactory.createCTFtnEdnTbl(tbl)
//
//        // create table rows
//        val tr = wmlObjectFactory.createTr()
//        tbl.content.add(tr)
//        // create table columns (wrapped in JAXBElement)
//        for (i in 1..3) {
//            val tc = wmlObjectFactory.createTc()
//            tc.content.add(paragraph)
//
//            val tcWrapped = wmlObjectFactory.createTrTc(tc)
//            tr.content.add(tcWrapped)
//        }


        val creator = AssessorWordCreator()

        val wordDocumentPart = MainDocumentPart()
        wordDocumentPart.contents = creator.create(assessment)
        wordPackage.addTargetPart(wordDocumentPart)

        val exportFile = File(folder, extractWordFileName(assessment.profile!!))
        wordPackage.save(exportFile)

        return exportFile
    }

    fun initialize() {
        Log.d("darby", "Initializing word press package...")

        if (isReady || isWorking) {
            return
        }

        isWorking = true

        createSetUpJob()
                .toFlowable(BackpressureStrategy.BUFFER)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { wordPackage -> onSetUpSuccess(wordPackage) },
                        { throwable -> onSetUpError(throwable) },
                        { onSetUpComplete() }
                )
    }

    private fun createSetUpJob(): Observable<WordprocessingMLPackage> {
        /*
         * Observable.generate<T, S>
         *     T: the type of emitter's parameter
         *     S: the type of Callable function
         */
        return Observable.generate<WordprocessingMLPackage, WordprocessingMLPackage>(
                Callable { WordprocessingMLPackage.createPackage() },
                BiFunction { wordPackage, emitter ->
                    emitter.onNext(wordPackage)
                    emitter.onComplete()
                    return@BiFunction wordPackage
                }
        )
    }

    private fun onSetUpSuccess(wordPackage: WordprocessingMLPackage) {
        this.isReady = true
        this.wordPackage = wordPackage
        Log.d("darby", "Word press package success")
    }

    private fun onSetUpError(throwable: Throwable) {
        Log.d("darby", "Word press package error", throwable)
    }

    private fun onSetUpComplete() {
        isWorking = false

        while (!workers.isEmpty()) {
            workers.removeAt(0).run()
        }

        Log.d("darby", "Word press package initializing complete")
    }

    fun postJob(job: Runnable) {
        if (!isReady) {
            workers.add(job)

        } else {
            job.run()
        }
    }

    private fun extractWordFileName(profile: ProfileSet): String {

        return "presentation_report_${profile.id}.docx"
    }


    companion object {

        val instance: WordFileManager by lazy { Holder.INSTANCE }

        /* Checks if external storage is available for read and write */
        fun isExternalStorageWritable(): Boolean {
            return Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED
        }

        /* Checks if external storage is available to at least read */
        fun isExternalStorageReadable(): Boolean {
            return Environment.getExternalStorageState() in
                    setOf(Environment.MEDIA_MOUNTED, Environment.MEDIA_MOUNTED_READ_ONLY)
        }
    }
}