package com.ais.uxclass.jhlee.app.assessor

import android.os.Bundle
import android.support.transition.TransitionManager
import android.view.View
import com.ais.uxclass.jhlee.R
import com.ais.uxclass.jhlee.app.assessor.domain.MarkSetManager
import com.ais.uxclass.jhlee.app.assessor.domain.model.Marks
import com.ais.uxclass.jhlee.app.assessor.domain.model.MarksAssessment
import com.ais.uxclass.jhlee.app.assessor.domain.model.ProfileSet
import com.ais.uxclass.jhlee.mvp.BasePresenter
import com.ais.uxclass.jhlee.mvp.BaseView
import com.ais.uxclass.jhlee.mvp.MvpActivityView
import com.ais.uxclass.jhlee.ui.event.CleanUpPagerViewEvent
import com.ais.uxclass.jhlee.ui.event.MarkerPagerFinishClickEvent
import com.ais.uxclass.jhlee.ui.event.ProfileStartViewClickEvent
import com.ais.uxclass.jhlee.ui.event.RxEventManager
import com.ais.uxclass.jhlee.utils.KeyboardController
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_assessor.*


/**
 * Date 2018. 9. 22.
 * Author Jun-Hyoung, Lee (niceguy0315@hotmail.com)
 */
class AssessorActivityView : MvpActivityView<BaseView, BasePresenter<BaseView>>(),
        MarkSetManager {

    private var disposables: CompositeDisposable? = null

    private var profile: ProfileSet? = null
    private var markSets: HashMap<String, Marks> = HashMap()

    private lateinit var profileView: ProfileFragmentView
    private lateinit var markerPagerView: MarkerPagerFragmentView
    private lateinit var finishView: FinishFragmentView


    //----------------------------------------------------------------------------------------------
    // overrides

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_assessor)

        setUpMarkSet()
        setUpViews()
        setUpEventListener()

        if (savedInstanceState == null) {
            profileView = ProfileFragmentView.create()
            markerPagerView = MarkerPagerFragmentView.create()
            finishView = FinishFragmentView.create()

            supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, profileView)
                    .replace(R.id.page_fragment_container, markerPagerView)
                    .replace(R.id.finish_fragment_container, finishView)
                    .commit()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        releaseEventListener()
    }


    //----------------------------------------------------------------------------------------------
    // implements: MvpActivityView

    override fun onCreatePresenter() = BasePresenter<BaseView>()


    //----------------------------------------------------------------------------------------------
    // implements: MarkSetManager

    override fun clearProfile() {
        this.profile = null
    }

    override fun putProfile(profile: ProfileSet) {
        this.profile = profile
    }

    override fun getProfile(): ProfileSet = this.profile!!

    override fun clearMarker() {
        setUpMarkSet()
    }

    override fun putMark(category: String, name: String, mark: Int) {
        markSets[category]!!.marks[name] = mark
    }

    override fun getMark(category: String, name: String): Int? = markSets[category]!!.marks[name]

    override fun removeMark(category: String, name: String): Int?
            = markSets[category]!!.marks.remove(name)

    override fun getMarkSet(category: String): Marks = markSets[category]!!

    override fun getMarkSet(): HashMap<String, Marks> = markSets

    override fun showProfileView() {
        clearProfile()
        clearMarker()

        RxEventManager.instance.post(CleanUpPagerViewEvent())

        TransitionManager.beginDelayedTransition(rootContainer)
        fragment_container.visibility = View.VISIBLE
        page_fragment_container.visibility = View.GONE
        finish_fragment_container.visibility = View.GONE
    }


    //----------------------------------------------------------------------------------------------
    // methods

    private fun setUpMarkSet() {
        markSets.clear()

        val content = getString(R.string.assessor_category_content_title)
        markSets[content] = Marks(content)

        val language = getString(R.string.assessor_category_language_title)
        markSets[language] = Marks(language)

        val technical = getString(R.string.assessor_category_technical_title)
        markSets[technical] = Marks(technical)
    }

    private fun setUpViews() {

    }

    private fun releaseEventListener() {
        if (disposables != null && !disposables!!.isDisposed) {
            disposables!!.dispose()
        }
    }

    private fun setUpEventListener() {
        releaseEventListener()

        disposables = CompositeDisposable()

        // profile view's start button clicked.
        disposables!!.add(RxEventManager.instance.subscribe(ProfileStartViewClickEvent::class.java) {
            putProfile(it.profile)

            showMarkerPagerView()
        }!!)

        // marker pager's finish button clicked.
        disposables!!.add(RxEventManager.instance.subscribe(MarkerPagerFinishClickEvent::class.java) {
            showFinishView()
        }!!)
    }

    private fun showMarkerPagerView() {
        currentFocus?.let {
            KeyboardController.instance.hide(it)
        }

        TransitionManager.beginDelayedTransition(rootContainer)
        fragment_container.visibility = View.GONE
        finish_fragment_container.visibility = View.GONE
        page_fragment_container.visibility = View.VISIBLE
    }

    private fun showFinishView() {
        TransitionManager.beginDelayedTransition(rootContainer)
        fragment_container.visibility = View.GONE
        page_fragment_container.visibility = View.GONE
        finish_fragment_container.visibility = View.VISIBLE
    }


    companion object {
        const val ASSESSOR_PROFILE_TITLE = "profile"
    }

}
