package com.ais.uxclass.jhlee.app.assessor.domain

import com.ais.uxclass.jhlee.app.assessor.domain.model.Marks
import com.ais.uxclass.jhlee.app.assessor.domain.model.ProfileSet

interface MarkSetManager {

    fun clearProfile()

    fun putProfile(profile: ProfileSet)

    fun getProfile(): ProfileSet

    fun clearMarker()

    fun putMark(category: String, name: String, mark: Int)

    fun getMark(category: String, name: String): Int?

    fun removeMark(category: String, name: String): Int?

    fun getMarkSet(category: String): Marks

    fun getMarkSet(): HashMap<String, Marks>

    fun showProfileView()
}