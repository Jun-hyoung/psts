package com.ais.uxclass.jhlee.mvp

import android.os.Bundle
import com.ais.uxclass.jhlee.MyApplication
import com.ais.uxclass.jhlee.base.BaseActivity
import com.ais.uxclass.jhlee.dagger.components.AppComponent

abstract class MvpActivityView<V : MvpView, P : BasePresenter<V>> : BaseActivity(), MvpView {

    protected var presenter: P? = null

    protected val appComponent: AppComponent
        get() = (application as MyApplication).appComponent

    @Suppress("UNCHECKED_CAST")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter = onCreatePresenter()
        presenter?.attachView(this as V)
    }

    override fun onDestroy() {
        super.onDestroy()
        this.presenter?.detachView()
    }

    abstract fun onCreatePresenter(): P

    override fun onSetPresenter(presenter: MvpPresenter<*>) {
        @Suppress("UNCHECKED_CAST")
        this.presenter = presenter as P
    }
}
