package com.ais.uxclass.jhlee.dagger.components

import android.app.Application

import javax.inject.Singleton

import dagger.Component
import com.ais.uxclass.jhlee.dagger.modules.AppModule
import com.ais.uxclass.jhlee.base.Navigator

@Singleton
@Component(modules = [(AppModule::class)])
interface AppComponent {

    fun inject(application: Application)

    fun application(): Application

    fun navigator(): Navigator

}
