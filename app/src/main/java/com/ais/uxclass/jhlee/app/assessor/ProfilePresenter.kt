package com.ais.uxclass.jhlee.app.assessor

import com.ais.uxclass.jhlee.mvp.BasePresenter

/**
 * Date 2018. 9. 22.
 * Author Jun-Hyoung, Lee (niceguy0315@hotmail.com)
 */
class ProfilePresenter<VIEW : ProfileView> : BasePresenter<VIEW>() {

    override fun attachView(view: VIEW) {
        super.attachView(view)
    }

    override fun detachView() {
        super.detachView()
    }
}