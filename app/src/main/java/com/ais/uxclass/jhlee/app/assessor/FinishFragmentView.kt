package com.ais.uxclass.jhlee.app.assessor

import android.Manifest
import android.app.Activity
import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.View
import com.ais.uxclass.jhlee.R
import com.ais.uxclass.jhlee.app.assessor.domain.MarkSetManager
import com.ais.uxclass.jhlee.app.assessor.domain.WordFileManager
import com.ais.uxclass.jhlee.app.assessor.domain.model.MarksAssessment
import com.ais.uxclass.jhlee.mvp.BaseView
import com.ais.uxclass.jhlee.mvp.MvpFragmentView
import com.ais.uxclass.jhlee.utils.RuntimePermissionChecker
import kotlinx.android.synthetic.main.fragment_finish.*
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.AppSettingsDialog
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.support.v4.content.ContextCompat
import android.text.Editable
import android.text.TextWatcher
import android.widget.TextView
import android.widget.Toast
import com.ais.uxclass.jhlee.ui.event.MarkerPagerFinishClickEvent
import com.ais.uxclass.jhlee.ui.event.ProfileStartViewClickEvent
import com.ais.uxclass.jhlee.ui.event.RxEventManager
import com.ais.uxclass.jhlee.ui.view.GraphMarkerView
import com.ais.uxclass.jhlee.utils.KeyboardController
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.formatter.LargeValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import io.reactivex.disposables.CompositeDisposable
import java.io.File
import java.util.ArrayList


class FinishFragmentView : MvpFragmentView<BaseView, FinishPresenter<BaseView>>(),
        BaseView,
        EasyPermissions.PermissionCallbacks,
        OnChartValueSelectedListener {

    private lateinit var activity: Activity
    private lateinit var markSetManager: MarkSetManager

    private var disposables: CompositeDisposable? = null

    private var isTaskWorking: Boolean = false
        set(value) {
            waitView.visibility = if (value) View.VISIBLE else View.GONE
        }

    //----------------------------------------------------------------------------------------------
    // overrides

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        activity = context as Activity
        markSetManager = context as MarkSetManager
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setUpEventListener()
    }

    override fun getLayoutResourceId() = R.layout.fragment_finish

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpViews()
    }

    override fun onDestroy() {
        super.onDestroy()
        releaseEventListener()
    }

    //----------------------------------------------------------------------------------------------
    // implements: EasyPermissions.PermissionCallbacks

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this)
    }

    override fun onPermissionsDenied(requestCode: Int, perms: MutableList<String>) {
        // (Optional) Check whether the user denied any permissions and checked "NEVER ASK AGAIN."
        // This will display a dialog directing them to enable the permission in app settings.
        if (EasyPermissions.somePermissionPermanentlyDenied(this, perms)) {
            AppSettingsDialog.Builder(this).build().show()
        }
    }

    override fun onPermissionsGranted(requestCode: Int, perms: MutableList<String>) {

    }


    //----------------------------------------------------------------------------------------------
    // implements: MvpFragmentView

    override fun onCreatePresenter(): FinishPresenter<BaseView> = FinishPresenter()


    //----------------------------------------------------------------------------------------------
    // implements: OnChartValueSelectedListener

    override fun onValueSelected(e: Entry?, h: Highlight?) {

    }

    override fun onNothingSelected() {

    }


    //----------------------------------------------------------------------------------------------
    // methods

    private fun setUpViews() {
        finishRootViewLayout.setOnClickListener {
            activity.currentFocus?.let {
                KeyboardController.instance.hide(it)
            }
        }
        resetView.setOnClickListener {
            markSetManager.showProfileView()
        }

        mailToView.setOnClickListener {
            saveMarksAsWordFile(MarksAssessment(markSetManager.getProfile(), markSetManager.getMarkSet()))
        }

        toView.addTextChangedListener(object : SimpleTextWatcher() {

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                validate()
            }
        })

        subjectView.addTextChangedListener(object : SimpleTextWatcher() {

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                validate()
            }
        })

        val prefs: SharedPreferences? = context?.getSharedPreferences(PREFS_MAIL_FORM, 0)
        rememberToView.isChecked = prefs!!.getBoolean(PREFS_MAIL_REMEMBER, true)
        rememberToView.setOnCheckedChangeListener { _, isChecked ->

            val editor: SharedPreferences.Editor = context!!.getSharedPreferences(PREFS_MAIL_FORM, 0)!!.edit()
            if (isChecked) {
                saveMailForm(editor)

            } else {
                deleteMailForm(editor)
            }

            // Saved whether saving id checked
            editor.putBoolean(PREFS_MAIL_REMEMBER, isChecked)
            editor.apply()
        }

        if (rememberToView.isChecked) {
            toView.setText(getMailForm(), TextView.BufferType.EDITABLE)
        }

        // set up chart
        chartView.setOnChartValueSelectedListener(this)
        chartView.description.isEnabled = false
        chartView.isScaleXEnabled = false
        chartView.isScaleYEnabled = false
        chartView.setPinchZoom(false)
        chartView.setDrawBarShadow(false)
        chartView.setDrawGridBackground(false)
        chartView.setDrawValueAboveBar(false)

        val markerView = GraphMarkerView(context, R.layout.graph_marker_layout)
        markerView.chartView = chartView
        chartView.marker = markerView

        // legend
        chartView.legend.isEnabled = false
//        val l = chartView.legend
//        l.verticalAlignment = Legend.LegendVerticalAlignment.TOP
//        l.horizontalAlignment = Legend.LegendHorizontalAlignment.RIGHT
//        l.orientation = Legend.LegendOrientation.VERTICAL
//        l.setDrawInside(true)
//        //l.setTypeface(mTfLight)
//        l.yOffset = 0f
//        l.xOffset = 0f
//        l.yEntrySpace = 0.5f
//        l.textSize = 8f

        // x axis
        val xAxis = chartView.xAxis
        //xAxis.typeface = mTfLight
        xAxis.granularity = 1f
        xAxis.valueFormatter = IAxisValueFormatter { value, axis -> value.toInt().toString() }
        xAxis.setCenterAxisLabels(true)
        xAxis.setDrawLabels(false)

        // y axis
        val yAxis = chartView.axisLeft
        //yAxis.setTypeface(mTfLight)
        yAxis.axisMaximum = 5f
        yAxis.axisMinimum = 0f
        yAxis.granularity = 1f
        yAxis.spaceTop = 35f
        yAxis.setLabelCount(5, false)
        yAxis.setDrawGridLines(false)

        chartView.axisRight.isEnabled = false
    }

    private fun setUpData() {
        val content = markSetManager.getMarkSet(getString(R.string.assessor_category_content_title))
        val focus = content.marks[getString(R.string.assessor_content_focus)]
        val organization = content.marks[getString(R.string.assessor_content_organization)]
        val visualAids = content.marks[getString(R.string.assessor_content_visual_aids)]
        val qa = content.marks[getString(R.string.assessor_content_qa)]

        val language = markSetManager.getMarkSet(getString(R.string.assessor_category_language_title))
        val eyeContact = language.marks[getString(R.string.assessor_language_eye_contact)]
        val enthusiasm = language.marks[getString(R.string.assessor_language_enthusiasm)]
        val elocution = language.marks[getString(R.string.assessor_language_elocution)]

        val technical = markSetManager.getMarkSet(getString(R.string.assessor_category_technical_title))
        val knowledge = technical.marks[getString(R.string.assessor_technical_knowledge)]
        val research = technical.marks[getString(R.string.assessor_technical_research)]
        val discussion = technical.marks[getString(R.string.assessor_technical_discussion_of_new_idea)]
        val argument = technical.marks[getString(R.string.assessor_technical_argument)]
        val questions = technical.marks[getString(R.string.assessor_technical_questions)]

        val contentSets: MutableList<Int> = ArrayList()
        val languageSets: MutableList<Int> = ArrayList()
        contentSets.add(focus!!)
        val technicalSets: MutableList<Int> = ArrayList()
        contentSets.add(organization!!)
        contentSets.add(visualAids!!)
        contentSets.add(qa!!)
        languageSets.add(eyeContact!!)
        languageSets.add(enthusiasm!!)
        languageSets.add(elocution!!)
        technicalSets.add(knowledge!!)
        technicalSets.add(research!!)
        technicalSets.add(discussion!!)
        technicalSets.add(argument!!)
        technicalSets.add(questions!!)

        var start = 1f
        val dataSets: MutableList<IBarDataSet> = ArrayList()

        // content skills
        val contents: MutableList<BarEntry> = ArrayList()
        for (item in contentSets) {
            contents.add(BarEntry(start++, item.toFloat()))
        }
        dataSets.add(setUpBarGraphColors(BarDataSet(contents, getString(R.string.assessor_category_content_title))))

        // language skills
        val languages: MutableList<BarEntry> = ArrayList()
        for (item in languageSets) {
            languages.add(BarEntry(start++, item.toFloat()))
        }
        dataSets.add(setUpBarGraphColors(BarDataSet(languages, getString(R.string.assessor_category_language_title))))

        // technical skills
        val technicals: MutableList<BarEntry> = ArrayList()
        for (item in technicalSets) {
            technicals.add(BarEntry(start++, item.toFloat()))
        }
        dataSets.add(setUpBarGraphColors(BarDataSet(technicals, getString(R.string.assessor_category_technical_title))))

        val data = BarData(dataSets)
        data.setValueTextSize(10f)
        //data.setValueTypeface(mTfLight)
        data.barWidth = 0.5f

        chartView.data = data
    }

    private fun setUpBarGraphColors(dataSet: BarDataSet): BarDataSet {
        val colors: MutableList<Int> = ArrayList()
        colors.add(ContextCompat.getColor(context!!, android.R.color.holo_orange_light))
        colors.add(ContextCompat.getColor(context!!, android.R.color.holo_blue_light))
        colors.add(ContextCompat.getColor(context!!, android.R.color.holo_green_light))
        colors.add(ContextCompat.getColor(context!!, android.R.color.holo_red_light))
        colors.add(ContextCompat.getColor(context!!, android.R.color.holo_purple))

        dataSet.colors = colors
        return dataSet
    }

    private fun clearViews() {
        toView.text?.clear()
        subjectView.text?.clear()
        validate()
    }

    private fun releaseEventListener() {
        if (disposables != null && !disposables!!.isDisposed) {
            disposables!!.dispose()
        }
    }

    private fun setUpEventListener() {
        releaseEventListener()

        disposables = CompositeDisposable()

        // marker pager's finish button clicked.
        disposables!!.add(RxEventManager.instance.subscribe(MarkerPagerFinishClickEvent::class.java) {
            setUpData()
        }!!)
    }

    private fun validate(): Boolean {
        val isValid = toView.text!!.isNotEmpty() && subjectView.text!!.isNotEmpty()
        mailToView.isEnabled = isValid
        return isValid
    }

    @AfterPermissionGranted(RuntimePermissionChecker.RC_PERMISSION_EXTERNAL_STORAGE)
    private fun saveMarksAsWordFile(assessment: MarksAssessment) {
        val permissions = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)

        if (EasyPermissions.hasPermissions(context!!, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            // todo copy following code to presenter

            if (WordFileManager.instance.isReady) {
                Log.d("darby", "saveMarksAsWordFile ready start")
                SaveMarksAsyncTask().execute(assessment)

            } else {
                Log.d("darby", "saveMarksAsWordFile word package waiting.")
                isTaskWorking = true

                // WordFileManager still initialize package.
                WordFileManager.instance.postJob(Runnable { SaveMarksAsyncTask().execute(assessment) })
            }

        } else {
            EasyPermissions.requestPermissions(
                    PermissionRequest.Builder(this, RuntimePermissionChecker.RC_PERMISSION_EXTERNAL_STORAGE, *permissions)
                            .setRationale(R.string.permission_rationale_external_storage)
                            .setPositiveButtonText(R.string.app_ok)
                            .setNegativeButtonText(R.string.app_cancel)
                            .build())
        }
    }

    private fun mailTo(recipients: String, subject: String, uri: Uri) {
        val email = Intent(Intent.ACTION_SEND)

        // The intent does not have a URI, so declare the "text/plain" MIME type
        email.type = "message/rfc822"
        email.putExtra(Intent.EXTRA_EMAIL, arrayOf(recipients)) // recipients
        email.putExtra(Intent.EXTRA_SUBJECT, subject)
        //email.putExtra(Intent.EXTRA_TEXT, "Email message")
        email.putExtra(Intent.EXTRA_STREAM, uri)
        // You can also attach multiple items by passing an ArrayList of Uris

        // Create intent to show chooser
        val chooser = Intent.createChooser(email, "Sending mail...")

        // Verify the intent will resolve to at least one activity
        if (email.resolveActivity(context!!.packageManager) != null) {
            startActivity(chooser)

            // save id if it's allowed
            if (rememberToView.isChecked) {
                saveMailForm(context!!.getSharedPreferences(PREFS_MAIL_FORM, 0)!!.edit())
            }

        } else {
            Toast.makeText(context!!.applicationContext,
                    "This device doesn't support email service. Please install email support application.",
                    Toast.LENGTH_SHORT)
                    .show()
        }
    }

    private fun deleteMailForm(editor: SharedPreferences.Editor) {
        editor.remove(PREFS_MAIL_ITEM_ID)
        editor.apply()
    }

    private fun saveMailForm(editor: SharedPreferences.Editor) {
        // Save id
        val id = toView.text.toString()
        if (id.isNotEmpty()) {
            editor.putString(PREFS_MAIL_ITEM_ID, id)
            editor.apply()
        }
    }

    private fun getMailForm(): String {
        val prefs: SharedPreferences? = context?.getSharedPreferences(PREFS_MAIL_FORM, 0)
        return prefs!!.getString(PREFS_MAIL_ITEM_ID, "")!!
    }


    inner class SaveMarksAsyncTask : AsyncTask<MarksAssessment, Unit, File>() {

        override fun doInBackground(vararg params: MarksAssessment): File {
            val assessment = params[0]

            return WordFileManager.instance.build(assessment)
        }

        override fun onPreExecute() {
            Log.d("darby", "Saving file started")
            isTaskWorking = true

        }

        override fun onPostExecute(result: File) {
            Log.d("darby", "Saving file finished")

            mailTo(toView.text.toString(), subjectView.text.toString(), Uri.fromFile(result))

            isTaskWorking= false
        }
    }


    private abstract class SimpleTextWatcher : TextWatcher {

        override fun afterTextChanged(s: Editable?) {

        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

        }
    }


    companion object {

        const val PREFS_MAIL_FORM = "mail_form"
        const val PREFS_MAIL_REMEMBER = "mail_remember"
        const val PREFS_MAIL_ITEM_ID = "mail_item_id"

        fun create() = FinishFragmentView()
    }
}
