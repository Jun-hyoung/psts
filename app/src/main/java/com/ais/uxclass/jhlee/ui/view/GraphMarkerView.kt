package com.ais.uxclass.jhlee.ui.view

import android.content.Context
import android.widget.TextView
import com.ais.uxclass.jhlee.R
import com.ais.uxclass.jhlee.base.AndroidContext
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.utils.MPPointF

class GraphMarkerView(context: Context?, layoutResourceId: Int)
    : MarkerView(context, layoutResourceId) {

    private var labelView: TextView = findViewById(R.id.labelView)
    private val skills: MutableMap<Float, String> = HashMap()

    init {
        skills[1f] = AndroidContext.string(R.string.assessor_content_focus)
        skills[2f] = AndroidContext.string(R.string.assessor_content_organization)
        skills[3f] = AndroidContext.string(R.string.assessor_content_visual_aids)
        skills[4f] = AndroidContext.string(R.string.assessor_content_qa)
        skills[5f] = AndroidContext.string(R.string.assessor_language_eye_contact)
        skills[6f] = AndroidContext.string(R.string.assessor_language_enthusiasm)
        skills[7f] = AndroidContext.string(R.string.assessor_language_elocution)
        skills[8f] = AndroidContext.string(R.string.assessor_technical_knowledge)
        skills[9f] = AndroidContext.string(R.string.assessor_technical_research)
        skills[10f] = AndroidContext.string(R.string.assessor_technical_discussion_of_new_idea)
        skills[11f] = AndroidContext.string(R.string.assessor_technical_argument)
        skills[12f] = AndroidContext.string(R.string.assessor_technical_questions)
    }

    override fun refreshContent(e: Entry?, highlight: Highlight?) {
        e?.let {
            labelView.text = skills[it.x]
        }

        super.refreshContent(e, highlight)
    }

    override fun getOffset(): MPPointF {
        return MPPointF((-(width / 2)).toFloat(), (-height).toFloat())
    }

}