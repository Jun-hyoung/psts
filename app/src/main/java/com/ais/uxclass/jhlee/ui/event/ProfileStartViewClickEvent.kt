package com.ais.uxclass.jhlee.ui.event

import com.ais.uxclass.jhlee.app.assessor.domain.model.ProfileSet

class ProfileStartViewClickEvent(val profile: ProfileSet)