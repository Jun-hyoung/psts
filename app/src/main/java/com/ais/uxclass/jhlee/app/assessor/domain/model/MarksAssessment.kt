package com.ais.uxclass.jhlee.app.assessor.domain.model

data class MarksAssessment(var profile: ProfileSet?, var markSets: HashMap<String, Marks>)