package com.ais.uxclass.jhlee.app.assessor

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ais.uxclass.jhlee.R
import com.ais.uxclass.jhlee.app.assessor.domain.MarkSetManager
import com.ais.uxclass.jhlee.base.AndroidContext
import com.ais.uxclass.jhlee.mvp.MvpFragmentView
import com.ais.uxclass.jhlee.ui.event.CleanUpPagerViewEvent
import com.ais.uxclass.jhlee.ui.event.MarkerPagerFinishClickEvent
import com.ais.uxclass.jhlee.ui.event.MarkerPagerMoveClickEvent
import com.ais.uxclass.jhlee.ui.event.RxEventManager
import com.willy.ratingbar.BaseRatingBar
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_technical_page.*
import kotlinx.android.synthetic.main.layout_score_description.view.*
import kotlinx.android.synthetic.main.pager_child_item_layout.view.*

class TechnicalPageFragmentView : MvpFragmentView<TechnicalPageView, TechnicalPagePresenter<TechnicalPageView>>(),
        TechnicalPageView {

    private lateinit var markSetManager: MarkSetManager

    private lateinit var category: String

    private var finishViewEnabled: Boolean = false
        set (value) {
            finishView.isEnabled = value
        }

    private var disposables: CompositeDisposable? = null

    private val handler = Handler()

    private val nextViewRunner = Runnable { childPagerView.currentItem = childPagerView.currentItem + 1 }


    //----------------------------------------------------------------------------------------------
    // overrides

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        markSetManager = context as MarkSetManager
        category = AndroidContext.string(R.string.assessor_category_technical_title)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setUpEventListener()
    }

    override fun getLayoutResourceId() = R.layout.fragment_technical_page

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpViews()
    }

    override fun onDestroy() {
        super.onDestroy()

        releaseEventListener()
    }


    //----------------------------------------------------------------------------------------------
    // implements: MvpFragmentView

    override fun onCreatePresenter() = TechnicalPagePresenter<TechnicalPageView>()


    //----------------------------------------------------------------------------------------------
    // methods

    private fun setUpViews() {
        childPagerView.adapter = PagerChildAdapter()

        prevView.setOnClickListener {
            RxEventManager.instance.post(MarkerPagerMoveClickEvent(MarkerPagerFragmentView.PAGE_LANGUAGE))
        }
        finishView.setOnClickListener {
            RxEventManager.instance.post(MarkerPagerFinishClickEvent())

        }
    }

    private fun releaseEventListener() {
        if (disposables != null && !disposables!!.isDisposed) {
            disposables!!.dispose()
        }
    }

    private fun setUpEventListener() {
        releaseEventListener()

        disposables = CompositeDisposable()

        // profile view's start button clicked.
        disposables!!.add(RxEventManager.instance.subscribe(CleanUpPagerViewEvent::class.java) {
            clearViews()
        }!!)
    }

    private fun clearViews() {
        childPagerView.currentItem = 0
        childPagerView.adapter?.notifyDataSetChanged()
    }

    private fun getPageName(pageType: Int): String = when (pageType) {
        PAGE_TYPE_KNOWLEDGE -> AndroidContext.string(R.string.assessor_technical_knowledge)
        PAGE_TYPE_RESEARCH -> AndroidContext.string(R.string.assessor_technical_research)
        PAGE_TYPE_DISCUSSION_OF_NEW_IDEAS -> AndroidContext.string(R.string.assessor_technical_discussion_of_new_idea)
        PAGE_TYPE_ARGUMENT -> AndroidContext.string(R.string.assessor_technical_argument)
        PAGE_TYPE_QUESTIONS -> AndroidContext.string(R.string.assessor_technical_questions)
        else -> {
            throw IndexOutOfBoundsException()
        }
    }


    private inner class PagerChildAdapter : PagerAdapter() {

        override fun getCount(): Int {
            return PAGE_TYPE_COUNT
        }

        override fun isViewFromObject(view: View, any: Any): Boolean {
            return view == any
        }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val view = LayoutInflater.from(container.context)
                    .inflate(R.layout.pager_child_item_layout, container, false)

            when (position) {
                PAGE_TYPE_KNOWLEDGE -> {
                    view.itemTitleView.setText(R.string.assessor_technical_knowledge)
                    view.score4ContentView.setText(R.string.assessor_technical_knowledge_excellent)
                    view.score3ContentView.setText(R.string.assessor_technical_knowledge_good)
                    view.score2ContentView.setText(R.string.assessor_technical_knowledge_fair)
                    view.score1ContentView.setText(R.string.assessor_technical_knowledge_poor)
                    initScoreView(PAGE_TYPE_KNOWLEDGE, view.itemScoreView)
                }
                PAGE_TYPE_RESEARCH -> {
                    view.itemTitleView.setText(R.string.assessor_technical_research)
                    view.score4ContentView.setText(R.string.assessor_technical_research_excellent)
                    view.score3ContentView.setText(R.string.assessor_technical_research_good)
                    view.score2ContentView.setText(R.string.assessor_technical_research_fair)
                    view.score1ContentView.setText(R.string.assessor_technical_research_poor)
                    initScoreView(PAGE_TYPE_RESEARCH, view.itemScoreView)
                }
                PAGE_TYPE_DISCUSSION_OF_NEW_IDEAS -> {
                    view.itemTitleView.setText(R.string.assessor_technical_discussion_of_new_idea)
                    view.score4ContentView.setText(R.string.assessor_technical_discussion_of_new_idea_excellent)
                    view.score3ContentView.setText(R.string.assessor_technical_discussion_of_new_idea_good)
                    view.score2ContentView.setText(R.string.assessor_technical_discussion_of_new_idea_fair)
                    view.score1ContentView.setText(R.string.assessor_technical_discussion_of_new_idea_poor)
                    initScoreView(PAGE_TYPE_DISCUSSION_OF_NEW_IDEAS, view.itemScoreView)
                }
                PAGE_TYPE_ARGUMENT -> {
                    view.itemTitleView.setText(R.string.assessor_technical_argument)
                    view.score4ContentView.setText(R.string.assessor_technical_argument_excellent)
                    view.score3ContentView.setText(R.string.assessor_technical_argument_good)
                    view.score2ContentView.setText(R.string.assessor_technical_argument_fair)
                    view.score1ContentView.setText(R.string.assessor_technical_argument_poor)
                    initScoreView(PAGE_TYPE_ARGUMENT, view.itemScoreView)
                }
                PAGE_TYPE_QUESTIONS -> {
                    view.itemTitleView.setText(R.string.assessor_technical_questions)
                    view.score4ContentView.setText(R.string.assessor_technical_questions_excellent)
                    view.score3ContentView.setText(R.string.assessor_technical_questions_good)
                    view.score2ContentView.setText(R.string.assessor_technical_questions_fair)
                    view.score1ContentView.setText(R.string.assessor_technical_questions_poor)
                    initScoreView(PAGE_TYPE_QUESTIONS, view.itemScoreView)
                }
            }

            container.addView(view)
            return view
        }

        override fun getItemPosition(`object`: Any): Int {
            return PagerAdapter.POSITION_NONE
        }

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(`object` as View)
        }

        private fun initScoreView(pageType: Int, scoreView: BaseRatingBar) {
            scoreView.setOnRatingChangeListener { view, rating ->
                markScoreView(pageType, view, rating)
            }

            markSetManager.getMark(category, getPageName(pageType))?.let {
                scoreView.rating = it.toFloat()
            }
        }

        private fun markScoreView(pageType: Int, scoreView: BaseRatingBar, rating: Float) {
            val name = getPageName(pageType)

            if (markSetManager.getMark(category, name) != null) {
                if (rating == 0.0f) {
                    markSetManager.removeMark(category, name)
                } else {
                    markSetManager.putMark(category, name, rating.toInt())
                }
            } else if (rating > 0) {
                markSetManager.putMark(category, name, rating.toInt())

                if (markSetManager.getMarkSet(category).marks.size != PAGE_TYPE_COUNT) {
                    handler.postDelayed(nextViewRunner, 300)
                }
            }

            finishViewEnabled =  (markSetManager.getMarkSet(category).marks.size == PAGE_TYPE_COUNT)
        }
    }


    companion object {
        fun create() = TechnicalPageFragmentView()

        const val PAGE_TYPE_KNOWLEDGE: Int = 0
        const val PAGE_TYPE_RESEARCH: Int = 1
        const val PAGE_TYPE_DISCUSSION_OF_NEW_IDEAS: Int = 2
        const val PAGE_TYPE_ARGUMENT: Int = 3
        const val PAGE_TYPE_QUESTIONS: Int = 4
        const val PAGE_TYPE_COUNT: Int = 5
    }

}
