package com.ais.uxclass.jhlee.base

/**
 * Date 2018. 1. 10.
 * Author Jun-hyoung, Lee
 */
interface Command {
    fun execute()
}